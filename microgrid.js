
var windGeneration = [];
var oWindGeneration = [];

var photovoltaicGeneration = [];
var oPhotovoltaicGeneration = [];

var batterySoC = [];
var oBatterySoC = [];

var batteryPower = [];
var oBatteryPower = [];

var loadPower = [];
var oLoadPower = [];

var totalProducedPower = [];
var oTotalProducedPower = [];

var totalCost = [];
var oTotalCost = [];

var dieselGeneration = [];
var oDieselGeneration = [];

var commercialLoads = [];
var residencialLoads = [];
var loadsSupplied = [];
var finalSeries = [];
var totalProducedEnergy = [];

var dLoadA1 = [];
var dLoadB1 = [];
var dLoadC1 = [];
var dLoadD1 = [];
var dLoadShedA1 = [];
var dLoadShedB1 = [];
var dLoadShedC1 = [];
var dLoadShedD1 = [];

var timestamps = [];

function cleanArray(splitLine) {
  var resultLine = [];
  splitLine.forEach(function(item) {
    if (item !== "") {
      resultLine.push(item);
    }
  });

  return resultLine;
}

function getLoadsProfiles(text, limit) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf("LoadA1") == 0) {
      console.log("Lendo dLoadA1.");

      row = row.replace(".", ",");
      data = row.split(";");

      dLoadA1.push({name: "Carga A1", data: []})

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        dLoadA1[0].data.push(value);
      }
    }
    if (row.indexOf("LoadB1") == 0) {
      console.log("Lendo dLoadB1.");

      row = row.replace(".", ",");
      data = row.split(";");

      dLoadB1.push({name: "Carga B1", data: []})

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        dLoadB1[0].data.push(value);
      }
    }
    if (row.indexOf("LoadC1") == 0) {
      console.log("Lendo dLoadC1.");

      row = row.replace(".", ",");
      data = row.split(";");

      dLoadC1.push({name: "Carga C1", data: []})

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        dLoadC1[0].data.push(value);
      }
    }
    if (row.indexOf("LoadD1") == 0) {
      console.log("Lendo dLoadD1.");

      row = row.replace(".", ",");
      data = row.split(";");

      dLoadD1.push({name: "Carga D1", data: []})

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        dLoadD1[0].data.push(value);
      }
    }
  });

  console.log(timestamps);
}

function getLoadsConsumption(text, limit) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf("LoadShedA1") == 0) {
      console.log("Lendo dLoadShedA1.");

      row = row.replace(".", ",");
      data = row.split(";");

      dLoadShedA1.push({name: "Corte Carga A1", data: []})

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        dLoadShedA1[0].data.push(value);
      }
    }
    if (row.indexOf("LoadShedB1") == 0) {
      console.log("Lendo dLoadShedB1.");

      row = row.replace(".", ",");
      data = row.split(";");

      dLoadShedB1.push({name: "Corte Carga B1", data: []})

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        dLoadShedB1[0].data.push(value);
      }
    }
    if (row.indexOf("LoadShedC1") == 0) {
      console.log("Lendo dLoadShedC1.");

      row = row.replace(".", ",");
      data = row.split(";");

      dLoadShedC1.push({name: "Corte Carga C1", data: []})

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        dLoadShedC1[0].data.push(value);
      }
    }
    if (row.indexOf("LoadShedD1") == 0) {
      console.log("Lendo dLoadShedD1.");

      row = row.replace(".", ",");
      data = row.split(";");

      dLoadShedD1.push({name: "Corte Carga D1", data: []})

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        dLoadShedD1[0].data.push(value);
      }
    }
  });

  console.log(timestamps);
}

function getTimestamps(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Lendo timestamps.");

      row = row.replace(".", ",");
      data = row.split(";");

      for (i = 1; i < (data.length - 1); i++) {
        timestamps.push(parseInt(data[i]) - (3 * 60 * 60 * 1000));
      }
    }
  });

  console.log(timestamps);
}

function getWindGeneration(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados de geração eolica.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oWindGeneration.push({name: "IF - Geração Eolica", data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          oWindGeneration[0].data.push(value);
        }
      } else {
        windGeneration.push({name: "ID - Geração Eolica", data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          windGeneration[0].data.push(value);
        }
      }
    }
  });
}

function getTotalCost(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados de custos.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oTotalCost.push({name: "IF - Custo Total", data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          oTotalCost[0].data.push(value);
        }
      } else {
        totalCost.push({name: "ID - Custo Total", data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          totalCost[0].data.push(value);
        }
      }
    }
  });
}

function getSolarGeneration(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados de geração fotovoltaica.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oPhotovoltaicGeneration.push({name: "IF - Geração Fotovoltaica", data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          oPhotovoltaicGeneration[0].data.push(value);
        }
      } else {
        photovoltaicGeneration.push({name: "ID - Geração Fotovoltaica", data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          photovoltaicGeneration[0].data.push(value);
        }
      }
    }
  });
}

function getDieselGeneration(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados de geração diesel.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oDieselGeneration.push({name: "IF - Geração Diesel", data: []})
      } else {
        dieselGeneration.push({name: "ID - Geração Diesel", data: []})
      }

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        if (option == "O") {
          oDieselGeneration[0].data.push(value);
        } else {
          dieselGeneration[0].data.push(value);
        }
      }
    }
  });
}

function getBatterySoC(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados de soc da bateria.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oBatterySoC.push({name: "IF - Bateria SoC", yAxis: 1, data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          oBatterySoC[0].data.push(value);
        }
      } else {
        batterySoC.push({name: "ID - Bateria SoC", yAxis: 1, data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          batterySoC[0].data.push(value);
        }
      }
    }
  });
}

function getBatteryPower(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados de potência da bateria.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oBatteryPower.push({name: "IF - Potência Bateria", data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          oBatteryPower[0].data.push(value);
        }
      } else {
        batteryPower.push({name: "ID - Potência Bateria", data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          batteryPower[0].data.push(value);
        }
      }
    }
  });
}

function getBatteryPower2(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados de potência da bateria.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oBatteryPower.push({name: "IF - Potência Bateria", yAxis: 0, data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          oBatteryPower[0].data.push(value);
        }
      } else {
        batteryPower.push({name: "ID - Potência Bateria", yAxis: 0, data: []})

        for (i = 1; i < (data.length - 1); i++) {
          var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
          batteryPower[0].data.push(value);
        }
      }
    }
  });
}

function getLoadPower(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados de consumo das cargas.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oLoadPower.push({name: "IF - Potência Cargas", data: []})
      } else {
        loadPower.push({name: "ID - Potência Cargas", data: []})
      }

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        if (option == "O") {
          oLoadPower[0].data.push(value);
        } else {
          loadPower[0].data.push(value);
        }

      }
    }
  });
}

function getTotalProducedPower(token, text, limit, option) {
  var data = [];

  text.forEach(function(row, index) {
    if (row.indexOf(token) == 0) {
      console.log("Encontrado dados potência total produzida.");

      row = row.replace(".", ",");
      data = row.split(";");

      if (option == "O") {
        oTotalProducedPower.push({name: "IF - TPP", data: []})
      } else {
        totalProducedPower.push({name: "ID - TPP", data: []})
      }

      for (i = 1; i < (data.length - 1); i++) {
        var value = isNaN(parseFloat(data[i])) ? 0 : parseFloat(data[i]);
        if (option == "O") {
          oTotalProducedPower[0].data.push(value);
        } else {
          totalProducedPower[0].data.push(value);
        }
      }
    }
  });
}

function getFileFromServer(url, doneCallback) {
    var xhr;

    xhr = new XMLHttpRequest();
    xhr.onreadystatechange = handleStateChange;
    xhr.open("GET", url, true);
    xhr.overrideMimeType("text/plain");
    xhr.send();

    function handleStateChange() {
        if (xhr.readyState === 4) {
            doneCallback(xhr.status == 200 ? xhr.responseText : null);
        }
    }
}

function generateSeries(option) {
  if (option == "ID") {
    dieselGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    windGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    photovoltaicGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    // batterySoC.forEach(function(item) {
    //   finalSeries.push(item);
    // });
    batteryPower.forEach(function(item) {
      finalSeries.push(item);
    });
    loadPower.forEach(function(item) {
      finalSeries.push(item);
    });
    totalProducedPower.forEach(function(item) {
      finalSeries.push(item);
    });
  } else if (option == "IF") {
    oDieselGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    oWindGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    oPhotovoltaicGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    oBatteryPower.forEach(function(item) {
      finalSeries.push(item);
    });
    oLoadPower.forEach(function(item) {
      finalSeries.push(item);
    });
    oTotalProducedPower.forEach(function(item) {
      finalSeries.push(item);
    });
  } else if (option == "GD") {
    dieselGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    oDieselGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
  } else if (option == "LOAD") {
    loadPower.forEach(function(item) {
      finalSeries.push(item);
    });
    oLoadPower.forEach(function(item) {
      finalSeries.push(item);
    });
  } else if (option == "PV") {
    photovoltaicGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    oPhotovoltaicGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
  } else if (option == "WIND") {
    windGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
    oWindGeneration.forEach(function(item) {
      finalSeries.push(item);
    });
  } else if (option == "BAT") {
    batterySoC.forEach(function(item) {
      finalSeries.push(item);
    });
    oBatterySoC.forEach(function(item) {
      finalSeries.push(item);
    });
    batteryPower.forEach(function(item) {
      finalSeries.push(item);
    });
    oBatteryPower.forEach(function(item) {
      finalSeries.push(item);
    });
  } else if (option == "COSTS") {
    totalCost.forEach(function(item) {
      finalSeries.push(item);
    });
    oTotalCost.forEach(function(item) {
      finalSeries.push(item);
    });
  } else if (option == "LOADS") {
    dLoadA1.forEach(function(item) {
      finalSeries.push(item);
    });
    dLoadB1.forEach(function(item) {
      finalSeries.push(item);
    });
    dLoadC1.forEach(function(item) {
      finalSeries.push(item);
    });
    dLoadD1.forEach(function(item) {
      finalSeries.push(item);
    });
    dLoadShedA1.forEach(function(item) {
      finalSeries.push(item);
    });
    dLoadShedB1.forEach(function(item) {
      finalSeries.push(item);
    });
    dLoadShedC1.forEach(function(item) {
      finalSeries.push(item);
    });
    dLoadShedD1.forEach(function(item) {
      finalSeries.push(item);
    });
  }

}

getFileFromServer("microgrid_result.csv", function(text) {
    if (text === null) {
        console.log("Texto retornado pelo servidor é vazio.")
    }
    else {
        var textLines = text.match(/[^\r\n]+/g);
        getTimestamps("Timestamps", textLines, 8);
        getWindGeneration("GenWind", textLines, 8);
        getSolarGeneration("GenSolar", textLines, 8);
        getDieselGeneration("GenDiesel", textLines, 8);
        //getBatterySoC("BatterySoC", textLines, 8);
        getBatteryPower("BatteryPower", textLines, 8);
        getLoadPower("LoadPower", textLines, 8);
        getTotalProducedPower("TotalProducedPower", textLines, 8);
        //getTotalCost("TotalCost", textLines, 8);
        //getDLoadsSupplied(textLines, 8);
        //getLoadsSupplied(textLines);
        //getTotalProducedPower(textLines, 8);
        //getTotalProducedEnergy(textLines, 8);

        generateSeries("ID");

        Highcharts.chart('container', {

            title: {
                text: null
                //show: false;
            },

            yAxis: {
                title: {
                    text: 'Potência (kW)'
                }
            },
            xAxis: {
              type: 'datetime',
              categories: timestamps,
              labels: {
                formatter: function() {
                  //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                  return Highcharts.dateFormat('%H:%M', this.value);
                }
              }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        enabled: false
                    }
                }
            },

            series: finalSeries,

            responsive: {
                rules: [{
                    condition: {
                        //maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }
});

getFileFromServer("microgrid_result.csv", function(text) {
    finalSeries = []
    dieselGeneration = [];
    windGeneration = [];
    photovoltaicGeneration = [];
    batterySoC = [];
    loadPower = [];
    totalProducedPower = [];
    totalCost = [];

    if (text === null) {
        console.log("Texto retornado pelo servidor é vazio.")
    }
    else {
        var textLines = text.match(/[^\r\n]+/g);
        getWindGeneration("OriginalGenWind", textLines, 8, "O");
        getSolarGeneration("OriginalGenSolar", textLines, 8, "O");
        getDieselGeneration("OriginalGenDiesel", textLines, 8, "O");
        getBatterySoC("OriginalBatterySoC", textLines, 8, "O");
        getBatteryPower("OriginalBatteryPower", textLines, 8, "O");
        getLoadPower("OriginalLoadPower", textLines, 8, "O");
        getTotalProducedPower("OriginalTotalProducedPower", textLines, 8, "O");
        //getTotalCost("TotalCost", textLines, 8);
        //getTotalCost("OriginalTotalCost", textLines, 8, "O");

        generateSeries("IF");

        Highcharts.chart('container2', {

            title: {
                text: null
            },

            yAxis: {
                title: {
                    text: 'Potência (kW)'
                }
            },
            xAxis: {
              type: 'datetime',
              categories: timestamps,
              labels: {
                formatter: function() {
                  //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                  return Highcharts.dateFormat('%H:%M', this.value);
                }
              }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        enabled: false
                    }
                }
            },

            series: finalSeries,

            responsive: {
                rules: [{
                    condition: {
                        //maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }
});

getFileFromServer("microgrid_result.csv", function(text) {
    finalSeries = []
    dieselGeneration = [];
    oDieselGeneration = [];
    windGeneration = [];
    photovoltaicGeneration = [];
    batterySoC = [];
    loadPower = [];
    totalProducedPower = [];
    totalCost = [];

    if (text === null) {
        console.log("Texto retornado pelo servidor é vazio.")
    }
    else {
        var textLines = text.match(/[^\r\n]+/g);
        getDieselGeneration("GenDiesel", textLines, 8);
        getDieselGeneration("OriginalGenDiesel", textLines, 8, "O");

        generateSeries("GD");

        Highcharts.chart('container-gd', {

            title: {
                text: null
            },

            yAxis: {
                title: {
                    text: 'Potência (kW)'
                }
            },
            xAxis: {
              min: 0,
              type: 'datetime',
              categories: timestamps,
              labels: {
                formatter: function() {
                  //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                  return Highcharts.dateFormat('%H:%M', this.value);
                }
              }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        enabled: false
                    }
                }
            },

            series: finalSeries,

            responsive: {
                rules: [{
                    condition: {
                        //maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }
});

getFileFromServer("microgrid_result.csv", function(text) {
    finalSeries = []
    dieselGeneration = [];
    oDieselGeneration = [];
    oLoadPower = [];
    windGeneration = [];
    photovoltaicGeneration = [];
    batterySoC = [];
    loadPower = [];
    totalProducedPower = [];
    totalCost = [];

    if (text === null) {
        console.log("Texto retornado pelo servidor é vazio.")
    }
    else {
        var textLines = text.match(/[^\r\n]+/g);
        getLoadPower("LoadPower", textLines, 8);
        getLoadPower("OriginalLoadPower", textLines, 8, "O");

        generateSeries("LOAD");

        Highcharts.chart('container-load', {

            title: {
                text: null
            },

            yAxis: {
                title: {
                    text: 'Potência (kW)'
                }
            },
            xAxis: {
              type: 'datetime',
              categories: timestamps,
              labels: {
                formatter: function() {
                  //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                  return Highcharts.dateFormat('%H:%M', this.value);
                }
              }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        enabled: false
                    }
                }
            },

            series: finalSeries,

            responsive: {
                rules: [{
                    condition: {
                        //maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }

    getFileFromServer("microgrid_result.csv", function(text) {
        finalSeries = []
        dieselGeneration = [];
        oDieselGeneration = [];
        oLoadPower = [];
        windGeneration = [];
        photovoltaicGeneration = [];
        oPhotovoltaicGeneration = [];
        batterySoC = [];
        oBatterySoC = [];
        loadPower = [];
        totalProducedPower = [];
        totalCost = [];

        if (text === null) {
            console.log("Texto retornado pelo servidor é vazio.")
        }
        else {
            var textLines = text.match(/[^\r\n]+/g);
            getSolarGeneration("GenSolar", textLines, 8);
            getSolarGeneration("OriginalGenSolar", textLines, 8, "O");

            generateSeries("PV");

            Highcharts.chart('container-pv', {

                title: {
                    text: null
                },

                yAxis: {
                    title: {
                        text: 'Potência (kW)'
                    }
                },
                xAxis: {
                  type: 'datetime',
                  categories: timestamps,
                  labels: {
                    formatter: function() {
                      //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                      return Highcharts.dateFormat('%H:%M', this.value);
                    }
                  }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            enabled: false
                        }
                    }
                },

                series: finalSeries,

                responsive: {
                    rules: [{
                        condition: {
                            //maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
        }
    });
});

getFileFromServer("microgrid_result.csv", function(text) {
    finalSeries = []
    dieselGeneration = [];
    oDieselGeneration = [];
    oLoadPower = [];
    windGeneration = [];
    oWindGeneration = [];
    photovoltaicGeneration = [];
    batterySoC = [];
    oBatterySoC = [];
    loadPower = [];
    totalProducedPower = [];
    totalCost = [];

    if (text === null) {
        console.log("Texto retornado pelo servidor é vazio.")
    }
    else {
        var textLines = text.match(/[^\r\n]+/g);
        getWindGeneration("GenWind", textLines, 8);
        getWindGeneration("OriginalGenWind", textLines, 8, "O");

        generateSeries("WIND");

        Highcharts.chart('container-wind', {

            title: {
                text: null
            },

            yAxis: {
                title: {
                    text: 'Potência (kW)'
                }
            },
            xAxis: {
              type: 'datetime',
              categories: timestamps,
              labels: {
                formatter: function() {
                  //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                  return Highcharts.dateFormat('%H:%M', this.value);
                }
              }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        enabled: false
                    }
                }
            },

            series: finalSeries,

            responsive: {
                rules: [{
                    condition: {
                        //maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }
});

getFileFromServer("microgrid_result.csv", function(text) {
    finalSeries = []
    dieselGeneration = [];
    oDieselGeneration = [];
    oLoadPower = [];
    windGeneration = [];
    oWindGeneration = [];
    photovoltaicGeneration = [];
    batterySoC = [];
    oBatterySoC = [];
    batteryPower = [];
    oBatteryPower = [];
    loadPower = [];
    totalProducedPower = [];
    totalCost = [];

    if (text === null) {
        console.log("Texto retornado pelo servidor é vazio.")
    }
    else {
        var textLines = text.match(/[^\r\n]+/g);
        //getBatteryPower("BatteryPower", textLines, 8);
        //getBatteryPower("OriginalBatteryPower", textLines, 8, "O");
        getBatterySoC("BatterySoC", textLines, 8);
        getBatterySoC("OriginalBatterySoC", textLines, 8, "O");

        generateSeries("BAT");

        Highcharts.chart('container-battery', {

            title: {
                text: null
            },

            yAxis: [{
                title: {
                    text: 'Potência (kW)'
                },
                opposite: false
              },{
                min: 0,
                max: 100,
                title: {
                    text: 'SoC (%)'
                },
                opposite: true
              }],
            xAxis: {
              type: 'datetime',
              categories: timestamps,
              labels: {
                formatter: function() {
                  //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                  return Highcharts.dateFormat('%H:%M', this.value);
                }
              }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        enabled: false
                    }
                }
            },

            series: finalSeries,

            responsive: {
                rules: [{
                    condition: {
                      //  maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }
});

getFileFromServer("microgrid_result.csv", function(text) {
    finalSeries = []
    dieselGeneration = [];
    oDieselGeneration = [];
    oLoadPower = [];
    windGeneration = [];
    oWindGeneration = [];
    photovoltaicGeneration = [];
    batterySoC = [];
    oBatterySoC = [];
    loadPower = [];
    totalProducedPower = [];
    totalCost = [];

    if (text === null) {
        console.log("Texto retornado pelo servidor é vazio.")
    }
    else {
        var textLines = text.match(/[^\r\n]+/g);
        getLoadsProfiles(textLines, 8);
        getLoadsConsumption(textLines, 8);

        generateSeries("LOADS");

        Highcharts.chart('container-loads', {

            title: {
                text: null
            },

            yAxis: {
                title: {
                    text: 'Potência (kW)'
                }
            },
            xAxis: {
              type: 'datetime',
              categories: timestamps,
              labels: {
                formatter: function() {
                  //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                  return Highcharts.dateFormat('%H:%M', this.value);
                }
              }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        enabled: false
                    }
                }
            },

            series: finalSeries,

            responsive: {
                rules: [{
                    condition: {
                        //maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }
});

getFileFromServer("microgrid_result.csv", function(text) {
    finalSeries = []
    dieselGeneration = [];
    oDieselGeneration = [];
    oLoadPower = [];
    windGeneration = [];
    oWindGeneration = [];
    photovoltaicGeneration = [];
    batterySoC = [];
    oBatterySoC = [];
    loadPower = [];
    totalProducedPower = [];
    totalCost = [];
    oTotalCost = [];

    if (text === null) {
        console.log("Texto retornado pelo servidor é vazio.")
    }
    else {
        var textLines = text.match(/[^\r\n]+/g);
        getTotalCost("TotalCost", textLines, 8);
        getTotalCost("OriginalTotalCost", textLines, 8, "O");

        generateSeries("COSTS");

        Highcharts.chart('container-costs', {

            title: {
                text: null
            },

            yAxis: {
                title: {
                    text: 'Custo (R$)'
                }
            },
            xAxis: {
              type: 'datetime',
              categories: timestamps,
              labels: {
                formatter: function() {
                  //return Highcharts.dateFormat('%Y-%m-%d %H:%M', this.value);
                  return Highcharts.dateFormat('%H:%M', this.value);
                }
              }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        enabled: false
                    }
                }
            },

            series: finalSeries,

            responsive: {
                rules: [{
                    condition: {
                        //maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }
});
