package Application;

import java.util.Date;

public class MGConfiguration {
	private Boolean useDieselGenerators;
	private Boolean useBattery;
	private Integer initialPeriodInterval;
	private Integer middlePeriodInterval;
	private Integer finalPeriodInterval;
	private Integer periodInterval;
	private Integer firstIntervalPeriods;
	private Integer variation;
	private Date interruptionDateTime;
	
	public MGConfiguration() {
		this.useBattery = true;
		this.useDieselGenerators = true;
		this.initialPeriodInterval = 5;
		this.middlePeriodInterval = 30;
		this.finalPeriodInterval = 60;
		this.periodInterval = 5;
		this.firstIntervalPeriods = 6;
	}
	
	public Boolean getUseDieselGenerators() {
		return useDieselGenerators;
	}
	
	public void setUseDieselGenerators(Boolean useDieselGenerators) {
		this.useDieselGenerators = useDieselGenerators;
	}
	
	public Boolean getUseBattery() {
		return useBattery;
	}
	
	public void setUseBattery(Boolean useBattery) {
		this.useBattery = useBattery;
	}
	
	public Integer getInitialPeriodInterval() {
		return initialPeriodInterval;
	}
	
	public void setInitialPeriodInterval(Integer initialPeriodInterval) {
		this.initialPeriodInterval = initialPeriodInterval;
	}
	
	public Integer getMiddlePeriodInterval() {
		return middlePeriodInterval;
	}
	
	public void setMiddlePeriodInterval(Integer middlePeriodInterval) {
		this.middlePeriodInterval = middlePeriodInterval;
	}
	
	public Integer getFinalPeriodInterval() {
		return finalPeriodInterval;
	}
	
	public void setFinalPeriodInterval(Integer finalPeriodInterval) {
		this.finalPeriodInterval = finalPeriodInterval;
	}
	
	public Integer getPeriodInterval() {
		return periodInterval;
	}
	
	public void setPeriodInterval(Integer periodInterval) {
		this.periodInterval = periodInterval;
	}

	public Integer getFirstIntervalPeriods() {
		return firstIntervalPeriods;
	}
	
	public void setFirstIntervalPeriods(Integer periods) {
		this.firstIntervalPeriods = periods;
	}
	
	public Integer getVariation() {
		return variation;
	}
	
	public void setVariation(Integer nVariation) {
		this.variation = nVariation;
	}

	public void setInterruptionStart(Date interruptionDateTime) {
		// TODO Auto-generated method stub
		this.interruptionDateTime = interruptionDateTime;
	}
	
	public Date getInterruptionStart() {
		return this.interruptionDateTime;
	}
}
