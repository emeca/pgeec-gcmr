package Application;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.apache.poi.EncryptedDocumentException;

import com.gams.api.GAMSVariable;
import com.gams.api.GAMSVariableRecord;

import Builders.SimulationDataBuilder;
import Data.BatteryData;
import Data.LoadsData;
import Data.RawData;
import Data.ResultData;
import Data.SimulationData;
import Data.WeatherData;
import Gams.GamsProcess;
import Parser.ExcelReader;
import Utils.DateTimeUtils;

public class MainApp {

	public static void main(String[] args) throws EncryptedDocumentException, IOException, ParseException {
		BatteryData battery = new BatteryData();
		MGConfiguration configuration = new MGConfiguration();
		long runTime = 0L;
		long totalRunTime = 0L;
		String interruptionTime = new String("26-08-2016 10:00:00");
		
		// Dura��o da interrup��o (Minutos)
		Integer interruptionPeriod = 640;
		// In�cio da Interrup��o
		Integer interruptionStart = DateTimeUtils.dateToInterruptionIndex(interruptionTime);
		
		configuration.setMiddlePeriodInterval(30);
		configuration.setFinalPeriodInterval(60);
		configuration.setFirstIntervalPeriods(6);
		configuration.setUseBattery(true);
		configuration.setVariation(0);
		configuration.setInterruptionStart(DateTimeUtils.getInterruptionDateTime(interruptionTime));
		
		Double totalCost = 0.0;
		// Definindo ponto inicial da bateria em 60%
		battery.setSoc(60.0);
		Boolean firstRun = true;
		// Vari�vel que define se primeira execu��o ser� com discretiza��o original (5 minutos)
		Boolean executeFull = true;
		WeatherData weatherData = new WeatherData();
		LoadsData loadsData = new LoadsData();
		ResultData resultData = new ResultData();
		ResultData resultDataOriginal = new ResultData();
		
		ExcelReader excelReader = new ExcelReader("WeatherData.xlsx", "Loads.xlsx");
		
		weatherData = excelReader.parseWeatherData();
		loadsData = excelReader.parseLoadsData();
		
		ArrayList<Double> tempLoadDataPredicted = new ArrayList<Double>();
		ArrayList<Double> tempLoadDataConsumed = new ArrayList<Double>();
		
		System.out.println("Ponto de interrup��o: " + interruptionStart);
		// Realizar c�lculos para agrupar intervalos
		// Abaixo de 40 minutos, agrupar e acima, dividir em intervalos equivalentes
		// Verificar vari�veis para setar as condi��es necess�rias de ajuste
		int counter = 0;
		totalRunTime = new Date().getTime();
		while (interruptionPeriod > 0) {
			runTime = new Date().getTime();
			SimulationDataBuilder sBuilder = new SimulationDataBuilder(weatherData, loadsData, interruptionStart + (counter), interruptionPeriod, configuration);
			SimulationData sData = null;

			sData = sBuilder.buildDinamic(executeFull); // Para criar com o perfil das cargas de acordo
			// Execucao do processo do GAMS
			GamsProcess gp = new GamsProcess(sData, configuration);
			gp.run(battery);
			
			if (firstRun && executeFull) {
				GAMSVariable var = gp.getVariable("PotenciaGeradaDiesel");
		        for (GAMSVariableRecord rec : var)
		        	resultDataOriginal.addGenDieselValue(new Date(), Double.valueOf(rec.getLevel()));
		        var = gp.getVariable("PotenciaGeradaSolar");
		        for (GAMSVariableRecord rec : var)
		        	resultDataOriginal.addGenSolar(new Date(), Double.valueOf(rec.getLevel()));
		        var = gp.getVariable("PotenciaGeradaEolica");
		        for (GAMSVariableRecord rec : var)
		        	resultDataOriginal.addGenWind(new Date(), Double.valueOf(rec.getLevel()));
		        var = gp.getVariable("PotenciaSupridaCargasDesp");
		        for (GAMSVariableRecord rec : var)
		        	resultDataOriginal.addLoadPower(new Date(), Double.valueOf(rec.getLevel()));
		        var = gp.getVariable("PotenciaNaoSupridaCargasDesp");
		        for (GAMSVariableRecord rec : var)
		        	resultDataOriginal.addLoadShededPower(new Date(), Double.valueOf(rec.getLevel()));
		        var = gp.getVariable("TotalPotenciaProduzida");
		        for (GAMSVariableRecord rec : var)
		        	resultDataOriginal.addTotalProducedPower(new Date(), Double.valueOf(rec.getLevel()));
		        var = gp.getVariable("BateriaSoC");
		        for (GAMSVariableRecord rec : var)
		        	resultDataOriginal.addBatterySOC(new Date(), Double.valueOf(rec.getLevel()));
		        var = gp.getVariable("CustoPorPeriodos");
		        for (GAMSVariableRecord rec : var) {
		        	resultDataOriginal.addPeriodCost(new Date(), Double.valueOf(rec.getLevel()));
		        }
		        var = gp.getVariable("BateriaCarregamento");
		        for (GAMSVariableRecord rec : var) {
		        	resultDataOriginal.addBatteryCharge(new Date(), Double.valueOf(rec.getLevel()));
		        }
		        var = gp.getVariable("BateriaDescarregamento");
		        for (GAMSVariableRecord rec : var) {
		        	resultDataOriginal.addBatteryDisharge(new Date(), Double.valueOf(rec.getLevel()));
		        }
		        for (int i = 0; i < resultDataOriginal.getBatteryCharge().size(); i++) {
		        	resultDataOriginal.addBatteryPower(new Date(), resultDataOriginal.getBatteryCharge().get(i).getData() - resultDataOriginal.getBatteryDisharge().get(i).getData());
		        }
		        
		        tempLoadDataPredicted = gp.getResultVariableParameter("perfilCargas", "dloadA1");
			    tempLoadDataConsumed = gp.getResultVariableParameter("PotenciaNaoSupridaCargasDesp", "dloadA1");
		        for (int i = 0; i < tempLoadDataPredicted.size(); i++) {
		        	resultDataOriginal.addLoadA1(new Date(), tempLoadDataPredicted.get(i));
		        	resultDataOriginal.addLoadShedA1(new Date(), tempLoadDataConsumed.get(i));
		        }	
		        tempLoadDataPredicted = gp.getResultVariableParameter("perfilCargas", "dloadB1");
			    tempLoadDataConsumed = gp.getResultVariableParameter("PotenciaNaoSupridaCargasDesp", "dloadB1");
		        for (int i = 0; i < tempLoadDataPredicted.size(); i++) {
		        	resultDataOriginal.addLoadA2(new Date(), tempLoadDataPredicted.get(i));
		        	resultDataOriginal.addLoadShedA2(new Date(), tempLoadDataConsumed.get(i));
		        }
		        tempLoadDataPredicted = gp.getResultVariableParameter("perfilCargas", "dloadC1");
			    tempLoadDataConsumed = gp.getResultVariableParameter("PotenciaNaoSupridaCargasDesp", "dloadC1");
		        for (int i = 0; i < tempLoadDataPredicted.size(); i++) {
		        	resultDataOriginal.addLoadA3(new Date(), tempLoadDataPredicted.get(i));
		        	resultDataOriginal.addLoadShedA3(new Date(), tempLoadDataConsumed.get(i));
		        }
		        tempLoadDataPredicted = gp.getResultVariableParameter("perfilCargas", "dloadD1");
			    tempLoadDataConsumed = gp.getResultVariableParameter("PotenciaNaoSupridaCargasDesp", "dloadD1");
		        for (int i = 0; i < tempLoadDataPredicted.size(); i++) {
		        	resultDataOriginal.addLoadA4(new Date(), tempLoadDataPredicted.get(i));
		        	resultDataOriginal.addLoadShedA4(new Date(), tempLoadDataConsumed.get(i));
		        }
        
		        firstRun = false;
		        executeFull = false;
		        System.out.println("Execu��o IF: " + ((new Date().getTime() - runTime) / 1000.0) + " segundos");
		        continue;
			}
			
			resultData.addGenDieselValue(new Date(), Double.valueOf(gp.getFirstResultVariable("PotenciaGeradaDiesel")));
			resultData.addGenSolar(new Date(), Double.valueOf(gp.getFirstResultVariable("PotenciaGeradaSolar")));
			resultData.addGenWind(new Date(), Double.valueOf(gp.getFirstResultVariable("PotenciaGeradaEolica")));
			resultData.addLoadPower(new Date(), Double.valueOf(gp.getFirstResultVariable("PotenciaSupridaCargasDesp")));
			resultData.addTotalProducedPower(new Date(), Double.valueOf(gp.getFirstResultVariable("TotalPotenciaProduzida")));
			resultData.addBatterySOC(new Date(), Double.valueOf(gp.getFirstResultVariable("BateriaSoC")));
			resultData.addBatteryPower(new Date(), Double.valueOf(gp.getFirstResultVariable("BateriaCarregamento")) - Double.valueOf(gp.getFirstResultVariable("BateriaDescarregamento")));
			resultData.addPeriodCost(new Date(), Double.valueOf(gp.getFirstResultVariable("CustoPorPeriodos")));
			
			resultData.addLoadA1(new Date(), Double.valueOf(gp.getFirstResultVariableParameter("perfilCargas", "dloadA1")));
			resultData.addLoadA2(new Date(), Double.valueOf(gp.getFirstResultVariableParameter("perfilCargas", "dloadB1")));
			resultData.addLoadA3(new Date(), Double.valueOf(gp.getFirstResultVariableParameter("perfilCargas", "dloadC1")));
			resultData.addLoadA4(new Date(), Double.valueOf(gp.getFirstResultVariableParameter("perfilCargas", "dloadD1")));
			
			resultData.addLoadShedA1(new Date(), Double.valueOf(gp.getFirstResultVariableParameter("PotenciaNaoSupridaCargasDesp", "dloadA1")));
			resultData.addLoadShedA2(new Date(), Double.valueOf(gp.getFirstResultVariableParameter("PotenciaNaoSupridaCargasDesp", "dloadB1")));
			resultData.addLoadShedA3(new Date(), Double.valueOf(gp.getFirstResultVariableParameter("PotenciaNaoSupridaCargasDesp", "dloadC1")));
			resultData.addLoadShedA4(new Date(), Double.valueOf(gp.getFirstResultVariableParameter("PotenciaNaoSupridaCargasDesp", "dloadD1")));
			
			if (!executeFull) {
				// Pegando informa��es da bateria para a pr�xima execu��o
				battery.setCharging(Double.valueOf(gp.getFirstResultVariable("BateriaCarregamento")));
				battery.setDischarging(Double.valueOf(gp.getFirstResultVariable("BateriaDescarregamento")));
				battery.setSoc(Double.valueOf(gp.getFirstResultVariable("BateriaSoC")));
				battery.setGeneratorStatus(Double.valueOf(gp.getFirstResultVariable("StatusGerDiesel")));
				
//				System.out.print(gp.getFirstResultVariable("BateriaCarregamento") + " ");
//				System.out.print(gp.getFirstResultVariable("BateriaDescarregamento") + " ");
//				System.out.println(gp.getFirstResultVariable("BateriaSoC"));
				
				//System.out.println(counter);
				// Resultado final � apresentar o primeiro momento de cada processo de otimiza��o
				// System.out.println("Tamanho do vetor: " + weatherData.getTemperature().size());
				interruptionPeriod -= 5;
				//System.out.println("Processo executado: " + interruptionPeriod + " minutos restantes do total da interrup��o.");
				counter++;
			}
			System.out.println(((new Date().getTime() - runTime) / 1000.0) + " segundos");
		}
		
		System.out.println(((new Date().getTime() - totalRunTime) / 1000.0) + " segundos para toda a execu��o");
		System.out.println("Tamanho final do resultado: " + resultData.getGenDiesel().size());
		BufferedWriter br = new BufferedWriter(new FileWriter("microgrid_result.csv"));
        StringBuilder sb = new StringBuilder();
        Date interruptionStartExit = DateTimeUtils.getInterruptionDateTime(interruptionTime);
        
        Double loadSheded = 0.0;
        Double loadPrediction = 0.0;
        Double loadShededOriginal = 0.0;
        Double loadPredictionOriginal = 0.0;
        Integer index = 0;
        
        Double loadA1Predicted = 0.0;
        Double loadA2Predicted = 0.0;
        Double loadA3Predicted = 0.0;
        Double loadA4Predicted = 0.0;
        
        Double loadA1Sheded = 0.0;
        Double loadA2Sheded = 0.0;
        Double loadA3Sheded = 0.0;
        Double loadA4Sheded = 0.0;
        
        Double tempoA1 = 0.0;
        Double tempoA2 = 0.0;
        Double tempoA3 = 0.0;
        Double tempoA4 = 0.0;
        
        // Escrita dos dados de sa�da para a cria��o dos gr�ficos
        // Dados da execu��o a cada 5 minutos
        sb.append("Timestamps;");
        for (RawData raw: resultData.getGenDiesel()) {
        	sb.append(interruptionStartExit.getTime() + ";");
        	interruptionStartExit = DateTimeUtils.addPeriod(interruptionStartExit, 5);
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("GenDiesel;");
        for (RawData raw: resultData.getGenDiesel()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("GenSolar;");
        for (RawData raw: resultData.getGenSolar()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("GenWind;");
        for (RawData raw: resultData.getGenWind()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("BatterySoC;");
        for (RawData raw: resultData.getBatterySOC()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("BatteryPower;");
        for (RawData raw: resultData.getBatteryPower()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("LoadPower;");
        for (RawData raw: resultData.getLoadPower()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("LoadA1;");
        for (RawData raw: resultData.getLoad1()) {
        	loadPrediction += raw.getData() * 0.083333;
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("LoadB1;");
        for (RawData raw: resultData.getLoad2()) {
        	loadPrediction += raw.getData() * 0.083333;
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("LoadC1;");
        for (RawData raw: resultData.getLoad3()) {
        	loadPrediction += raw.getData() * 0.083333;
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("LoadD1;");
        for (RawData raw: resultData.getLoad4()) {
        	loadPrediction += raw.getData() * 0.083333;
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("LoadShedA1;");
        for (RawData raw: resultData.getLoadShed1()) {
        	loadA1Predicted += resultData.getLoad1().get(index).getData() * 0.083333;
        	if (raw.getData() > 0) {
        		loadSheded += resultData.getLoad1().get(index).getData() * 0.083333;
        		loadA1Sheded += resultData.getLoad1().get(index).getData() * 0.083333;
        	} else {
        		tempoA1 += 5.0;
        	}
        	sb.append((resultData.getLoad1().get(index).getData() - raw.getData()) + ";");
        	index++;
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        index = 0;
        
        sb.append("LoadShedB1;");
        for (RawData raw: resultData.getLoadShed2()) {
        	loadA2Predicted += resultData.getLoad2().get(index).getData() * 0.083333;
        	if (raw.getData() > 0) {
        		loadSheded += resultData.getLoad2().get(index).getData() * 0.083333;
        		loadA2Sheded += resultData.getLoad2().get(index).getData() * 0.083333;
        	} else {
        		tempoA2 += 5.0;
        	}
        	sb.append((resultData.getLoad2().get(index).getData() - raw.getData()) + ";");
        	index++;
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        index = 0;
        
        sb.append("LoadShedC1;");
        for (RawData raw: resultData.getLoadShed3()) {
        	loadA3Predicted += resultData.getLoad3().get(index).getData() * 0.083333;
        	if (raw.getData() > 0) {
        		loadSheded += resultData.getLoad3().get(index).getData() * 0.083333;
        		loadA3Sheded += resultData.getLoad3().get(index).getData() * 0.083333;
        	} else {
        		tempoA3 += 5.0;
        	}
        	sb.append((resultData.getLoad3().get(index).getData() - raw.getData()) + ";");
        	index++;
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        index = 0;
        
        sb.append("LoadShedD1;");
        for (RawData raw: resultData.getLoadShed4()) {
        	loadA4Predicted += resultData.getLoad4().get(index).getData() * 0.083333;
        	if (raw.getData() > 0) {
        		loadSheded += resultData.getLoad4().get(index).getData() * 0.083333;
        		loadA4Sheded += resultData.getLoad4().get(index).getData() * 0.083333;
        	} else {
        		tempoA4 += 5.0;
        	}
        	sb.append((resultData.getLoad4().get(index).getData() - raw.getData()) + ";");
        	index++;
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        index = 0;
        
        sb.append("TotalProducedPower;");
        for (RawData raw: resultData.getTotalProducedPower()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("TotalCost;");
        totalCost = 0.0;
        for (RawData raw: resultData.getPeriodCost()) {
        	totalCost += raw.getData();
        	sb.append(totalCost + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0,  sb.length());
        
        // Dados da primeira execu��o total
        sb.append("OriginalGenDiesel;");
        for (RawData raw: resultDataOriginal.getGenDiesel()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("OriginalGenSolar;");
        for (RawData raw: resultDataOriginal.getGenSolar()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("OriginalGenWind;");
        for (RawData raw: resultDataOriginal.getGenWind()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("OriginalBatterySoC;");
        for (RawData raw: resultDataOriginal.getBatterySOC()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("OriginalBatteryPower;");
        for (RawData raw: resultDataOriginal.getBatteryPower()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("OriginalLoadPower;");
        for (RawData raw: resultDataOriginal.getLoadPower()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        sb.append("OriginalTotalProducedPower;");
        for (RawData raw: resultDataOriginal.getTotalProducedPower()) {
        	sb.append(raw.getData() + ";");
        }
        br.write(sb.toString() + "\n");
        sb.delete(0, sb.length());
        
        totalCost = 0.0;
        sb.append("OriginalTotalCost;");
        for (RawData raw: resultDataOriginal.getPeriodCost()) {
        	totalCost += raw.getData();
        	sb.append(totalCost + ";");
        }
        br.write(sb.toString() + "\n");

        br.close();
        
        for (RawData raw: resultDataOriginal.getLoadShededPower()) {
        	if (raw.getData() > 0) {
        		loadShededOriginal += raw.getData() * 0.083333;
        	}
        }
        
        for (RawData raw: resultDataOriginal.getLoadPower()) {
        	loadPredictionOriginal += raw.getData() * 0.083333;
        }
        
        System.out.println("Previs�o das cargas: IF(" + (loadPredictionOriginal + loadShededOriginal) + ") ID(" + loadPrediction + ")");
        System.out.println("Total de carga cortada: IF(" + loadShededOriginal + ") ID(" + loadSheded + ")");
        System.out.println("Diferen�a do previsto: IF(" + (loadShededOriginal / (loadPredictionOriginal + loadShededOriginal)) + ") ID(" + (loadSheded / loadPrediction) + ")");
        System.out.println("Previs�o/Corte Carga A1[" + loadA1Predicted + "|" + loadA1Sheded + "] Tempo Atendido: " + tempoA1 + " minutos.");
        System.out.println("Previs�o/Corte Carga A2[" + loadA2Predicted + "|" + loadA2Sheded + "] Tempo Atendido: " + tempoA2 + " minutos.");
        System.out.println("Previs�o/Corte Carga A3[" + loadA3Predicted + "|" + loadA3Sheded + "] Tempo Atendido: " + tempoA3 + " minutos.");
        System.out.println("Previs�o/Corte Carga A4[" + loadA4Predicted + "|" + loadA4Sheded + "] Tempo Atendido: " + tempoA4 + " minutos.");
        
		
	}

}
