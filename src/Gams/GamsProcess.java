package Gams;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.gams.api.GAMSException;
import com.gams.api.GAMSJob;
import com.gams.api.GAMSOptions;
import com.gams.api.GAMSVariable;
import com.gams.api.GAMSVariableRecord;
import com.gams.api.GAMSWorkspace;
import com.gams.api.GAMSWorkspaceInfo;

import Application.MGConfiguration;
import Data.BatteryData;
import Data.SimulationData;
import Model.IslandedModel;

public class GamsProcess {
	private SimulationData simulationData;
	private GAMSJob job;
	private MGConfiguration configuration;
	
	public GamsProcess(SimulationData sData, MGConfiguration config) {
		this.simulationData = sData;
		this.configuration = config;
	}
	
	public void run(BatteryData battery) {
		IslandedModel inputModel = new IslandedModel(this.simulationData, battery.getSoc());
		
		File workingDirectory = new File(System.getProperty("user.dir"), "MicrogridModel");
		workingDirectory.mkdir();
		
        GAMSWorkspace ws = null;

        // create GAMSWorkspace "ws" with default system directory and default working directory 
        // (the directory named with current date and time under System.getProperty("java.io.tmpdir"))
        //ws = new GAMSWorkspace();
    	GAMSWorkspaceInfo wsInfo = new GAMSWorkspaceInfo();
    	wsInfo.setWorkingDirectory(workingDirectory.getAbsolutePath());
    	ws = new GAMSWorkspace(wsInfo);

        // create GAMSJob "t2" from the "model" string variable
        //GAMSJob t2 = ws.addJobFromString(inputDataAndModel.getModel());
        inputModel.buildModel3(battery, this.configuration);
    	//inputModel.buildModel2Gen(battery, this.configuration);
        //System.out.println(inputModel.getModel());
        GAMSJob t2 = ws.addJobFromString(inputModel.getModel());
        System.out.println(inputModel.getModel());
        // create GAMSOption "opt" and define "incname" as "tdata"
        GAMSOptions opt = ws.addOptions();
        opt.defines("incname", "tdata");
        // run GAMSJob "t2" with GAMSOptions "opt"
        t2.run(opt);

        // cleanup GAMSWorkspace's working directory
        // cleanup(ws.workingDirectory());
        // terminate program
        this.job = t2;
	}
	
	public String getFirstResultVariable(String variableName) {
		// retrieve GAMSVariable "x" from GAMSJob's output databases
        GAMSVariable var = this.job.OutDB().getVariable(variableName);
        //for (GAMSVariableRecord rec : var)
        //   System.out.println(variableName + "(" + rec.getKey(0) + "): level=" + rec.getLevel() + " marginal=" + rec.getMarginal());
        return new String(String.valueOf(var.getFirstRecord().getLevel()));
	}
	
	public String getFirstResultVariableParameter(String variableName, String parameter) {
		// retrieve GAMSVariable "x" from GAMSJob's output databases
        GAMSVariable var = this.job.OutDB().getVariable(variableName);
        //for (GAMSVariableRecord rec : var)
        //   System.out.println(variableName + "(" + rec.getKey(0) + "): level=" + rec.getLevel() + " marginal=" + rec.getMarginal());
        if (variableName.equals(variableName)) {
        	try {
        	       for (GAMSVariableRecord rec : var)
        	    	   if (rec.getKey(0).equals(parameter)) {
        	    		   //System.out.println(variableName + "(" + rec.getKey(0) + "): level=" + rec.getLevel() + " marginal=" + rec.getMarginal());
        	    		   return new String(String.valueOf(rec.getLevel()));
        	    	   }
        	   } catch (GAMSException e) {
        	       System.out.println("No records found");
        	   }
        }
        return "0.0";
	}
	
	public ArrayList<Double> getResultVariableParameter(String variableName, String parameter) {
		ArrayList<Double> result = new ArrayList<Double>();
		
		GAMSVariable var = this.job.OutDB().getVariable(variableName);
        //for (GAMSVariableRecord rec : var)
        //   System.out.println(variableName + "(" + rec.getKey(0) + "): level=" + rec.getLevel() + " marginal=" + rec.getMarginal());
        if (variableName.equals(variableName)) {
        	try {
        	       for (GAMSVariableRecord rec : var)
        	    	   if (rec.getKey(0).equals(parameter)) {
        	    		   result.add(rec.getLevel());
        	    	   }
        	   } catch (GAMSException e) {
        	       System.out.println("No records found");
        	   }
        }
        return result;
	}
	
	public void printResultVariable(String variableName) {
		// retrieve GAMSVariable "x" from GAMSJob's output databases
        GAMSVariable var = this.job.OutDB().getVariable(variableName);
        for (GAMSVariableRecord rec : var)
           System.out.println(variableName + "(" + rec.getKey(0) + "): level=" + rec.getLevel() + " marginal=" + rec.getMarginal());
	}
	
	public GAMSVariable getVariable(String variableName) {
		GAMSVariable var = this.job.OutDB().getVariable(variableName);
       
        return var;
	}

}
