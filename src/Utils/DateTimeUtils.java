package Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtils {
	public static Integer dateToInterruptionIndex(String dateTime) throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy HH:mm:ss");
		Date horarioInterrupcao = sdf.parse(dateTime);
		Calendar gCalendar = new GregorianCalendar();
		
		gCalendar.setTime(horarioInterrupcao);
		
		Integer interruptionIndex = (gCalendar.get(Calendar.HOUR_OF_DAY) * 60 + gCalendar.get(Calendar.MINUTE)) / 5;
		
		return interruptionIndex;
	}
	
	public static Date getInterruptionDateTime(String dateTime) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy HH:mm:ss");
		Date horarioInterrupcao = sdf.parse(dateTime);
		
		return horarioInterrupcao;
	}

	public static Date addPeriod(Date interruptionStart, int period) {
		// TODO Auto-generated method stub
		Calendar gCalendar = new GregorianCalendar();
		gCalendar.setTime(interruptionStart);
		
		return new Date(gCalendar.getTimeInMillis() + (period * 60000));
	}
}
