package Builders;

import java.util.ArrayList;
import java.util.Date;

import Application.MGConfiguration;
import Data.LoadsData;
import Data.SimulationData;
import Data.WeatherData;
import Model.ComercialLoadModel;
import Model.PhotovoltaicPanel;
import Model.ResidentialLoadModel;
import Model.WindTurbine;
import Utils.DateTimeUtils;

public class SimulationDataBuilder {
	private WeatherData wData;
	private LoadsData lData;
	private Integer interruptTime;
	private Integer interruptPeriod;
	private MGConfiguration config;
	private ArrayList<String> tDelta;
	private Date interruptionStart;
	
	public SimulationDataBuilder(WeatherData wData, LoadsData lData, Integer iTime, Integer iPeriod, MGConfiguration config) {
		this.wData = wData;
		this.lData = lData;
		this.interruptTime = iTime;
		this.interruptPeriod = iPeriod;
		this.config = config;
		tDelta = new ArrayList<String>();
		this.interruptionStart = config.getInterruptionStart();
	}
	
	public SimulationData build(Boolean runFull) {
		WindTurbine wTurbine = new WindTurbine(10.0);
		PhotovoltaicPanel pPanel = new PhotovoltaicPanel(10.0, wData.getMaxIrradiation());
		// Cen�rio Gera��o > Demanda
//		ResidentialLoadModel rLoad1 = new ResidentialLoadModel(4.0);
//		ResidentialLoadModel rLoad2 = new ResidentialLoadModel(3.0);
//		ResidentialLoadModel rLoad3 = new ResidentialLoadModel(2.0);
//		ComercialLoadModel cLoad1 = new ComercialLoadModel(5.0);
		// Cen�rio Demanda > Gera��o
		ResidentialLoadModel rLoad1 = new ResidentialLoadModel(10.0);
		ResidentialLoadModel rLoad2 = new ResidentialLoadModel(8.0);
		ResidentialLoadModel rLoad3 = new ResidentialLoadModel(8.0);
		ComercialLoadModel cLoad1 = new ComercialLoadModel(13.0);
		
		Integer secondIntervalSize = 30;
		
		Double tempValue = 0.0;
		Double tempWValue = 0.0;
		Double tempCValue = 0.0;
		Double tempRValue = 0.0;
		Double variationValue = 0.0;
		
		Integer interval = this.interruptTime;
		Integer timeLeft = this.interruptPeriod;
		Integer intervalUsedCount = 0;
		SimulationData tempData = new SimulationData();
		SimulationData sData = new SimulationData();
		
		
		// runFull = true para executar a primeira vez com discretiza��o de 5 minutos para todo o per�odo
		if (!runFull) {
		
			while (timeLeft > 0) {
				variationValue = Math.random() * config.getVariation();
				tempData.addPowerWindData(new Date(), wTurbine.getGeneration(this.wData.getWind().get(interval).getData().intValue()));
				tempData.addPowerSolarData(new Date(), pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData()));
				tempData.addComercialLoad(new Date(), cLoad1.getGeneration(this.lData.getComercial().get(interval).getData()));
				tempData.addResidentialLowLoad(new Date(), rLoad3.getLoad(this.lData.getResidentialLow().get(interval).getData()));
				tempData.addResidentialMediumLoad(new Date(), rLoad2.getLoad(this.lData.getResidentialMedium().get(interval).getData()));
				tempData.addResidentialHighLoad(new Date(), rLoad1.getLoad(this.lData.getResidentialHigh().get(interval).getData()));
				timeLeft -= 5;
				interval++;
			}
			
			int intervalsUsed = 6;
			int periodLeft = this.interruptPeriod;
			int periodQty = 0;
			boolean firstPeriodIncluded = false;
			boolean secondPeriodIncluded = false;
			boolean thirdPeriodIncluded = false;
			boolean extraPeriod = false;
			
			while (periodLeft > 0) {
				tempValue = 0.0;
				tempWValue = 0.0;
				if (!firstPeriodIncluded) {
					if (this.interruptPeriod >= 30) {
						sData.addTDelta("t1*t6 0.08333");
						for (int i = 0; i < 6; i++) {
							sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
							sData.addPowerWindData(tempData.getPowerWind().get(i).getTimestamp(), tempData.getPowerWind().get(i).getData());
							sData.addComercialLoad(tempData.getComercialLoad().get(i).getTimestamp(), tempData.getComercialLoad().get(i).getData());
							sData.addResidentialHighLoad(tempData.getResidentialHighLoad().get(i).getTimestamp(), tempData.getResidentialHighLoad().get(i).getData());
							sData.addResidentialMediumLoad(tempData.getResidentialMediumLoad().get(i).getTimestamp(), tempData.getResidentialMediumLoad().get(i).getData());
							sData.addResidentialLowLoad(tempData.getResidentialLowLoad().get(i).getTimestamp(), tempData.getResidentialLowLoad().get(i).getData());
						}
						firstPeriodIncluded = true;
						periodLeft -= 30;
					} else {
						sData.addTDelta("t1*t" + (this.interruptPeriod / 5) + " 0.08333");
						for (int i = 0; i < (this.interruptPeriod / 5); i++) {
							sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
							sData.addPowerWindData(tempData.getPowerWind().get(i).getTimestamp(), tempData.getPowerWind().get(i).getData());
							sData.addComercialLoad(tempData.getComercialLoad().get(i).getTimestamp(), tempData.getComercialLoad().get(i).getData());
							sData.addResidentialHighLoad(tempData.getResidentialHighLoad().get(i).getTimestamp(), tempData.getResidentialHighLoad().get(i).getData());
							sData.addResidentialMediumLoad(tempData.getResidentialMediumLoad().get(i).getTimestamp(), tempData.getResidentialMediumLoad().get(i).getData());
							sData.addResidentialLowLoad(tempData.getResidentialLowLoad().get(i).getTimestamp(), tempData.getResidentialLowLoad().get(i).getData());
						}
						firstPeriodIncluded = true;
						periodLeft = 0;
					}
				}
				if (!secondPeriodIncluded && periodLeft > 0) {
					// Para os casos em que o periodo e menor que 30 minutos
					if (periodLeft <= 30) {
						//sData.addTDelta("t7 " + ( ((interruptPeriod - 30) / 5) * 0.083333));
						sData.addTDelta("t7 " + String.format("%2.4f", ( ((interruptPeriod - 30) / 5) * 0.083333)).replace(",", ".") );
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / ((interruptPeriod - 30) / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (periodLeft / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (periodLeft / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						secondPeriodIncluded = true;
						periodLeft -= periodLeft % 60;
						intervalsUsed += periodLeft / 5;
					} else if (periodLeft <= 90) {
						if (periodLeft - 30 < 30) {
							sData.addTDelta("t7 " + String.format("%2.4f", ( ((periodLeft - 30) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft - 30) / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft - 30) / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft - 30) / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft - 30) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft - 30) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft - 30) / 5));
							tempRValue = 0.0;						
							
							intervalsUsed += (periodLeft - 30) / 5;
							// Inclus�o do segundo per�odo
							sData.addTDelta("t8 " + String.format("%2.4f", ( (30 / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (30 / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (30 / 5));
							
							for (int i = 0; i < (30 / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (30 / 5));
							
							for (int i = 0; i < (30 / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (30 / 5));
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft;
							intervalsUsed += (30 / 5);
						} else {
							// Para os casos maiores igual que 60 minutos restantes
							sData.addTDelta("t7 " + String.format("%2.4f", ( (30 / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (30 / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (30 / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (30 / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (30 / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							intervalsUsed += (30 / 5);
							// Inclus�o do segundo per�odo
							sData.addTDelta("t8 " + String.format("%2.4f", ( ((periodLeft - 30) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft - 30) / 5));
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft - 30) / 5));
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft - 30) / 5));
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft - 30) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft - 30) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - 30) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft - 30) / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft;
							intervalsUsed += ((periodLeft - 30) / 5);
						}
					// Casos maiores que 90 minutos
					} else if ((periodLeft % 60) > 0) {
						if ((periodLeft % 60) < 30) {
							sData.addTDelta("t7 " + String.format("%2.4f", ( ((periodLeft % 60) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft % 60) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft % 60) / 5));
							
							for (int i = 0; i < ((periodLeft % 60) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft % 60) / 5));
							
							for (int i = 0; i < ((periodLeft % 60) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft % 60) / 5));
							
							for (int i = 0; i < ((periodLeft % 60) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft % 60) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft % 60) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft % 60) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft % 60) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft % 60) / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft % 60;
							intervalsUsed += ((periodLeft % 60) / 5);
						} else {
							sData.addTDelta("t7 " + String.format("%2.4f", ( (((periodLeft % 60) - 30) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (((periodLeft % 60) - 30) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (((periodLeft % 60) - 30) / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % 60) - 30) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (((periodLeft % 60) - 30) / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % 60) - 30) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (((periodLeft % 60) - 30) / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % 60) - 30) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (((periodLeft % 60) - 30) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % 60) - 30) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (((periodLeft % 60) - 30) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % 60) - 30) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (((periodLeft % 60) - 30) / 5));
							tempRValue = 0.0;
							
							intervalsUsed += ((periodLeft % 60) - 30) / 5;
							// Inclus�o do segundo per�odo
							sData.addTDelta("t8 " + String.format("%2.4f", ( (30 / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (30 / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (30 / 5));
							
							for (int i = 0; i < (30 / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (30 / 5));
							
							for (int i = 0; i < (30 / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (30 / 5));
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (30 / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (30 / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft % 60;
							intervalsUsed += (30 / 5);
							extraPeriod = true;
						}
					//Caso onde o per�odo restante � m�ltiplo de 60
					} else if ((periodLeft % 60 == 0) && (periodLeft / 60 >= 1)) {
						sData.addTDelta("t7 " + String.format("%2.4f", ( (30 / 5) * 0.083333)).replace(",", ".") );
						
						for (int i = 0; i < (30 / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / (30 / 5));
						tempValue = 0.0;
						
						for (int i = 0; i < (30 / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (30 / 5));
						tempWValue = 0.0;
						
						for (int i = 0; i < (30 / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (30 / 5));
						tempCValue = 0.0;
						
						for (int i = 0; i < (30 / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (30 / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (30 / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (30 / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (30 / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (30 / 5));
						tempRValue = 0.0;
						
						intervalsUsed += (30 / 5);
						// Inclus�o do segundo intervalo
						sData.addTDelta("t8 " + String.format("%2.4f", ( (30 / 5) * 0.083333)).replace(",", ".") );
						
						for (int i = 0; i < (30 / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / (30 / 5));
						
						for (int i = 0; i < (30 / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (30 / 5));
						
						for (int i = 0; i < (30 / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (30 / 5));
						
						for (int i = 0; i < (30 / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (30 / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (30 / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (30 / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (30 / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (30 / 5));
						tempRValue = 0.0;
						
						secondPeriodIncluded = true;
						periodLeft -= 60;
						intervalsUsed += (30 / 5);
						extraPeriod = true;
					}
				}
				tempValue = 0.0;
				tempWValue = 0.0;
				tempCValue = 0.0;
				tempRValue = 0.0;
				
				// Caso foi inserido um per�odo intermedi�rio adicional
				Integer additionalPeriod = 8;
				if (extraPeriod) {
					additionalPeriod = 9;
				}
				
				if (firstPeriodIncluded && secondPeriodIncluded && periodLeft > 0) {
					int intervalAux = intervalsUsed;
					periodQty = periodLeft / 60;
					if (periodQty > 1) {
						sData.addTDelta("t" + additionalPeriod + "*t" + (periodQty * 1 + (additionalPeriod - 1)) + " 1");
					} else {
						sData.addTDelta("t" + additionalPeriod + " 1");
					}
					
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < 12; j++) {
							tempValue += tempData.getPowerSolar().get(intervalAux + j).getData();
						}
						intervalAux += 12;
						sData.addPowerSolarData(new Date(), tempValue / 12);
						tempValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < 12; j++) {
							tempWValue += tempData.getPowerWind().get(intervalAux + j).getData();
						}
						intervalAux += 12;
						sData.addPowerWindData(new Date(), tempWValue / 12);
						tempWValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < 12; j++) {
							tempCValue += tempData.getComercialLoad().get(intervalAux + j).getData();
						}
						intervalAux += 12;
						sData.addComercialLoad(new Date(), tempCValue / 12);
						tempCValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < 12; j++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalAux + j).getData();
						}
						intervalAux += 12;
						sData.addResidentialLowLoad(new Date(), tempRValue / 12);
						tempRValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < 12; j++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalAux + j).getData();
						}
						intervalAux += 12;
						sData.addResidentialMediumLoad(new Date(), tempRValue / 12);
						tempRValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < 12; j++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalAux + j).getData();
						}
						intervalAux += 12;
						sData.addResidentialHighLoad(new Date(), tempRValue / 12);
						tempRValue = 0.0;
					}
					
					periodLeft = 0;
				}
			} 
		} else {
			while (timeLeft > 0) {
				variationValue = Math.random() * config.getVariation();
				sData.addPowerWindData(new Date(), wTurbine.getGeneration(this.wData.getWind().get(interval).getData().intValue()));
				sData.addPowerSolarData(new Date(), pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData()));
				sData.addComercialLoad(new Date(), cLoad1.getGeneration(this.lData.getComercial().get(interval).getData()));
				sData.addResidentialLowLoad(new Date(), rLoad3.getLoad(this.lData.getResidentialLow().get(interval).getData()));
				sData.addResidentialMediumLoad(new Date(), rLoad2.getLoad(this.lData.getResidentialMedium().get(interval).getData()));
				sData.addResidentialHighLoad(new Date(), rLoad1.getLoad(this.lData.getResidentialHigh().get(interval).getData()));
				timeLeft -= 5;
				interval++;
				intervalUsedCount++;
			}
			
			sData.addTDelta("t1*t" + intervalUsedCount + " 0.08333");
			System.out.println(sData.getTHeader());
			
		}
		
		return sData;
	}
	
	public SimulationData buildDinamic(Boolean runFull) {
		WindTurbine wTurbine = new WindTurbine(10.0);
		PhotovoltaicPanel pPanel = new PhotovoltaicPanel(10.0, wData.getMaxIrradiation());
		// Cen�rio Gera��o > Demanda
//		ResidentialLoadModel rLoad1 = new ResidentialLoadModel(8.0);
//		ResidentialLoadModel rLoad2 = new ResidentialLoadModel(10.0);
//		ResidentialLoadModel rLoad3 = new ResidentialLoadModel(10.0);
//		ComercialLoadModel cLoad1 = new ComercialLoadModel(12.0);
		// Cen�rio Demanda > Gera��o
		ResidentialLoadModel rLoad1 = new ResidentialLoadModel(10.0);
		ResidentialLoadModel rLoad2 = new ResidentialLoadModel(18.0);
		ResidentialLoadModel rLoad3 = new ResidentialLoadModel(13.0);
		ComercialLoadModel cLoad1 = new ComercialLoadModel(13.0);
		// Cen�rio Demanda > Gera��o (2 motores diesel)
//		ResidentialLoadModel rLoad1 = new ResidentialLoadModel(50.0);
//		ResidentialLoadModel rLoad2 = new ResidentialLoadModel(48.0);
//		ResidentialLoadModel rLoad3 = new ResidentialLoadModel(53.0);
//		ComercialLoadModel cLoad1 = new ComercialLoadModel(43.0);

		// Cen�rio Testes Algoritmo
//		ResidentialLoadModel rLoad1 = new ResidentialLoadModel(70.0);
//		ResidentialLoadModel rLoad2 = new ResidentialLoadModel(0.0);
//		ResidentialLoadModel rLoad3 = new ResidentialLoadModel(0.0);
//		ComercialLoadModel cLoad1 = new ComercialLoadModel(0.0);
		
		Integer firstIntervalPeriods = config.getFirstIntervalPeriods();
		Integer secondIntervalSize = config.getMiddlePeriodInterval();
		Integer finalIntervalSize = config.getFinalPeriodInterval();
		
		Double tempValue = 0.0;
		Double tempWValue = 0.0;
		Double tempCValue = 0.0;
		Double tempRValue = 0.0;
		Double variationValue = 0.0;
		
		Integer interval = this.interruptTime;
		Integer timeLeft = this.interruptPeriod;
		Integer intervalUsedCount = 0;
		SimulationData tempData = new SimulationData();
		SimulationData sData = new SimulationData();
		
		// runFull = true para executar a primeira vez com discretiza��o de 5 minutos para todo o per�odo
		if (!runFull) {
		
//			while (timeLeft > 0) {
//				variationValue = Math.random() * config.getVariation();
//				tempData.addPowerWindData(new Date(), wTurbine.getGeneration(this.wData.getWind().get(interval).getData().intValue()));
//				tempData.addPowerSolarData(new Date(), pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData()));
//				tempData.addComercialLoad(new Date(), cLoad1.getGeneration(this.lData.getComercial().get(interval).getData()));
//				tempData.addResidentialLowLoad(new Date(), rLoad3.getGeneration(this.lData.getResidentialLow().get(interval).getData()));
//				tempData.addResidentialMediumLoad(new Date(), rLoad2.getGeneration(this.lData.getResidentialMedium().get(interval).getData()));
//				tempData.addResidentialHighLoad(new Date(), rLoad1.getGeneration(this.lData.getResidentialHigh().get(interval).getData()));
//				timeLeft -= 5;
//				interval++;
//			}
			
			// Com utiliza��o do hor�rio de in�cio da interrup��o
			while (timeLeft > 0) {
				variationValue = Math.random() * config.getVariation();
				tempData.addPowerWindData(this.interruptionStart, wTurbine.getGeneration(this.wData.getWind().get(interval).getData().intValue()));
				tempData.addPowerSolarData(this.interruptionStart, pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData()));
				tempData.addComercialLoad(this.interruptionStart, cLoad1.getGeneration(this.lData.getComercial().get(interval).getData()));
				tempData.addResidentialLowLoad(this.interruptionStart, rLoad3.getLoad(this.lData.getResidentialLow().get(interval).getData()));
				tempData.addResidentialMediumLoad(this.interruptionStart, rLoad2.getLoad(this.lData.getResidentialMedium().get(interval).getData()));
				tempData.addResidentialHighLoad(this.interruptionStart, rLoad1.getLoad(this.lData.getResidentialHigh().get(interval).getData()));
				timeLeft -= 5;
				interval++;
				this.interruptionStart = DateTimeUtils.addPeriod(this.interruptionStart, 5);
			}
			
			int intervalsUsed = firstIntervalPeriods;
			int periodLeft = this.interruptPeriod;
			int periodQty = 0;
			boolean firstPeriodIncluded = false;
			boolean secondPeriodIncluded = false;
			boolean thirdPeriodIncluded = false;
			boolean extraPeriod = false;
			
			while (periodLeft > 0) {
				tempValue = 0.0;
				tempWValue = 0.0;
				if (!firstPeriodIncluded) {
					if (this.interruptPeriod >= (firstIntervalPeriods * 5)) {
						sData.addTDelta("t1*t" + firstIntervalPeriods + " 0.08333");
						for (int i = 0; i < firstIntervalPeriods; i++) {
							sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
							sData.addPowerWindData(tempData.getPowerWind().get(i).getTimestamp(), tempData.getPowerWind().get(i).getData());
							sData.addComercialLoad(tempData.getComercialLoad().get(i).getTimestamp(), tempData.getComercialLoad().get(i).getData());
							sData.addResidentialHighLoad(tempData.getResidentialHighLoad().get(i).getTimestamp(), tempData.getResidentialHighLoad().get(i).getData());
							sData.addResidentialMediumLoad(tempData.getResidentialMediumLoad().get(i).getTimestamp(), tempData.getResidentialMediumLoad().get(i).getData());
							sData.addResidentialLowLoad(tempData.getResidentialLowLoad().get(i).getTimestamp(), tempData.getResidentialLowLoad().get(i).getData());
						}
						firstPeriodIncluded = true;
						periodLeft -= firstIntervalPeriods * 5;
					} else {
						sData.addTDelta("t1*t" + (this.interruptPeriod / 5) + " 0.08333");
						for (int i = 0; i < (this.interruptPeriod / 5); i++) {
							sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
							sData.addPowerWindData(tempData.getPowerWind().get(i).getTimestamp(), tempData.getPowerWind().get(i).getData());
							sData.addComercialLoad(tempData.getComercialLoad().get(i).getTimestamp(), tempData.getComercialLoad().get(i).getData());
							sData.addResidentialHighLoad(tempData.getResidentialHighLoad().get(i).getTimestamp(), tempData.getResidentialHighLoad().get(i).getData());
							sData.addResidentialMediumLoad(tempData.getResidentialMediumLoad().get(i).getTimestamp(), tempData.getResidentialMediumLoad().get(i).getData());
							sData.addResidentialLowLoad(tempData.getResidentialLowLoad().get(i).getTimestamp(), tempData.getResidentialLowLoad().get(i).getData());
						}
						firstPeriodIncluded = true;
						periodLeft -= firstIntervalPeriods * 5;
					}
				}
				if (!secondPeriodIncluded && periodLeft > 0) {
					// Para os casos em que o periodo e menor que 30 minutos
					if (periodLeft <= secondIntervalSize) {
						//sData.addTDelta("t7 " + ( ((interruptPeriod - 30) / 5) * 0.083333));
						//if (secondIntervalSize != 5) {
						//if (secondIntervalSize != periodLeft) {
						//	sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%3.5f", ( ((secondIntervalSize - (firstIntervalPeriods * 5)) / 5) * 0.083333)).replace(",", ".") );
						//} else {
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%3.5f", 0.083333).replace(",", ".") );
						//}
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / (periodLeft / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (periodLeft / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (periodLeft / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						secondPeriodIncluded = true;
						if (secondIntervalSize != 5) {
							periodLeft -= periodLeft % finalIntervalSize;
						} else {
							periodLeft = 0;
						}
						intervalsUsed += periodLeft / 5;
					} else if (periodLeft <= (secondIntervalSize + finalIntervalSize)) {
						if (periodLeft - secondIntervalSize <= secondIntervalSize) {
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%3.5f", ( ((periodLeft - secondIntervalSize) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft - secondIntervalSize) / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft - secondIntervalSize) / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft - secondIntervalSize) / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;						
							
							intervalsUsed += (periodLeft - secondIntervalSize) / 5;
							periodLeft -= (periodLeft - secondIntervalSize);
							// TODO Inclusao do if
							//if ((periodLeft - secondIntervalSize) >= secondIntervalSize) {
							if (periodLeft >= secondIntervalSize) {
								// Inclus�o do segundo per�odo
								sData.addTDelta("t" + (firstIntervalPeriods + 2) + " " + String.format("%3.5f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
								
								for (int i = 0; i < (secondIntervalSize / 5); i++) {
									tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
								}
								sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
								
								for (int i = 0; i < (secondIntervalSize / 5); i++) {
									tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
								}
								sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
								
								for (int i = 0; i < (secondIntervalSize / 5); i++) {
									tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
								}
								sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
								
								for (int i = 0; i < (secondIntervalSize / 5); i++) {
									tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
								}
								sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
								tempRValue = 0.0;
								
								for (int i = 0; i < (secondIntervalSize / 5); i++) {
									tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
								}
								sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
								tempRValue = 0.0;
								
								for (int i = 0; i < (secondIntervalSize / 5); i++) {
									tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
								}
								sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
								tempRValue = 0.0;
								
								periodLeft -= periodLeft;
								intervalsUsed += (secondIntervalSize / 5);
							}
							
							secondPeriodIncluded = true;
							// TODO comentado
							//periodLeft -= periodLeft;
							//intervalsUsed += (secondIntervalSize / 5);
						} else {
							// Para os casos maiores igual que 60 minutos restantes
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%3.5f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							intervalsUsed += (secondIntervalSize / 5);
							// Inclus�o do segundo per�odo
							sData.addTDelta("t" + (firstIntervalPeriods + 2) + " " + String.format("%3.5f", ( ((periodLeft - secondIntervalSize) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft - secondIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft - secondIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft - secondIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft;
							intervalsUsed += ((periodLeft - secondIntervalSize) / 5);
						}
					// Casos maiores que 90 minutos
					} else if ((periodLeft % finalIntervalSize) > 0) {
						if ((periodLeft % finalIntervalSize) <= secondIntervalSize) {
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%3.5f", ( ((periodLeft % finalIntervalSize) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft % finalIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft % finalIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft % finalIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft % finalIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft % finalIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft % finalIntervalSize) / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft % finalIntervalSize;
							intervalsUsed += ((periodLeft % finalIntervalSize) / 5);
						} else {
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%3.5f", ( (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							intervalsUsed += ((periodLeft % finalIntervalSize) - secondIntervalSize) / 5;
							// Inclus�o do segundo per�odo
							sData.addTDelta("t" + (firstIntervalPeriods + 2) + " " + String.format("%3.5f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft % finalIntervalSize;
							intervalsUsed += (secondIntervalSize / 5);
							extraPeriod = true;
						}
					//Caso onde o per�odo restante � m�ltiplo de 60
					} else if ((periodLeft % finalIntervalSize == 0) && (periodLeft / finalIntervalSize >= 1)) {
						sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%3.5f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
						tempValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
						tempWValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
						tempCValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						intervalsUsed += (secondIntervalSize / 5);
						// Inclus�o do segundo intervalo
						sData.addTDelta("t" + (firstIntervalPeriods + 2) + " " + String.format("%3.5f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						secondPeriodIncluded = true;
						periodLeft -= finalIntervalSize;
						intervalsUsed += (secondIntervalSize / 5);
						extraPeriod = true;
					}
				}
				tempValue = 0.0;
				tempWValue = 0.0;
				tempCValue = 0.0;
				tempRValue = 0.0;
				
				// Caso foi inserido um per�odo intermedi�rio adicional
				Integer additionalPeriod = firstIntervalPeriods + 2;
				if (extraPeriod) {
					additionalPeriod = firstIntervalPeriods + 3;
				}
				
				if (firstPeriodIncluded && secondPeriodIncluded && periodLeft > 0) {
					int intervalAux = intervalsUsed;
					periodQty = periodLeft / finalIntervalSize;
					if (periodQty > 1) {
						if (finalIntervalSize != 5) {
							sData.addTDelta("t" + additionalPeriod + "*t" + (periodQty * 1 + (additionalPeriod - 1)) + " " + String.format("%3.5f ", (finalIntervalSize / 5) * 0.083333).replace(",", "."));
						} else {
							periodQty--;
							sData.addTDelta("t" + additionalPeriod + "*t" + (periodQty * 1 + (additionalPeriod - 1)) + " " + String.format("%3.5f ", (finalIntervalSize / 5) * 0.083333).replace(",", "."));
						}
					} else {
						if (finalIntervalSize == 5) {
							periodQty--;
						}
						sData.addTDelta("t" + additionalPeriod + " " + String.format("%3.5f ",(finalIntervalSize / 5) * 0.083333).replace(",", "."));
					}
					
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempValue += tempData.getPowerSolar().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addPowerSolarData(new Date(), tempValue / (finalIntervalSize / 5));
						tempValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempWValue += tempData.getPowerWind().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addPowerWindData(new Date(), tempWValue / (finalIntervalSize / 5));
						tempWValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempCValue += tempData.getComercialLoad().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addComercialLoad(new Date(), tempCValue / (finalIntervalSize / 5));
						tempCValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addResidentialLowLoad(new Date(), tempRValue / (finalIntervalSize / 5));
						tempRValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addResidentialMediumLoad(new Date(), tempRValue / (finalIntervalSize / 5));
						tempRValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addResidentialHighLoad(new Date(), tempRValue / (finalIntervalSize / 5));
						tempRValue = 0.0;
					}
					
					periodLeft = 0;
				}
			} 
		} else {
//			while (timeLeft > 0) {
//				sData.addPowerWindData(new Date(), wTurbine.getGeneration(new Double(this.wData.getWind().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)).intValue()));
//				sData.addPowerSolarData(new Date(), pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				sData.addComercialLoad(new Date(), cLoad1.getGeneration(this.lData.getComercial().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				sData.addResidentialLowLoad(new Date(), rLoad3.getGeneration(this.lData.getResidentialLow().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				sData.addResidentialMediumLoad(new Date(), rLoad2.getGeneration(this.lData.getResidentialMedium().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				sData.addResidentialHighLoad(new Date(), rLoad1.getGeneration(this.lData.getResidentialHigh().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				timeLeft -= 5;
//				interval++;
//				intervalUsedCount++;
//			}
			
			// Com utiliza��o do hor�rio de in�cio da interrup��o
			while (timeLeft > 0) {
				sData.addPowerWindData(this.interruptionStart, wTurbine.getGeneration(new Double(this.wData.getWind().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)).intValue()));
				sData.addPowerSolarData(this.interruptionStart, pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
				sData.addComercialLoad(this.interruptionStart, cLoad1.getGeneration(this.lData.getComercial().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
				sData.addResidentialLowLoad(this.interruptionStart, rLoad3.getLoad(this.lData.getResidentialLow().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
				sData.addResidentialMediumLoad(this.interruptionStart, rLoad2.getLoad(this.lData.getResidentialMedium().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
				sData.addResidentialHighLoad(this.interruptionStart, rLoad1.getLoad(this.lData.getResidentialHigh().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
				timeLeft -= 5;
				interval++;
				intervalUsedCount++;
				this.interruptionStart = DateTimeUtils.addPeriod(this.interruptionStart, 5);
			}
			
			sData.addTDelta("t1*t" + intervalUsedCount + " 0.08333");
			System.out.println(sData.getTHeader());
			
		}
		
		return sData;
	}
	
	public SimulationData buildDinamicEqualDemand(Boolean runFull) {
		WindTurbine wTurbine = new WindTurbine(10.0);
		PhotovoltaicPanel pPanel = new PhotovoltaicPanel(10.0, wData.getMaxIrradiation());
		// Cen�rio Gera��o > Demanda
//		ResidentialLoadModel rLoad1 = new ResidentialLoadModel(4.0);
//		ResidentialLoadModel rLoad2 = new ResidentialLoadModel(3.0);
//		ResidentialLoadModel rLoad3 = new ResidentialLoadModel(2.0);
//		ComercialLoadModel cLoad1 = new ComercialLoadModel(5.0);
		// Cen�rio Demanda > Gera��o
		ResidentialLoadModel rLoad1 = new ResidentialLoadModel(10.0);
		ResidentialLoadModel rLoad2 = new ResidentialLoadModel(8.0);
		ResidentialLoadModel rLoad3 = new ResidentialLoadModel(8.0);
		ComercialLoadModel cLoad1 = new ComercialLoadModel(13.0);
		
		Integer firstIntervalPeriods = config.getFirstIntervalPeriods();
		Integer secondIntervalSize = config.getMiddlePeriodInterval();
		Integer finalIntervalSize = config.getFinalPeriodInterval();
		
		Double tempValue = 0.0;
		Double tempWValue = 0.0;
		Double tempCValue = 0.0;
		Double tempRValue = 0.0;
		Double variationValue = 0.0;
		
		Integer interval = this.interruptTime;
		Integer timeLeft = this.interruptPeriod;
		Integer intervalUsedCount = 0;
		SimulationData tempData = new SimulationData();
		SimulationData sData = new SimulationData();
		
		
		// runFull = true para executar a primeira vez com discretiza��o de 5 minutos para todo o per�odo
		if (!runFull) {
		
			while (timeLeft > 0) {
				variationValue = Math.random() * config.getVariation();
//				tempData.addPowerWindData(new Date(), wTurbine.getGeneration(this.wData.getWind().get(interval).getData().intValue()));
//				tempData.addPowerSolarData(new Date(), pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData()));
				//tempData.addComercialLoad(new Date(), cLoad1.getGeneration(this.lData.getComercial().get(interval).getData()));
				//tempData.addResidentialLowLoad(new Date(), rLoad3.getGeneration(this.lData.getResidentialLow().get(interval).getData()));
				//tempData.addResidentialMediumLoad(new Date(), rLoad2.getGeneration(this.lData.getResidentialMedium().get(interval).getData()));
				//tempData.addResidentialHighLoad(new Date(), rLoad1.getGeneration(this.lData.getResidentialHigh().get(interval).getData()));
				tempData.addPowerWindData(new Date(), wTurbine.getEqualGeneration(15));
				tempData.addPowerSolarData(new Date(), pPanel.getEqualGeneration(500.0));
				tempData.addComercialLoad(new Date(), cLoad1.getEqualDemand(2.0));
				tempData.addResidentialLowLoad(new Date(), rLoad3.getEqualDemand(2.0));
				tempData.addResidentialMediumLoad(new Date(), rLoad2.getEqualDemand(2.0));
				tempData.addResidentialHighLoad(new Date(), rLoad1.getEqualDemand(2.0));
				timeLeft -= 5;
				interval++;
			}
			
			int intervalsUsed = firstIntervalPeriods;
			int periodLeft = this.interruptPeriod;
			int periodQty = 0;
			boolean firstPeriodIncluded = false;
			boolean secondPeriodIncluded = false;
			boolean thirdPeriodIncluded = false;
			boolean extraPeriod = false;
			
			while (periodLeft > 0) {
				tempValue = 0.0;
				tempWValue = 0.0;
				if (!firstPeriodIncluded) {
					if (this.interruptPeriod >= (firstIntervalPeriods * 5)) {
						sData.addTDelta("t1*t" + firstIntervalPeriods + " 0.08333");
						for (int i = 0; i < firstIntervalPeriods; i++) {
							sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
							sData.addPowerWindData(tempData.getPowerWind().get(i).getTimestamp(), tempData.getPowerWind().get(i).getData());
							sData.addComercialLoad(tempData.getComercialLoad().get(i).getTimestamp(), tempData.getComercialLoad().get(i).getData());
							sData.addResidentialHighLoad(tempData.getResidentialHighLoad().get(i).getTimestamp(), tempData.getResidentialHighLoad().get(i).getData());
							sData.addResidentialMediumLoad(tempData.getResidentialMediumLoad().get(i).getTimestamp(), tempData.getResidentialMediumLoad().get(i).getData());
							sData.addResidentialLowLoad(tempData.getResidentialLowLoad().get(i).getTimestamp(), tempData.getResidentialLowLoad().get(i).getData());
						}
						firstPeriodIncluded = true;
						periodLeft -= firstIntervalPeriods * 5;
					} else {
						sData.addTDelta("t1*t" + (this.interruptPeriod / 5) + " 0.08333");
						for (int i = 0; i < (this.interruptPeriod / 5); i++) {
							sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
							sData.addPowerWindData(tempData.getPowerWind().get(i).getTimestamp(), tempData.getPowerWind().get(i).getData());
							sData.addComercialLoad(tempData.getComercialLoad().get(i).getTimestamp(), tempData.getComercialLoad().get(i).getData());
							sData.addResidentialHighLoad(tempData.getResidentialHighLoad().get(i).getTimestamp(), tempData.getResidentialHighLoad().get(i).getData());
							sData.addResidentialMediumLoad(tempData.getResidentialMediumLoad().get(i).getTimestamp(), tempData.getResidentialMediumLoad().get(i).getData());
							sData.addResidentialLowLoad(tempData.getResidentialLowLoad().get(i).getTimestamp(), tempData.getResidentialLowLoad().get(i).getData());
						}
						firstPeriodIncluded = true;
						periodLeft -= firstIntervalPeriods * 5;
					}
				}
				if (!secondPeriodIncluded && periodLeft > 0) {
					// Para os casos em que o periodo e menor que 30 minutos
					if (periodLeft <= secondIntervalSize) {
						//sData.addTDelta("t7 " + ( ((interruptPeriod - 30) / 5) * 0.083333));
						sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%2.4f", ( ((secondIntervalSize - (firstIntervalPeriods * 5)) / 5) * 0.083333)).replace(",", ".") );
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / (periodLeft / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (periodLeft / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (periodLeft / 5));
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (periodLeft / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (periodLeft / 5));
						tempRValue = 0.0;
						
						secondPeriodIncluded = true;
						periodLeft -= periodLeft % finalIntervalSize;
						intervalsUsed += periodLeft / 5;
					} else if (periodLeft <= (secondIntervalSize + finalIntervalSize)) {
						if (periodLeft - secondIntervalSize < secondIntervalSize) {
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%2.4f", ( ((periodLeft - secondIntervalSize) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft - secondIntervalSize) / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft - secondIntervalSize) / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft - secondIntervalSize) / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;						
							
							intervalsUsed += (periodLeft - secondIntervalSize) / 5;
							// Inclus�o do segundo per�odo
							sData.addTDelta("t" + (firstIntervalPeriods + 2) + " " + String.format("%2.4f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft;
							intervalsUsed += (secondIntervalSize / 5);
						} else {
							// Para os casos maiores igual que 60 minutos restantes
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%2.4f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							intervalsUsed += (secondIntervalSize / 5);
							// Inclus�o do segundo per�odo
							sData.addTDelta("t" + (firstIntervalPeriods + 2) + " " + String.format("%2.4f", ( ((periodLeft - secondIntervalSize) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft - secondIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft - secondIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft - secondIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft;
							intervalsUsed += ((periodLeft - secondIntervalSize) / 5);
						}
					// Casos maiores que 90 minutos
					} else if ((periodLeft % finalIntervalSize) > 0) {
						if ((periodLeft % finalIntervalSize) <= secondIntervalSize) {
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%2.4f", ( ((periodLeft % finalIntervalSize) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / ((periodLeft % finalIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / ((periodLeft % finalIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / ((periodLeft % finalIntervalSize) / 5));
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / ((periodLeft % finalIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / ((periodLeft % finalIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < ((periodLeft % finalIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / ((periodLeft % finalIntervalSize) / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft % finalIntervalSize;
							intervalsUsed += ((periodLeft % finalIntervalSize) / 5);
						} else {
							sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%2.4f", ( (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempWValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempCValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (((periodLeft % finalIntervalSize) - secondIntervalSize) / 5));
							tempRValue = 0.0;
							
							intervalsUsed += ((periodLeft % finalIntervalSize) - secondIntervalSize) / 5;
							// Inclus�o do segundo per�odo
							sData.addTDelta("t" + (firstIntervalPeriods + 2) + " " + String.format("%2.4f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
							}
							sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
							}
							sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
							}
							sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							for (int i = 0; i < (secondIntervalSize / 5); i++) {
								tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
							}
							sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
							tempRValue = 0.0;
							
							secondPeriodIncluded = true;
							periodLeft -= periodLeft % finalIntervalSize;
							intervalsUsed += (secondIntervalSize / 5);
							extraPeriod = true;
						}
					//Caso onde o per�odo restante � m�ltiplo de 60
					} else if ((periodLeft % finalIntervalSize == 0) && (periodLeft / finalIntervalSize >= 1)) {
						sData.addTDelta("t" + (firstIntervalPeriods + 1) + " " + String.format("%2.4f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
						tempValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
						tempWValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
						tempCValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						intervalsUsed += (secondIntervalSize / 5);
						// Inclus�o do segundo intervalo
						sData.addTDelta("t" + (firstIntervalPeriods + 2) + " " + String.format("%2.4f", ( (secondIntervalSize / 5) * 0.083333)).replace(",", ".") );
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempValue += tempData.getPowerSolar().get(intervalsUsed + i).getData();
						}
						sData.addPowerSolarData(new Date(), tempValue / (secondIntervalSize / 5));
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempWValue += tempData.getPowerWind().get(intervalsUsed + i).getData();
						}
						sData.addPowerWindData(new Date(), tempWValue / (secondIntervalSize / 5));
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempCValue += tempData.getComercialLoad().get(intervalsUsed + i).getData();
						}
						sData.addComercialLoad(new Date(), tempCValue / (secondIntervalSize / 5));
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialLowLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialMediumLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						for (int i = 0; i < (secondIntervalSize / 5); i++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalsUsed + i).getData();
						}
						sData.addResidentialHighLoad(new Date(), tempRValue / (secondIntervalSize / 5));
						tempRValue = 0.0;
						
						secondPeriodIncluded = true;
						periodLeft -= finalIntervalSize;
						intervalsUsed += (secondIntervalSize / 5);
						extraPeriod = true;
					}
				}
				tempValue = 0.0;
				tempWValue = 0.0;
				tempCValue = 0.0;
				tempRValue = 0.0;
				
				// Caso foi inserido um per�odo intermedi�rio adicional
				Integer additionalPeriod = firstIntervalPeriods + 2;
				if (extraPeriod) {
					additionalPeriod = firstIntervalPeriods + 3;
				}
				
				if (firstPeriodIncluded && secondPeriodIncluded && periodLeft > 0) {
					int intervalAux = intervalsUsed;
					periodQty = periodLeft / finalIntervalSize;
					if (periodQty > 1) {
						sData.addTDelta("t" + additionalPeriod + "*t" + (periodQty * 1 + (additionalPeriod - 1)) + " " + String.format("%3.2f ", (finalIntervalSize / 5) * 0.083333).replace(",", "."));
					} else {
						sData.addTDelta("t" + additionalPeriod + " " + String.format("%3.2f ",(finalIntervalSize / 5) * 0.083333).replace(",", "."));
					}
					
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempValue += tempData.getPowerSolar().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addPowerSolarData(new Date(), tempValue / (finalIntervalSize / 5));
						tempValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempWValue += tempData.getPowerWind().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addPowerWindData(new Date(), tempWValue / (finalIntervalSize / 5));
						tempWValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempCValue += tempData.getComercialLoad().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addComercialLoad(new Date(), tempCValue / (finalIntervalSize / 5));
						tempCValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempRValue += tempData.getResidentialLowLoad().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addResidentialLowLoad(new Date(), tempRValue / (finalIntervalSize / 5));
						tempRValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempRValue += tempData.getResidentialMediumLoad().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addResidentialMediumLoad(new Date(), tempRValue / (finalIntervalSize / 5));
						tempRValue = 0.0;
					}
					
					intervalAux = intervalsUsed;
					for (int i = 0; i < periodQty; i++) {
						for (int j = 0; j < (finalIntervalSize / 5); j++) {
							tempRValue += tempData.getResidentialHighLoad().get(intervalAux + j).getData();
						}
						intervalAux += (finalIntervalSize / 5);
						sData.addResidentialHighLoad(new Date(), tempRValue / (finalIntervalSize / 5));
						tempRValue = 0.0;
					}
					
					periodLeft = 0;
				}
			} 
		} else {
			while (timeLeft > 0) {
				//sData.addPowerWindData(new Date(), wTurbine.getGeneration(new Double(this.wData.getWind().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)).intValue()));
				//sData.addPowerSolarData(new Date(), pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				sData.addComercialLoad(new Date(), cLoad1.getGeneration(this.lData.getComercial().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				sData.addResidentialLowLoad(new Date(), rLoad3.getGeneration(this.lData.getResidentialLow().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				sData.addResidentialMediumLoad(new Date(), rLoad2.getGeneration(this.lData.getResidentialMedium().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
//				sData.addResidentialHighLoad(new Date(), rLoad1.getGeneration(this.lData.getResidentialHigh().get(interval).getData() * ((Math.random() * config.getVariation() / 100) + 1)));
				sData.addPowerWindData(new Date(), wTurbine.getEqualGeneration(15));
				sData.addPowerSolarData(new Date(), pPanel.getEqualGeneration(500.0));
				sData.addComercialLoad(new Date(), cLoad1.getEqualDemand(2.0));
				sData.addResidentialLowLoad(new Date(), rLoad3.getEqualDemand(2.0));
				sData.addResidentialMediumLoad(new Date(), rLoad2.getEqualDemand(2.0));
				sData.addResidentialHighLoad(new Date(), rLoad1.getEqualDemand(2.0));
				timeLeft -= 5;
				interval++;
				intervalUsedCount++;
			}
			
			sData.addTDelta("t1*t" + intervalUsedCount + " 0.08333");
			System.out.println(sData.getTHeader());
			
		}
		
		return sData;
	}
	
	public SimulationData buildOriginal() {
		WindTurbine wTurbine = new WindTurbine(10.0);
		PhotovoltaicPanel pPanel = new PhotovoltaicPanel(10.0, wData.getMaxIrradiation());
		Double tempValue = 0.0;
		Double tempWValue = 0.0;
		
		Integer interval = this.interruptTime;
		Integer timeLeft = this.interruptPeriod;
		SimulationData tempData = new SimulationData();
		SimulationData sData = new SimulationData();
		
		while (timeLeft > 0) {
			tempData.addPowerWindData(new Date(), wTurbine.getGeneration(this.wData.getWind().get(interval).getData().intValue()));
			tempData.addPowerSolarData(new Date(), pPanel.getGeneration(this.wData.getIrradiation().get(interval).getData()));
			timeLeft -= 5;
			interval++;
		}
		
		if (this.interruptPeriod >= 30) {
			sData.addTDelta("t1*t6 0.08333");
			for (int i = 0; i < 6; i++) {
				sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
				sData.addPowerWindData(tempData.getPowerWind().get(i).getTimestamp(), tempData.getPowerWind().get(i).getData());
			}
		} else {
			sData.addTDelta("t1*t" + (this.interruptPeriod / 5) + " 0.08333");
			for (int i = 0; i < (this.interruptPeriod / 5); i++) {
				sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
				sData.addPowerWindData(tempData.getPowerWind().get(i).getTimestamp(), tempData.getPowerWind().get(i).getData());
			}
		}
		tempValue = 0.0;
		tempWValue = 0.0;
		if (this.interruptPeriod >= 60) {
			sData.addTDelta("t7 0.5");
			for (int i = 0; i < 6; i++) {
				tempValue += tempData.getPowerSolar().get(6 + i).getData();
			}
			sData.addPowerSolarData(new Date(), tempValue / 6);
			for (int i = 0; i < 6; i++) {
				tempWValue += tempData.getPowerWind().get(6 + i).getData();
			}
			sData.addPowerWindData(new Date(), tempWValue / 6);
		} else if (this.interruptPeriod > 30 && this.interruptPeriod < 60) {
			sData.addTDelta("t7 " + ( ((interruptPeriod - 30) / 5) * 0.083333));
			for (int i = 0; i < ((interruptPeriod - 30) / 5); i++) {
				tempValue += tempData.getPowerSolar().get(6 + i).getData();
			}
			sData.addPowerSolarData(new Date(), tempValue / ((interruptPeriod - 30) / 5));
			for (int i = 0; i < ((interruptPeriod - 30) / 5); i++) {
				tempWValue += tempData.getPowerWind().get(6 + i).getData();
			}
			sData.addPowerWindData(new Date(), tempWValue / ((interruptPeriod - 30) / 5));
		}
		tempValue = 0.0;
		tempWValue = 0.0;
		if (this.interruptPeriod > 60) {
			sData.addTDelta("t8 " + ( ((interruptPeriod - 60) / 5) * 0.083333));
			for (int i = 0; i < ((interruptPeriod - 60) / 5); i++) {
				tempValue += tempData.getPowerSolar().get(12 + i).getData();
				//sData.addPowerSolarData(tempData.getPowerSolar().get(i).getTimestamp(), tempData.getPowerSolar().get(i).getData());
			}
			sData.addPowerSolarData(new Date(), tempValue / ((interruptPeriod - 60) / 5));
			for (int i = 0; i < ((interruptPeriod - 60) / 5); i++) {
				tempWValue += tempData.getPowerWind().get(12 + i).getData();
			}
			sData.addPowerWindData(new Date(), tempWValue / ((interruptPeriod - 60) / 5));
		}
		
		return sData;
	}
}
