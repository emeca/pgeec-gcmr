package Data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

public class SimulationData {
	private ArrayList<RawData> powerWind;
	private ArrayList<RawData> powerSolar;
	private ArrayList<RawData> residentialLowLoad;
	private ArrayList<RawData> residentialMediumLoad;
	private ArrayList<RawData> residentialHighLoad;
	private ArrayList<RawData> comercialLoad;
	private ArrayList<String> tDelta;
	
	public SimulationData() {
		this.powerWind = new ArrayList<RawData>();
		this.powerSolar = new ArrayList<RawData>();
		this.comercialLoad = new ArrayList<RawData>();
		this.residentialLowLoad = new ArrayList<RawData>();
		this.residentialMediumLoad = new ArrayList<RawData>();
		this.residentialHighLoad = new ArrayList<RawData>();
		this.tDelta = new ArrayList<String>();
	}
	
	public void addPowerWindData(Date date, Double value) {
		this.powerWind.add(new RawData(date, value));
	}
	
	public void addPowerSolarData(Date date, Double value) {
		this.powerSolar.add(new RawData(date, value));
	}
	
	public void addResidentialLowLoad(Date date, Double value) {
		this.residentialLowLoad.add(new RawData(date, value));
	}
	
	public void addResidentialMediumLoad(Date date, Double value) {
		this.residentialMediumLoad.add(new RawData(date, value));
	}
	
	public void addResidentialHighLoad(Date date, Double value) {
		this.residentialHighLoad.add(new RawData(date, value));
	}
	
	public void addComercialLoad(Date date, Double value) {
		this.comercialLoad.add(new RawData(date, value));
	}
	
	public void addTDelta(String value) {
		this.tDelta.add(value);
	}
	
	public ArrayList<RawData> getPowerWind() {
		return powerWind;
	}
	public void setPowerWind(ArrayList<RawData> powerWind) {
		this.powerWind = powerWind;
	}
	public ArrayList<RawData> getPowerSolar() {
		return powerSolar;
	}
	public void setPowerSolar(ArrayList<RawData> powerSolar) {
		this.powerSolar = powerSolar;
	}

	public ArrayList<RawData> getResidentialLowLoad() {
		return residentialLowLoad;
	}

	public ArrayList<RawData> getResidentialMediumLoad() {
		return residentialMediumLoad;
	}

	public ArrayList<RawData> getResidentialHighLoad() {
		return residentialHighLoad;
	}

	public ArrayList<RawData> getComercialLoad() {
		return comercialLoad;
	}
	
	public String powerWindToString() {
		String result = new String("wind1   ");
		DecimalFormat df = new DecimalFormat("0000.00000");
		
		for (RawData data: this.powerWind) {
			//result += String.valueOf(Math.round(data.getData())) + " ";
			//result += String.format("%4d ", Math.round(data.getData()));
			result += df.format(data.getData()) + " ";
		}
		
		result += ";\n";
		
		return result;
	}
	
	public String powerSolarToString() {
		String result = new String("photo1  ");
		DecimalFormat df = new DecimalFormat("0000.00000");
		
		for (RawData data: this.powerSolar) {
			//result += String.valueOf(Math.round(data.getData().intValue())) + " ";
			//result += String.format("%4d ", Math.round(data.getData()));
			result += df.format(data.getData()) + " ";
		}
		
		result += ";\n";
		
		return result;
	}
	
	public String comercialLoadToString() {
		String result = new String("dloadA1 ");
		DecimalFormat df = new DecimalFormat("0000.00000");
		
		for (RawData data: this.comercialLoad) {
			//result += String.valueOf(Math.round(data.getData().intValue())) + " ";
			//result += String.format("%04.5f ", data.getData());
			result += df.format(data.getData()) + " ";
		}
		
		result += "\n";
		
		return result;
	}
	
	public String residentialLowLoadToString() {
		String result = new String("dloadB1 ");
		DecimalFormat df = new DecimalFormat("0000.00000");
		
		for (RawData data: this.residentialLowLoad) {
			//result += String.valueOf(Math.round(data.getData().intValue())) + " ";
			//result += String.format("%04.5f ", data.getData());
			result += df.format(data.getData()) + " ";
		}
		
		result += "\n";
		
		return result;
	}
	
	public String residentialMediumLoadToString() {
		String result = new String("dloadC1 ");
		DecimalFormat df = new DecimalFormat("0000.00000");
		
		for (RawData data: this.residentialMediumLoad) {
			//result += String.valueOf(Math.round(data.getData().intValue())) + " ";
			//result += String.format("%04.5f ", data.getData());
			result += df.format(data.getData()) + " ";
		}
		
		result += "\n";
		
		return result;
	}
	
	public String residentialHighLoadToString() {
		String result = new String("dloadD1 ");
		DecimalFormat df = new DecimalFormat("0000.00000");
		
		for (RawData data: this.residentialHighLoad) {
			//result += String.valueOf(Math.round(data.getData().intValue())) + " ";
			//result += String.format("%04.5f ", data.getData());
			result += df.format(data.getData()) + " ";
		}
		
		result += "\n";
		
		return result;
	}
	
	public String tDeltaToString() {
		String result = new String("/");
		
		for (String str: this.tDelta) {
			result += str + "\n";
		}
		result += " /";
		
		return result;
	}
	
	public String getTHeader() {
		String result = new String("        ");
		
		for (int i = 0; i < this.comercialLoad.size(); i++) {
			if (i >= 100) {
				result += "t" + (i + 1) + "       ";
			} else 	if (i < 10) {
				result += "t" + (i + 1) + "         ";
			} else {
				result += "t" + (i + 1) + "        ";
			}
		}
		
		return result + "\n";
	}
	
	public String getPeriods() {
		String result = new String("t1*t" + this.comercialLoad.size());
		
		return result;
	}
}
