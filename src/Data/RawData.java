package Data;

import java.util.Date;

public class RawData {
	private Date timestamp;
	private Double value;
	
	public RawData(Date date, Double value) {
		this.timestamp = date;
		this.value = value;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public Double getData() {
		return value;
	}
	public void setData(Double data) {
		this.value = data;
	}
	
	
}
