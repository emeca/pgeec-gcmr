package Data;

public class BatteryData {
	private Double charging;
	private Double discharging;
	private Double soc;
	private Double statusGerDiesel;
	
	public BatteryData() {
		this.charging = 0.0;
		this.discharging = 0.0;
		this.soc = 0.0;
		this.statusGerDiesel = 0.0;
	}
	
	public Double getCharging() {
		return charging;
	}
	public void setCharging(Double charging) {
		this.charging = charging;
	}
	public Double getDischarging() {
		return discharging;
	}
	public void setDischarging(Double discharging) {
		this.discharging = discharging;
	}
	public Double getSoc() {
		return soc;
	}
	public void setSoc(Double soc) {
		this.soc = soc;
	}

	public void setGeneratorStatus(Double valueOf) {
		this.statusGerDiesel = valueOf;
	}
	
	public Double getGeneratorStatus() {
		return this.statusGerDiesel;
	}
	
}
