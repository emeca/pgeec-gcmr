package Data;

import java.util.ArrayList;
import java.util.Date;

public class WeatherData {
	private ArrayList<RawData> wind;
	private ArrayList<RawData> irradiation;
	private ArrayList<RawData> temperature;
	
	public WeatherData() {
		this.wind = new ArrayList<RawData>();
		this.irradiation = new ArrayList<RawData>();
		this.temperature = new ArrayList<RawData>();
	}
	
	public ArrayList<RawData> getWind() {
		return wind;
	}
	public void setWind(ArrayList<RawData> wind) {
		this.wind = wind;
	}
	public ArrayList<RawData> getIrradiation() {
		return irradiation;
	}
	public void setIrradiation(ArrayList<RawData> irradiation) {
		this.irradiation = irradiation;
	}
	public ArrayList<RawData> getTemperature() {
		return temperature;
	}
	public void setTemperature(ArrayList<RawData> temperature) {
		this.temperature = temperature;
	}
	
	public void addWindValue(Date date, Double value) {
		RawData data = new RawData(date, value);
		this.wind.add(data);
	}
	
	public void addIrradiationValue(Date date, Double value) {
		RawData data = new RawData(date, value);
		this.irradiation.add(data);
	}
	
	public void addTemperatureValue(Date date, Double value) {
		RawData data = new RawData(date, value);
		this.temperature.add(data);
	}

	public Double getMaxIrradiation() {
		Double maxIrradiation = 0.0;
		
		for (RawData data: this.irradiation) {
			if (data.getData() > maxIrradiation) {
				maxIrradiation = data.getData();
			}
		}
		
		return maxIrradiation;
	}

}
