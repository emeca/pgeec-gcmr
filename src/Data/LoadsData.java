package Data;

import java.util.ArrayList;
import java.util.Date;

public class LoadsData {
	private ArrayList<RawData> residentialLow;
	private ArrayList<RawData> residentialMedium;
	private ArrayList<RawData> residentialHigh;
	private ArrayList<RawData> comercial;
	
	public LoadsData() {
		this.residentialLow = new ArrayList<RawData>();
		this.residentialMedium = new ArrayList<RawData>();
		this.residentialHigh = new ArrayList<RawData>();
		this.comercial = new ArrayList<RawData>();
	}
	
	public ArrayList<RawData> getResidentialLow() {
		return residentialLow;
	}
	public void setResidentialLow(ArrayList<RawData> residentialLow) {
		this.residentialLow = residentialLow;
	}
	public ArrayList<RawData> getResidentialMedium() {
		return residentialMedium;
	}
	public void setResidentialMedium(ArrayList<RawData> residentialMedium) {
		this.residentialMedium = residentialMedium;
	}
	public ArrayList<RawData> getResidentialHigh() {
		return residentialHigh;
	}
	public void setResidentialHigh(ArrayList<RawData> residentialHigh) {
		this.residentialHigh = residentialHigh;
	}
	public ArrayList<RawData> getComercial() {
		return comercial;
	}
	public void setComercial(ArrayList<RawData> comercial) {
		this.comercial = comercial;
	}
	
	public void addResidentialLowValue(Date date, Double value) {
		RawData data = new RawData(date, value);
		this.residentialLow.add(data);
	}
	
	public void addResidentialMediumValue(Date date, Double value) {
		RawData data = new RawData(date, value);
		this.residentialMedium.add(data);
	}
	
	public void addResidentialHighValue(Date date, Double value) {
		RawData data = new RawData(date, value);
		this.residentialHigh.add(data);
	}
	
	public void addComercialValue(Date date, Double value) {
		RawData data = new RawData(date, value);
		this.comercial.add(data);
	}
}
