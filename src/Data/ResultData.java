package Data;

import java.util.ArrayList;
import java.util.Date;

public class ResultData {
	private ArrayList<RawData> periodCost;
	private ArrayList<RawData> genDiesel;
	private ArrayList<RawData> genSolar;
	private ArrayList<RawData> genWind;
	private ArrayList<RawData> batterySOC;
	private ArrayList<RawData> batteryPower;
	private ArrayList<RawData> batteryCharge;
	private ArrayList<RawData> batteryDisharge;
	private ArrayList<RawData> loadPower;
	private ArrayList<RawData> loadShededPower;
	private ArrayList<RawData> totalProducedPower;
	private ArrayList<RawData> load1;
	private ArrayList<RawData> load2;
	private ArrayList<RawData> load3;
	private ArrayList<RawData> load4;
	private ArrayList<RawData> loadShed1;
	private ArrayList<RawData> loadShed2;
	private ArrayList<RawData> loadShed3;
	private ArrayList<RawData> loadShed4;
	
	public ResultData() {
		this.periodCost = new ArrayList<RawData>();
		this.genDiesel = new ArrayList<RawData>();
		this.genSolar = new ArrayList<RawData>();
		this.genWind = new ArrayList<RawData>();
		this.loadPower = new ArrayList<RawData>();
		this.loadShededPower = new ArrayList<RawData>();
		this.batterySOC = new ArrayList<RawData>();
		this.batteryPower = new ArrayList<RawData>();
		this.batteryCharge = new ArrayList<RawData>();
		this.batteryDisharge = new ArrayList<RawData>();
		this.totalProducedPower = new ArrayList<RawData>();
		
		this.load1 = new ArrayList<RawData>();
		this.load2 = new ArrayList<RawData>();
		this.load3 = new ArrayList<RawData>();
		this.load4 = new ArrayList<RawData>();
		
		this.loadShed1 = new ArrayList<RawData>();
		this.loadShed2 = new ArrayList<RawData>();
		this.loadShed3 = new ArrayList<RawData>();
		this.loadShed4 = new ArrayList<RawData>();
	
	}
	
	public void addGenDieselValue(Date data, Double value) {
		this.genDiesel.add(new RawData(data, value));
	}

	public void addPeriodCost(Date data, Double value) {
		this.periodCost.add(new RawData(data, value));
	}
	
	public void addGenSolar(Date data, Double value) {
		this.genSolar.add(new RawData(data, value));
	}
	
	public void addGenWind(Date data, Double value) {
		this.genWind.add(new RawData(data, value));
	}
	
	public void addBatterySOC(Date data, Double value) {
		this.batterySOC.add(new RawData(data, value));
	}
	
	public void addLoadPower(Date data, Double value) {
		this.loadPower.add(new RawData(data, value));
	}
	
	public void addLoadShededPower(Date data, Double value) {
		this.loadShededPower.add(new RawData(data, value));
	}
	
	public void addTotalProducedPower(Date data, Double value) {
		this.totalProducedPower.add(new RawData(data, value));
	}
				
	public ArrayList<RawData> getGenDiesel() {
		return genDiesel;
	}

	public ArrayList<RawData> getPeriodCost() {
		return periodCost;
	}

	public ArrayList<RawData> getGenSolar() {
		return genSolar;
	}

	public ArrayList<RawData> getGenWind() {
		return genWind;
	}

	public ArrayList<RawData> getBatterySOC() {
		return batterySOC;
	}

	public ArrayList<RawData> getLoadPower() {
		return loadPower;
	}
	
	public ArrayList<RawData> getLoadShededPower() {
		return loadShededPower;
	}

	public ArrayList<RawData> getTotalProducedPower() {
		return totalProducedPower;
	}

	public ArrayList<RawData> getLoad1() {
		return load1;
	}

	public void setLoad1(ArrayList<RawData> load1) {
		this.load1 = load1;
	}

	public ArrayList<RawData> getLoad2() {
		return load2;
	}

	public void setLoad2(ArrayList<RawData> load2) {
		this.load2 = load2;
	}

	public ArrayList<RawData> getLoad3() {
		return load3;
	}

	public void setLoad3(ArrayList<RawData> load3) {
		this.load3 = load3;
	}

	public ArrayList<RawData> getLoad4() {
		return load4;
	}
	
	public ArrayList<RawData> getLoadShed1() {
		return loadShed1;
	}

	public ArrayList<RawData> getLoadShed2() {
		return loadShed2;
	}

	public ArrayList<RawData> getLoadShed3() {
		return loadShed3;
	}

	public ArrayList<RawData> getLoadShed4() {
		return loadShed4;
	}

	public void setLoad4(ArrayList<RawData> load4) {
		this.load4 = load4;
	}
	
	public void addLoadA1(Date data, Double value) {
		this.load1.add(new RawData(data, value));
	}
	
	public void addLoadA2(Date data, Double value) {
		this.load2.add(new RawData(data, value));
	}
	
	public void addLoadA3(Date data, Double value) {
		this.load3.add(new RawData(data, value));
	}
	
	public void addLoadA4(Date data, Double value) {
		this.load4.add(new RawData(data, value));
	}
	
	public void addLoadShedA1(Date data, Double value) {
		this.loadShed1.add(new RawData(data, value));
	}
	
	public void addLoadShedA2(Date data, Double value) {
		this.loadShed2.add(new RawData(data, value));
	}
	
	public void addLoadShedA3(Date data, Double value) {
		this.loadShed3.add(new RawData(data, value));
	}
	
	public void addLoadShedA4(Date data, Double value) {
		this.loadShed4.add(new RawData(data, value));
	}

	public void addBatteryPower(Date data, double value) {
		this.batteryPower.add(new RawData(data, value));
	}

	public ArrayList<RawData> getBatteryPower() {
		return batteryPower;
	}
	
	public void addBatteryCharge(Date data, double value) {
		this.batteryCharge.add(new RawData(data, value));
	}

	public ArrayList<RawData> getBatteryCharge() {
		return batteryCharge;
	}
	
	public void addBatteryDisharge(Date data, double value) {
		this.batteryDisharge.add(new RawData(data, value));
	}

	public ArrayList<RawData> getBatteryDisharge() {
		return batteryDisharge;
	}
	
}
