package Model;

import Application.MGConfiguration;
import Data.BatteryData;
import Data.SimulationData;

public class IslandedModel {
	private String data;
	private String model;
	private SimulationData simulationData;
	private Double SoC;
	
	public IslandedModel(SimulationData sData, Double SoC) {
		this.simulationData = sData;
		this.SoC = SoC;
	}
	
	public String buildModel(BatteryData battery) {
		
		if (battery.getCharging() > 0) {
			this.SoC = battery.getSoc() + (battery.getCharging() * 10 * 0.083333);
		} else if (battery.getDischarging() > 0) {
			this.SoC = battery.getSoc() - (battery.getDischarging() * 10 * 0.083333);
		} else {
			this.SoC = battery.getSoc();
		}
		
		this.model = "Sets\r\n" + 
				"         ConjGerDiesel   conjunto dos geradores a diesel                 / gen1 /\r\n" + 
				"         ConjPeriodos    conjunto dos per�odos da simula��o              / " + this.simulationData.getPeriods() + " /\r\n" + 
				"         ConjGerSolar    conjunto dos geradores fotovoltaicos            / photo1 /\r\n" + 
				"         ConjGerEolica   conjunto dos geradores eolicos                  / wind1 /\r\n" + 
				"         ConjCargasDespachaveis  conjunto das cargas despachaveis        / dloadA1, dloadB1, dloadC1, dloadD1 /\r\n" + 
				"         ConjBaterias    conjunto das baterias                           / bat1 /\r\n" + 
				"         ConjILinearizacao conjunto com valores para linearizacao do consumo do gerador a diese  / 1*6 /;\r\n" + 
				"\r\n" + 
				"* Defini��o dos alias (sinonimos) para os conjuntos criados, com o objetivo de simplificar\r\n" + 
				"* a chamada dos conjuntos no modelo matem�tico\r\n" + 
				"Alias(ConjGerDiesel, G);\r\n" + 
				"Alias(ConjGerEolica, W);\r\n" + 
				"Alias(ConjBaterias, B);\r\n" + 
				"Alias(ConjGerSolar, P);\r\n" + 
				"Alias(ConjCargasDespachaveis, DL);\r\n" + 
				"\r\n" + 
				"* Definicao dos custos de fornecimento dos Recursos Energicamente Distribuidos\r\n" + 
				"Scalar CustoGerEolica custo de gera��o por kW  para energia e�lica / 0.01 /;\r\n" + 
				"Scalar CustoGerSolar valor de gera��o por kW  para energia solar / 0.01 /;\r\n" + 
				"Scalar CustoBateria valor para utiliza��o da bateria / 0.001 /;\r\n" + 
				"Scalar CustoCombustivel custo de combustivel (R$ por litro) /4.1/;\r\n" + 
				"\r\n" + 
				"* Definicao de constantes para a geracao a diesel\r\n" + 
				"Scalar GerDieselConstA parametro A do calculo de linearizacao /0.004446/;\r\n" + 
				"Scalar GerDieselConstB parametro B do calculo de linearizacao /0.121035/;\r\n" + 
				"Scalar GerDieselConstC parametro C do calculo de linearizacao /1.653882/;\r\n" + 
				"Scalar GerDieselLimiteRampaSubida limite de subida de geracao a diesel / 5 /;\r\n" + 
				"Scalar GerDieselLimiteRampaDescida limite de descida de geracao a diesel / -5 /;\r\n" + 
				"\r\n" + 
				"* Par�metros para configura��o de limites operacionais dos elementos da microrrede\r\n" + 
				"Parameters\r\n" + 
				"         ParGerDieselMaxima(ConjGerDiesel) limites maximo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 20 /\r\n" + 
				"*           gen2 500\r\n" + 
				"*           gen3 500\r\n" + 
				"*           gen4 500 /\r\n" + 
				"\r\n" + 
				"         ParGerDieselMinima(ConjGerDiesel) limites minimo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 4 /\r\n" + 
				"*           gen2 130\r\n" + 
				"*           gen3 130\r\n" + 
				"*           gen4 130 /\r\n" + 
				"\r\n" + 
				"         ParGerFotovoltaicaMaxima(ConjGerSolar) limites m�ximo de gera��o fotovoltaica em kW\r\n" + 
				"         / photo1 10 /\r\n" + 
				"\r\n" + 
				"         ParGerEolicaMaxima(ConjGerEolica) limites m�ximo da gera��o eolica em kW\r\n" + 
				"         / wind1 10 /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteCarregamento(ConjBaterias) limite de carregamento da bateria em kW\r\n" + 
				"         / bat1 0 /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteDescarregamento(ConjBaterias) limite d edescarregamento da bateria em kW\r\n" + 
				"         / bat1 0 /\r\n" + 
				"*           bat2 -100 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMinimaEnergia(ConjBaterias) limite minimo de armazenamento da bateria SOC\r\n" + 
				"         / bat1 2 /\r\n" + 
				"*          bat2 60 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMaximaEnergia(ConjBaterias) limite maximo de armazenamento da bateria SOC\r\n" + 
				"         / bat1 10 /\r\n" + 
				"*           bat2 400 /\r\n" + 
				"\r\n" + 
				"         ParCargasDespachaveisPenalidade(ConjCargasDespachaveis) custo de penalizacao das cargas despach�veis\r\n" + 
				"         / dloadA1 30\r\n" + 
				"*           dloadA2 30\r\n" + 
				"           dloadB1 20\r\n" + 
				"*           dloadB2 20\r\n" + 
				"           dloadC1 15\r\n" + 
				"*           dloadC2 10\r\n" + 
				"           dloadD1 10 /\r\n" + 
				"*           dloadD2 10 /\r\n" + 
				"\r\n" + 
				"Table ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) valores linearizados usados para a curva de consumo do gerador\r\n" + 
				"         1       2       3       4       5       6\r\n" + 
				"gen1     0.0     4.0     7.0     11.5    15.0    20.0;\r\n" + 
				"\r\n" + 
				"Table consumoger(ConjGerDiesel, ConjILinearizacao) curva de consumo do gerador interpolada\r\n" + 
				"         1\r\n" + 
				"gen1     0;\r\n" + 
				"\r\n" + 
				"      consumoger(ConjGerDiesel, ConjILinearizacao)$(ord(ConjILinearizacao) > 1) = (GerDieselConstA * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) + GerDieselConstB * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) + GerDieselConstC);\r\n" + 
				"\r\n" + 
				"*DISCRETIZACAO EM 3 NIVEIS: na primeira meia hora otimiza de 5 em 5 min (dt=.083\r\n" + 
				"* horas), na segunda meia hora discretiza em .5 horas,\r\n" + 
				"* e no restante das 23 horas do dia, discretiza de\r\n" + 
				"* hora em hora (dt = 1 hora).\r\n" + 
				"Parameter dt(conjPeriodos)\r\n" + 
				this.simulationData.tDeltaToString() + "\n" +
//				"/t1*t19 0.08333\r\n" + 
				"\r\n" + 
				"Table    ParBateriaSOCInicial(B, ConjPeriodos) valor do SOC da bateria para o periodo inicial da simula��o\r\n" + 
				"         t1\r\n" + 
				"bat1     " + String.format("%3.2f ", (this.SoC / 10)).replace(",", ".") + ";\r\n" + 
				"\r\n" + 
				"Table    PrevisaoCargasDesp(ConjCargasDespachaveis, ConjPeriodos) previs�o de consumo das cargas despach�veis\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.comercialLoadToString() +
				this.simulationData.residentialHighLoadToString() +
				this.simulationData.residentialMediumLoadToString() +
				this.simulationData.residentialLowLoadToString() +
				"\r\n" + 
				"Table    PrevisaoGerSolar(ConjGerSolar, ConjPeriodos) previs�o de gera��o solar\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.powerSolarToString() + 
				"\r\n" + 
				"Table    PrevisaoGerEolica(ConjGerEolica, ConjPeriodos) previs�o de gera��o e�lica\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.powerWindToString() + 
				"\r\n" + 
				"Variable CustoTotal Custo total de opera��o;\r\n" + 
				"\r\n" + 
				"Positive Variable PotenciaGeradaEolica(ConjGerEolica, ConjPeriodos) Geracao eolica do gerador i no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaSolar(ConjGerSolar, ConjPeriodos) Geracao solar do gerador i no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaDiesel(ConjGerDiesel, ConjPeriodos) Geracao a diesel no periodo t;\r\n" + 
				"\r\n" + 
				"Variable BateriaCarregamento(B, ConjPeriodos) Carregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaDescarregamento(B, ConjPeriodos) Descarregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaEnergia(B, ConjPeriodos) Energia armazenada na bateria no periodo t;\r\n" + 
				"Variable BateriaSoC(B, ConjPeriodos) Estado de carga (SoC) da bateria no periodo t;\r\n" + 
				"\r\n" + 
				"Variable ConsumoDiesel(ConjPeriodos) Consumo de combustivel do gerador a diesel no periodo t;\r\n" + 
				"*Variable ConsumoCargasPrioritarias(ConjPeriodos) Consumo das cargas prioritarias no periodo t;\r\n" + 
				"Variable ConsumoCargasDesp(ConjPeriodos) Consumo das cargas despachaveis no periodo t;\r\n" + 
				"\r\n" +
				"Variable CustoPorPeriodos(ConjPeriodos) Custo de opera��o da microrrede para cada per�odo t;" +
				"\r\n" +
				"Variable PrevisaoRenovaveis(ConjPeriodos) previs�o da gera��o renov�vel para o per�odo t;\r\n" + 
				"Variable PotenciaGeradaExcessoRenovaveis(ConjPeriodos) previs�o de gera��o em excesso para renov�veis no per�odo t;\r\n" +
				"\r\n" +
				"Variable TotalPotenciaProduzida(ConjPeriodos) Total de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaExcesso(ConjPeriodos) Excesso de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) Potencia nao suprida das cargas despachaveis no periodo t;\r\n" + 
				"Variable PotenciaSupridaCargasDesp(ConjPeriodos) Potencia suprida para as cargas despachaveis no periodo t;\r\n" + 
				"\r\n" + 
				"*variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"sos2 variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"\r\n" + 
				"* VARI�VEIS BIN�RIAS\r\n" + 
				"Binary Variable StatusGerDiesel(G, ConjPeriodos) On-Off Status do gerador a diesel no per�odo t (0 desligado - 1 Ligado);\r\n" + 
				"Binary Variable StatusBateria(B, ConjPeriodos) Status of battery (0 descarregando - 1 carregando);\r\n" + 
				"Binary Variable StatusCargasDesp(DL, ConjPeriodos) On-Off Status das cargas despach�veis no per�odo t;\r\n" + 
				"*Binary Variable StatusCargas(L, ConjPeriodos) On-Off Status das cargas priorit�rias no per�odo t;\r\n" + 
				"Binary Variable StatusGerSolar(P, ConjPeriodos) On-Off Status do gerador fotovoltaico n no per�odo t;\r\n" + 
				"Binary Variable StatusGerEolica(W, ConjPeriodos) On-Off Status do gerador e�lico n no per�odo t;\r\n" + 
				"\r\n" + 
				"* Vari�veis relacionadas ao c�lculo de energia consumida e gerada\r\n" + 
				"Variable EnergiaGeradaGerDiesel(ConjPeriodos) Total de energia gerada pelo gerador diesel no per�odo t;\r\n" + 
				"Variable EnergiaGeradaGerEolico(ConjPeriodos) Total de energia gerada pelo gerador eolico no per�odo t;\r\n" + 
				"Variable EnergiaGeradaGerFotovoltaico(ConjPeriodos) Total de energia gerada pelo gerador fotovoltaico no per�odo t;\r\n" + 
				"Variable EnergiaGeradaBateria(ConjPeriodos) Total de energia gerada pelo banco de baterias no per�odo t;\r\n" + 
				"Variable TotalEnergiaProduzida(ConjPeriodos) Total de energia produzida por todos os REDs no per�odo t;\r\n" + 
				"\r\n" + 
				"Equations\r\n" + 
				"*        Fun��o objetivo para o problema de minimiza��o do custo operacional no gerenciamento de uma microrrede\r\n" + 
				"         CustoFinal define the objetive function\r\n" + 
				"\r\n" +
				"         CalculaCustoParcial(ConjPeriodos) C�lculo do custo de opera��o para os per�odos\r\n" +
				"		  CalcularPrevisaoRenovaveis(ConjPeriodos) previsao de geracao e�lica para o per�odo t" +
				"\r\n" +
				"*        Balan�o de pot�ncia na microrrede\r\n" + 
				"         BalancoPotencia(ConjPeriodos) Balanco de potencia no periodo t\r\n" + 
				"\r\n" + 
				"*        Load_consume(ConjPeriodos) Consumo real das cargas priorit�rias para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas �s cargas despach�veis\r\n" + 
				"         RespostaDemanda(ConjPeriodos) Aplica��o da resposta da demanda para o per�odo t\r\n" + 
				"         ConsumoDemanda(ConjPeriodos) Consumo real das cargas despach�veis para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionada ao gerador diesel\r\n" + 
				"         CalcularGeracaoDiesel(G, ConjPeriodos) Gera��o no per�odo t\r\n" + 
				"         CalcularConsumoGeradorDiesel(ConjPeriodos) Consumo de diesel para o gerador g no per�odo t \r\n" + 
				"         LimiteSuperiorGeracaoDiesel(G, ConjPeriodos) Limite m�ximo de gera��o para o gerador diesel \r\n" + 
				"         LimiteInferiorGeracaoDiesel(G, ConjPeriodos) Limite m�nimo de gera��o para o gerador diesel \r\n" + 
				"         LinearizarGeradorDiesel(G, ConjPeriodos) Restri��o da fun��o sos2 definindo um �nico intervalo linear para pi \r\n" + 
				"         LimiteRampaSubidaGeradorDiesel(G, ConjPeriodos) Restri��o de rampa de subida para gera��o a diesel \r\n" + 
				"         LimiteRampaDescidaGeradorDiesel(G, ConjPeriodos) Restri��o de rampa de descida para a gera��o a diesel \r\n" + 
				"\r\n" + 
				"         CalcularTotalPotenciaProduzida(ConjPeriodos) calculo do total gerado na microrrede no periodo t \r\n" +
				"         CalcularPGER(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t \r\n " +
				"         CalcularPGE(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t \r\n" + 
				"         CalcularPNS(DL, ConjPeriodos) C�lculo da energia n�o suprida para as cargas no per�odo t \r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador e�lico\r\n" + 
				"         LimiteGeracaoEolica(W, ConjPeriodos) Limite superior de gera��o da gera��o e�lica no periodo t\r\n" + 
				"         LimiteSuperiorGeracaoEolica(W, ConjPeriodos) gera��o e�lica m�xima para o per�odo t\r\n" + 
				"         LimiteInferiorGeracaoEolica(W, ConjPeriodos) gera��o e�lica m�nima para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador fotovoltaico\r\n" + 
				"         LimiteGeracaoSolar(P, ConjPeriodos) Limite superior de gera��o solar no per�odo t\r\n" + 
				"         LimiteSuperiorGeracaoSolar(P, ConjPeriodos) Gera��o solar m�xima para o per�odo t\r\n" + 
				"         LimiteInferiorGeracaoSolar(P, ConjPeriodos) Gera��o solar m�nima para o per�odo t\r\n" +
				"\r\n" + 
				"*        Restri��es relacionadas a bateria\r\n" + 
				"         BalancoPotenciaBateria(B, ConjPeriodos) balan�o de pot�ncia da bateria\r\n" + 
				"         CargaMinimaBateria(B, ConjPeriodos) valor m�nimo para armazenamento da bateria\r\n" + 
				"         CargaMaximaBateria(B, ConjPeriodos) valor m�ximo para armazenamento da bateria\r\n" + 
				"         LimiteSuperiorCarregamentoBateria(B, ConjPeriodos) carregamento da bateria i para o per�odo t\r\n" + 
				"         LimiteInferiorCarregamentoBateria(B, ConjPeriodos) limite inferior para carregamento da bateria\r\n" + 
				"         LimiteCarregamentoBateria(B, ConjPeriodos) limite de carregamento considerando energia armazenada na bateria\r\n" + 
				"         LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos) descarregamento m�ximo da bateria no per�odo t\r\n" + 
				"         LimiteInferiorDescarregamentoBateria(B, ConjPeriodos) descarregamento m�nimo da bateria no per�odo t\r\n" + 
//				"         LimiteDescarregamentoBateria(B, ConjPeriodos) limite de descarregamento considerando energia armazenada na bateria\r\n" + 
				"         CalcularSOCBateria(B, ConjPeriodos) SoC da bateria no per�odo t\r\n" + 
				"\r\n" + 
				"*        C�lculo da gera��o de energia pelas REDs\r\n" + 
				"         CalculaGeracaoDiesel(ConjPeriodos) calcula a geracao de energia a diesel no per�odo t\r\n" + 
				"         CalculaGeracaoEolica(ConjPeriodos) calcula a geracao de energia eolica no per�odo t\r\n" + 
				"         CalculaGeracaoFotovoltaica(ConjPeriodos) calcula a geracao de energia fotovoltaica no per�odo t\r\n" + 
				"         CalculaGeracaoBateria(ConjPeriodos) calcula a geracao de energia pela bateria no per�odo t\r\n" + 
				"         CalculaTotalEnergiaProduzida(ConjPeriodos) calcula o total de energia gerada na microrrede pelos DERs;\r\n" + 
				"\r\n" +  
				"CustoFinal..   CustoTotal =e= sum(ConjPeriodos, ConsumoDiesel(ConjPeriodos) * CustoCombustivel * dt(ConjPeriodos) ) + sum((ConjPeriodos,ConjGerEolica), (CustoGerEolica * PotenciaGeradaEolica(ConjGerEolica,ConjPeriodos) * dt(ConjPeriodos))) + sum((ConjPeriodos,ConjGerSolar), (CustoGerSolar * PotenciaGeradaSolar(ConjGerSolar,ConjPeriodos) * dt(ConjPeriodos))) - sum((ConjPeriodos, ConjBaterias), BateriaDescarregamento(ConjBaterias, ConjPeriodos) * CustoBateria) + sum((ConjPeriodos, ConjBaterias), BateriaCarregamento(ConjBaterias, ConjPeriodos) * CustoBateria) + sum((DL, ConjPeriodos), PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL)) + sum(ConjPeriodos, PotenciaGeradaExcessoRenovaveis(ConjPeriodos) * 0.2);\r\n" + 
				"\r\n" +
				"CalculaCustoParcial(ConjPeriodos).. CustoPorPeriodos(ConjPeriodos) =e= (ConsumoDiesel(ConjPeriodos) * CustoCombustivel * dt(ConjPeriodos)) + sum(ConjGerEolica, (CustoGerEolica * PotenciaGeradaEolica(ConjGerEolica,ConjPeriodos) * dt(ConjPeriodos))) + sum(ConjGerSolar, (CustoGerSolar * PotenciaGeradaSolar(ConjGerSolar,ConjPeriodos) * dt(ConjPeriodos))) - sum(ConjBaterias, BateriaDescarregamento(ConjBaterias, ConjPeriodos) * 0.01) + sum(ConjBaterias, BateriaCarregamento(ConjBaterias, ConjPeriodos) * 0.01) + sum(DL, PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL));" +
				"\r\n" +
				"BalancoPotencia(ConjPeriodos).. sum(G, PotenciaGeradaDiesel(G, ConjPeriodos)) + sum(W, PotenciaGeradaEolica(W, ConjPeriodos))+ sum(P, PotenciaGeradaSolar(P, ConjPeriodos)) + sum(B, BateriaDescarregamento(B, ConjPeriodos)) =e= sum(B, BateriaCarregamento(B, ConjPeriodos)) + PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"\r\n" + 
				"LimiteSuperiorGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =l= ParGerDieselMaxima(G) * StatusGerDiesel(G, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =g= ParGerDieselMinima(G) * StatusGerDiesel(G, ConjPeriodos);\r\n" + 
				"CalcularGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =e= sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao) * ParGerDieselConsumoLinearizado(G, ConjILinearizacao));\r\n" + 
				"CalcularConsumoGeradorDiesel(ConjPeriodos).. ConsumoDiesel(ConjPeriodos) =e= sum((G, ConjILinearizacao), lam(ConjPeriodos, ConjILinearizacao) * consumoger(G, ConjILinearizacao));\r\n" + 
				"LinearizarGeradorDiesel(G, ConjPeriodos).. sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao)) =e= 1;\r\n" + 
				"LimiteRampaSubidaGeradorDiesel(G, ConjPeriodos)$(ord(ConjPeriodos) > 1).. PotenciaGeradaDiesel(G, ConjPeriodos) - PotenciaGeradaDiesel(G, ConjPeriodos - 1) =l= GerDieselLimiteRampaSubida;\r\n" + 
				"LimiteRampaDescidaGeradorDiesel(G, ConjPeriodos)$(ord(ConjPeriodos) > 1).. PotenciaGeradaDiesel(G, ConjPeriodos) - PotenciaGeradaDiesel(G, ConjPeriodos - 1) =g= GerDieselLimiteRampaDescida;\r\n" + 
				"\r\n" + 
				"*Load_consume(ConjPeriodos).. ConsumoCargasPrioritarias(ConjPeriodos) =e= sum(L, PrevisaoCargas(L, ConjPeriodos) * (1 - StatusCargas(L, ConjPeriodos)));\r\n" + 
				"\r\n" + 
				"CalcularTotalPotenciaProduzida(ConjPeriodos).. TotalPotenciaProduzida(ConjPeriodos) =e=  sum(W, PotenciaGeradaEolica(W, ConjPeriodos)) + sum(P, PotenciaGeradaSolar(P, ConjPeriodos)) + sum(B, BateriaDescarregamento(B, ConjPeriodos)) + sum(G, PotenciaGeradaDiesel(G, ConjPeriodos));\r\n" +
				"CalcularPrevisaoRenovaveis(ConjPeriodos).. PrevisaoRenovaveis(ConjPeriodos) =e= sum(P, PrevisaoGerSolar(P, ConjPeriodos)) + sum(W, PrevisaoGerEolica(W, ConjPeriodos));\r\n" +
				"CalcularPGER(ConjPeriodos).. PotenciaGeradaExcessoRenovaveis(ConjPeriodos) =e= PrevisaoRenovaveis(ConjPeriodos) - (sum(W, PotenciaGeradaEolica(W, ConjPeriodos)) + sum(P, PotenciaGeradaSolar(P, ConjPeriodos)));" +
				"CalcularPGE(ConjPeriodos).. PotenciaGeradaExcesso(ConjPeriodos) =e= TotalPotenciaProduzida(ConjPeriodos) - PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"CalcularPNS(DL, ConjPeriodos).. PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) =e= (1 - StatusCargasDesp(DL, ConjPeriodos)) * PrevisaoCargasDesp(DL, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"* Restri��es relacionadas ao balan�o de pot�ncia\r\n" + 
				"RespostaDemanda(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"ConsumoDemanda(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"\r\n" + 
				"LimiteGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =l= ParGerEolicaMaxima(W);\r\n" + 
				"LimiteSuperiorGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =l= PrevisaoGerEolica(W, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =g= 0;\r\n" + 
				"*WT_generation(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =e= PrevisaoGerEolica(W, ConjPeriodos) * StatusGerEolica(W, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"LimiteGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =l= ParGerFotovoltaicaMaxima(P);\r\n" + 
				"LimiteSuperiorGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =l= PrevisaoGerSolar(P, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =g= 0;\r\n" + 
				"*PT_generation(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =e= PrevisaoGerSolar(P, ConjPeriodos) * StatusGerSolar(P, ConjPeriodos);\r\n" + 
				"\r\n" +
				"BalancoPotenciaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos - 1) + ((BateriaCarregamento(B, ConjPeriodos - 1) - BateriaDescarregamento(B, ConjPeriodos - 1)) * dt(ConjPeriodos)) + ParBateriaSOCInicial(B, ConjPeriodos);\r\n" + 
				"CargaMinimaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =g= ParBateriaMinimaEnergia(B);\r\n" + 
				"CargaMaximaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =l= ParBateriaMaximaEnergia(B);\r\n" + 
				"LimiteSuperiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =l= ParBateriaLimiteCarregamento(B) * StatusBateria(B, ConjPeriodos);\r\n" + 
				"LimiteInferiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
				"LimiteCarregamentoBateria(B, ConjPeriodos).. ParBateriaMaximaEnergia(B) =g= BateriaEnergia(B, ConjPeriodos - 1) + BateriaCarregamento(B, ConjPeriodos);\r\n" + 
				"LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =l= ParBateriaLimiteDescarregamento(B) * (1 - StatusBateria(B, ConjPeriodos));\r\n" + 
				"LimiteInferiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
				"CalcularSOCBateria(B, ConjPeriodos).. BateriaSoC(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos) / ParBateriaMaximaEnergia(B) * 100;\r\n" + 
//				"BalancoPotenciaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos - 1) + ( (BateriaCarregamento(B, ConjPeriodos - 1) - BateriaDescarregamento(B, ConjPeriodos - 1)  * dt(ConjPeriodos)) ) + ParBateriaSOCInicial(B, ConjPeriodos);\r\n" + 
//				"CargaMinimaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =g= ParBateriaMinimaEnergia(B);\r\n" + 
//				"CargaMaximaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =l= ParBateriaMaximaEnergia(B);\r\n" + 
//				"LimiteSuperiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =l= ParBateriaLimiteCarregamento(B) * StatusBateria(B, ConjPeriodos);\r\n" + 
//				"LimiteInferiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
//				"LimiteCarregamentoBateria(B, ConjPeriodos).. ParBateriaMaximaEnergia(B) =g= BateriaEnergia(B, ConjPeriodos - 1) + BateriaCarregamento(B, ConjPeriodos);\r\n" + 
//				"LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =l= ParBateriaLimiteDescarregamento(B) * (1 - StatusBateria(B, ConjPeriodos));\r\n" + 
//				"LimiteInferiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
//				"LimiteDescarregamentoBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos - 1) =g= BateriaDescarregamento(B, ConjPeriodos);\r\n" + 
//				"CalcularSOCBateria(B, ConjPeriodos).. BateriaSoC(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos) / ParBateriaMaximaEnergia(B) * 100;\r\n" + 
				"\r\n" + 
				"CalculaGeracaoDiesel(ConjPeriodos).. EnergiaGeradaGerDiesel(ConjPeriodos) =e= sum(G, PotenciaGeradaDiesel(G, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoEolica(ConjPeriodos).. EnergiaGeradaGerEolico(ConjPeriodos) =e= sum(W, PotenciaGeradaEolica(W, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoFotovoltaica(ConjPeriodos).. EnergiaGeradaGerFotovoltaico(ConjPeriodos) =e= sum(P, PotenciaGeradaSolar(P, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoBateria(ConjPeriodos).. EnergiaGeradaBateria(ConjPeriodos) =e= sum(B, BateriaDescarregamento(B, ConjPeriodos));\r\n" + 
				"CalculaTotalEnergiaProduzida(ConjPeriodos).. TotalEnergiaProduzida(ConjPeriodos) =e= EnergiaGeradaGerDiesel(ConjPeriodos) + EnergiaGeradaGerEolico(ConjPeriodos) + EnergiaGeradaGerFotovoltaico(ConjPeriodos) + EnergiaGeradaBateria(ConjPeriodos);\r\n" + 
				"\r\n" + 
				"model microgrid / all /;\r\n" + 
				"\r\n" + 
				"solve microgrid using MIP minimizing CustoTotal;\r\n" + 
				"\r\n" + 
				"Display CustoTotal.l, CustoTotal.m";
		
		return this.model;
	}
	
// Neste modelo � considerado o custo de inicializa��o dos geradores � diesel
	public String buildModel2(BatteryData battery, MGConfiguration config) {
		
		if (battery.getCharging() > 0) {
			this.SoC = battery.getSoc() + (battery.getCharging() * 10 * 0.083333);
		} else if (battery.getDischarging() > 0) {
			this.SoC = battery.getSoc() - (battery.getDischarging() * 10 * 0.083333);
		} else {
			this.SoC = battery.getSoc();
		}
		
		this.model = "Sets\r\n" + 
				"         ConjGerDiesel   conjunto dos geradores a diesel                 / gen1 /\r\n" + 
				"         ConjPeriodos    conjunto dos per�odos da simula��o              / " + this.simulationData.getPeriods() + " /\r\n" + 
				"         ConjGerSolar    conjunto dos geradores fotovoltaicos            / photo1 /\r\n" + 
				"         ConjGerEolica   conjunto dos geradores eolicos                  / wind1 /\r\n" + 
				"         ConjCargasDespachaveis  conjunto das cargas despachaveis        / dloadA1, dloadB1, dloadC1, dloadD1 /\r\n" + 
				"         ConjBaterias    conjunto das baterias                           / bat1 /\r\n" + 
				"         ConjILinearizacao conjunto com valores para linearizacao do consumo do gerador a diese  / 1*6 /;\r\n" + 
				"\r\n" + 
				"* Defini��o dos alias (sinonimos) para os conjuntos criados, com o objetivo de simplificar\r\n" + 
				"* a chamada dos conjuntos no modelo matem�tico\r\n" + 
				"Alias(ConjGerDiesel, G);\r\n" + 
				"Alias(ConjGerEolica, W);\r\n" + 
				"Alias(ConjBaterias, B);\r\n" + 
				"Alias(ConjGerSolar, P);\r\n" + 
				"Alias(ConjCargasDespachaveis, DL);\r\n" + 
				"\r\n" + 
				"* Definicao dos custos de fornecimento dos Recursos Energicamente Distribuidos\r\n" + 
				"Scalar CustoGerEolica custo de gera��o por kW  para energia e�lica / 0.01 /;\r\n" + 
				"Scalar CustoGerSolar valor de gera��o por kW  para energia solar / 0.01 /;\r\n" + 
				"Scalar CustoBateria valor para utiliza��o da bateria / 0.001 /;\r\n" + 
				"Scalar CustoCombustivel custo de combustivel (R$ por litro) /4.1/;\r\n" +
				"Scalar CustoOperacaoGeradorDiesel custo de operacao do gerador / 10 /; \r\n" +
				"Scalar EstadoInicialGerador custo de operacao do gerador / " + Math.round(battery.getGeneratorStatus()) + " /; \r\n; " + 
				"\r\n" + 
				"* Definicao de constantes para a geracao a diesel\r\n" + 
				"Scalar GerDieselConstA parametro A do calculo de linearizacao /0.004446/;\r\n" + 
				"Scalar GerDieselConstB parametro B do calculo de linearizacao /0.121035/;\r\n" + 
				"Scalar GerDieselConstC parametro C do calculo de linearizacao /1.653882/;\r\n" + 
				"Scalar GerDieselLimiteRampaSubida limite de subida de geracao a diesel / 5 /;\r\n" + 
				"Scalar GerDieselLimiteRampaDescida limite de descida de geracao a diesel / -5 /;\r\n" + 
				"\r\n" + 
				"* Par�metros para configura��o de limites operacionais dos elementos da microrrede\r\n" + 
				"Parameters\r\n" + 
				"         ParGerDieselMaxima(ConjGerDiesel) limites maximo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 " + (config.getUseDieselGenerators() ? "20" : "0") + " /\r\n" + 
				"*           gen2 500\r\n" + 
				"*           gen3 500\r\n" + 
				"*           gen4 500 /\r\n" + 
				"\r\n" + 
				"         ParGerDieselMinima(ConjGerDiesel) limites minimo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 " + (config.getUseDieselGenerators() ? "4" : "0") + " /\r\n" + 
				"*           gen2 130\r\n" + 
				"*           gen3 130\r\n" + 
				"*           gen4 130 /\r\n" + 
				"\r\n" + 
				"         ParGerFotovoltaicaMaxima(ConjGerSolar) limites m�ximo de gera��o fotovoltaica em kW\r\n" + 
				"         / photo1 10 /\r\n" + 
				"\r\n" + 
				"         ParGerEolicaMaxima(ConjGerEolica) limites m�ximo da gera��o eolica em kW\r\n" + 
				"         / wind1 10 /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteCarregamento(ConjBaterias) limite de carregamento da bateria em kW\r\n" + 
				"         / bat1 " +  (config.getUseBattery() ? "10" : "0")  +  " /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteDescarregamento(ConjBaterias) limite d edescarregamento da bateria em kW\r\n" + 
				"         / bat1 " + (config.getUseBattery() ? "10" : "0") + " /\r\n" + 
				"*           bat2 -100 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMinimaEnergia(ConjBaterias) limite minimo de armazenamento da bateria SOC\r\n" + 
				"         / bat1 2 /\r\n" + 
				"*          bat2 60 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMaximaEnergia(ConjBaterias) limite maximo de armazenamento da bateria SOC\r\n" + 
				"         / bat1 10 /\r\n" + 
				"*           bat2 400 /\r\n" + 
				"\r\n" + 
				"         ParCargasDespachaveisPenalidade(ConjCargasDespachaveis) custo de penalizacao das cargas despach�veis\r\n" + 
				"         / dloadA1 100\r\n" + 
				"*           dloadA2 30\r\n" + 
				"           dloadB1 50\r\n" + 
				"*           dloadB2 20\r\n" + 
				"           dloadC1 25\r\n" + 
				"*           dloadC2 10\r\n" + 
				"           dloadD1 10 /\r\n" + 
				"*           dloadD2 10 /\r\n" + 
				"\r\n" + 
				"Table ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) valores linearizados usados para a curva de consumo do gerador\r\n" + 
				"         1       2       3       4       5       6\r\n" + 
				"gen1     0.0     4.0     7.0     11.5    15.0    20.0;\r\n" + 
				//"         1       2       3       4       \r\n" + 
				//"gen1     0.0     5.5     11      15;    \r\n" +
				"\r\n" + 
				"Table consumoger(ConjGerDiesel, ConjILinearizacao) curva de consumo do gerador interpolada\r\n" + 
				"         1\r\n" + 
				"gen1     0;\r\n" + 
				"\r\n" + 
				"      consumoger(ConjGerDiesel, ConjILinearizacao)$(ord(ConjILinearizacao) > 1) = (GerDieselConstA * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) + GerDieselConstB * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) + GerDieselConstC);\r\n" + 
				"\r\n" + 
				"*DISCRETIZACAO EM 3 NIVEIS: na primeira meia hora otimiza de 5 em 5 min (dt=.083\r\n" + 
				"* horas), na segunda meia hora discretiza em .5 horas,\r\n" + 
				"* e no restante das 23 horas do dia, discretiza de\r\n" + 
				"* hora em hora (dt = 1 hora).\r\n" + 
				"Parameter dt(conjPeriodos)\r\n" + 
				this.simulationData.tDeltaToString() + "\n" +
//				"/t1*t19 0.08333\r\n" + 
				"\r\n" + 
				"Table    ParBateriaSOCInicial(B, ConjPeriodos) valor do SOC da bateria para o periodo inicial da simula��o\r\n" + 
				"         t1\r\n" + 
				"bat1     " + String.format("%3.2f ", (this.SoC / 10)).replace(",", ".") + ";\r\n" + 
				"\r\n" + 
				"Table    PrevisaoCargasDesp(ConjCargasDespachaveis, ConjPeriodos) previs�o de consumo das cargas despach�veis\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.comercialLoadToString().replace(",", ".") +
				this.simulationData.residentialHighLoadToString().replace(",", ".") +
				this.simulationData.residentialMediumLoadToString().replace(",", ".") +
				this.simulationData.residentialLowLoadToString().replace(",", ".") +
				"\r\n" + 
				"Table    PrevisaoGerSolar(ConjGerSolar, ConjPeriodos) previs�o de gera��o solar\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.powerSolarToString().replace(",", ".") + 
				"\r\n" + 
				"Table    PrevisaoGerEolica(ConjGerEolica, ConjPeriodos) previs�o de gera��o e�lica\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.powerWindToString().replace(",", ".") + 
				"\r\n" + 
				"Variable CustoTotal Custo total de opera��o;\r\n" + 
				"\r\n" + 
				"Positive Variable PotenciaGeradaEolica(ConjGerEolica, ConjPeriodos) Geracao eolica do gerador i no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaSolar(ConjGerSolar, ConjPeriodos) Geracao solar do gerador i no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaDiesel(ConjGerDiesel, ConjPeriodos) Geracao a diesel no periodo t;\r\n" + 
				"\r\n" + 
				"Variable perfilCargas(DL, ConjPeriodos) Previs�o de consumo das cargas para o per�odo t;\r\n" + 
				"\r\n" + 
				"Variable BateriaCarregamento(B, ConjPeriodos) Carregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaDescarregamento(B, ConjPeriodos) Descarregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaEnergia(B, ConjPeriodos) Energia armazenada na bateria no periodo t;\r\n" + 
				"Variable BateriaSoC(B, ConjPeriodos) Estado de carga (SoC) da bateria no periodo t;\r\n" + 
				"\r\n" + 
				"Variable ConsumoDiesel(ConjPeriodos) Consumo de combustivel do gerador a diesel no periodo t;\r\n" + 
				"*Variable ConsumoCargasPrioritarias(ConjPeriodos) Consumo das cargas prioritarias no periodo t;\r\n" + 
				"Variable ConsumoCargasDesp(ConjPeriodos) Consumo das cargas despachaveis no periodo t;\r\n" + 
				"\r\n" +
				"Variable CustoPorPeriodos(ConjPeriodos) Custo de opera��o da microrrede para cada per�odo t; \r\n" +
				"Variable CustoGerador(ConjPeriodos) Custo da gera��o � diesel por per�odo t; \r\n" +
				"\r\n" +
				"Variable PrevisaoRenovaveis(ConjPeriodos) previs�o da gera��o renov�vel para o per�odo t;\r\n" + 
				"Variable PotenciaGeradaExcessoRenovaveis(ConjPeriodos) previs�o de gera��o em excesso para renov�veis no per�odo t;\r\n" +
				"\r\n" +
				"Variable TotalPotenciaProduzida(ConjPeriodos) Total de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaExcesso(ConjPeriodos) Excesso de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) Potencia nao suprida das cargas despachaveis no periodo t;\r\n" + 
				"Variable PotenciaSupridaCargasDesp(ConjPeriodos) Potencia suprida para as cargas despachaveis no periodo t;\r\n" + 
				"\r\n" + 
				"*variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"sos2 variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"\r\n" + 
				"* VARI�VEIS BIN�RIAS\r\n" + 
				"Binary Variable StatusGerDiesel(G, ConjPeriodos) On-Off Status do gerador a diesel no per�odo t (0 desligado - 1 Ligado);\r\n" + 
				"Binary Variable StatusBateria(B, ConjPeriodos) Status of battery (0 descarregando - 1 carregando);\r\n" + 
				"Binary Variable StatusCargasDesp(DL, ConjPeriodos) On-Off Status das cargas despach�veis no per�odo t;\r\n" + 
				"*Binary Variable StatusCargas(L, ConjPeriodos) On-Off Status das cargas priorit�rias no per�odo t;\r\n" + 
				"Binary Variable StatusGerSolar(P, ConjPeriodos) On-Off Status do gerador fotovoltaico n no per�odo t;\r\n" + 
				"Binary Variable StatusGerEolica(W, ConjPeriodos) On-Off Status do gerador e�lico n no per�odo t;\r\n" +
				"Binary Variable GeradorLigou(G, ConjPeriodos) Indica se o gerador ligou no per�odo t; \r\n" +
				"Binary Variable GeradorDesligou(G, ConjPeriodos) Indica se o gerador desligou no per�odo t; \r\n" + 
				"\r\n" + 
				"* Vari�veis relacionadas ao c�lculo de energia consumida e gerada\r\n" + 
				"Variable EnergiaGeradaGerDiesel(ConjPeriodos) Total de energia gerada pelo gerador diesel no per�odo t;\r\n" + 
				"Variable EnergiaGeradaGerEolico(ConjPeriodos) Total de energia gerada pelo gerador eolico no per�odo t;\r\n" + 
				"Variable EnergiaGeradaGerFotovoltaico(ConjPeriodos) Total de energia gerada pelo gerador fotovoltaico no per�odo t;\r\n" + 
				"Variable EnergiaGeradaBateria(ConjPeriodos) Total de energia gerada pelo banco de baterias no per�odo t;\r\n" + 
				"Variable TotalEnergiaProduzida(ConjPeriodos) Total de energia produzida por todos os REDs no per�odo t;\r\n" + 
				"\r\n" + 
				"Equations\r\n" + 
				"*        Fun��o objetivo para o problema de minimiza��o do custo operacional no gerenciamento de uma microrrede\r\n" + 
				"         CustoFinal define the objetive function\r\n" + 
				"\r\n" +
				"         CalculaCustoParcial(ConjPeriodos) C�lculo do custo de opera��o para os per�odos\r\n" +
				"		  CalcularPrevisaoRenovaveis(ConjPeriodos) previsao de geracao e�lica para o per�odo t" +
				"\r\n" +
				"*        Balan�o de pot�ncia na microrrede\r\n" + 
				"         BalancoPotencia(ConjPeriodos) Balanco de potencia no periodo t\r\n" + 
				"\r\n" + 
				"*        Load_consume(ConjPeriodos) Consumo real das cargas priorit�rias para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas �s cargas despach�veis\r\n" + 
				"         RespostaDemanda(ConjPeriodos) Aplica��o da resposta da demanda para o per�odo t\r\n" + 
				"         ConsumoDemanda(ConjPeriodos) Consumo real das cargas despach�veis para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionada ao gerador diesel\r\n" +
				"		  IndicarOperacaoGeradorDiesel(G, ConjPeriodos) Indica��o se o gerador ligou ou desligou no periodo t \r\n" + 
				"         CalcularCustoGerador(ConjPeriodos) C�lculo do custo de gera��o � diesel no per�odo t \r\n" + 
				"         CalcularGeracaoDiesel(G, ConjPeriodos) Gera��o no per�odo t\r\n" + 
				"         CalcularConsumoGeradorDiesel(ConjPeriodos) Consumo de diesel para o gerador g no per�odo t \r\n" + 
				"         LimiteSuperiorGeracaoDiesel(G, ConjPeriodos) Limite m�ximo de gera��o para o gerador diesel \r\n" + 
				"         LimiteInferiorGeracaoDiesel(G, ConjPeriodos) Limite m�nimo de gera��o para o gerador diesel \r\n" + 
				"         LinearizarGeradorDiesel(G, ConjPeriodos) Restri��o da fun��o sos2 definindo um �nico intervalo linear para pi \r\n" + 
				"         LimiteRampaSubidaGeradorDiesel(G, ConjPeriodos) Restri��o de rampa de subida para gera��o a diesel \r\n" + 
				"         LimiteRampaDescidaGeradorDiesel(G, ConjPeriodos) Restri��o de rampa de descida para a gera��o a diesel \r\n" + 
				"\r\n" + 
				"         CalcularTotalPotenciaProduzida(ConjPeriodos) calculo do total gerado na microrrede no periodo t \r\n" +
				"         CalcularPGER(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t \r\n " +
				"         CalcularPGE(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t \r\n" + 
				"         CalcularPNS(DL, ConjPeriodos) C�lculo da energia n�o suprida para as cargas no per�odo t \r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador e�lico\r\n" + 
				"         LimiteGeracaoEolica(W, ConjPeriodos) Limite superior de gera��o da gera��o e�lica no periodo t\r\n" + 
				"         LimiteSuperiorGeracaoEolica(W, ConjPeriodos) gera��o e�lica m�xima para o per�odo t\r\n" + 
				"         LimiteInferiorGeracaoEolica(W, ConjPeriodos) gera��o e�lica m�nima para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador fotovoltaico\r\n" + 
				"         LimiteGeracaoSolar(P, ConjPeriodos) Limite superior de gera��o solar no per�odo t\r\n" + 
				"         LimiteSuperiorGeracaoSolar(P, ConjPeriodos) Gera��o solar m�xima para o per�odo t\r\n" + 
				"         LimiteInferiorGeracaoSolar(P, ConjPeriodos) Gera��o solar m�nima para o per�odo t\r\n" +
				"\r\n" + 
				"*        Restri��es relacionadas a bateria\r\n" + 
				"         BalancoPotenciaBateria(B, ConjPeriodos) balan�o de pot�ncia da bateria\r\n" + 
				"         CargaMinimaBateria(B, ConjPeriodos) valor m�nimo para armazenamento da bateria\r\n" + 
				"         CargaMaximaBateria(B, ConjPeriodos) valor m�ximo para armazenamento da bateria\r\n" + 
				"         LimiteSuperiorCarregamentoBateria(B, ConjPeriodos) carregamento da bateria i para o per�odo t\r\n" + 
				"         LimiteInferiorCarregamentoBateria(B, ConjPeriodos) limite inferior para carregamento da bateria\r\n" + 
				"         LimiteCarregamentoBateria(B, ConjPeriodos) limite de carregamento considerando energia armazenada na bateria\r\n" + 
				"         LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos) descarregamento m�ximo da bateria no per�odo t\r\n" + 
				"         LimiteInferiorDescarregamentoBateria(B, ConjPeriodos) descarregamento m�nimo da bateria no per�odo t\r\n" + 
//				"         LimiteDescarregamentoBateria(B, ConjPeriodos) limite de descarregamento considerando energia armazenada na bateria\r\n" + 
				"         CalcularSOCBateria(B, ConjPeriodos) SoC da bateria no per�odo t\r\n" + 
				"\r\n" + 
				"*        C�lculo da gera��o de energia pelas REDs\r\n" + 
				"         CalculaGeracaoDiesel(ConjPeriodos) calcula a geracao de energia a diesel no per�odo t\r\n" + 
				"         CalculaGeracaoEolica(ConjPeriodos) calcula a geracao de energia eolica no per�odo t\r\n" + 
				"         CalculaGeracaoFotovoltaica(ConjPeriodos) calcula a geracao de energia fotovoltaica no per�odo t\r\n" + 
				"         CalculaGeracaoBateria(ConjPeriodos) calcula a geracao de energia pela bateria no per�odo t\r\n" + 
				"         CalculaTotalEnergiaProduzida(ConjPeriodos) calcula o total de energia gerada na microrrede pelos DERs\r\n" +
				"         CalculaPrevisaoCargas(DL, ConjPeriodos) calcula a previs�o de consumo das cargas;\r\n" + 
				"\r\n" +  
				"CustoFinal..   CustoTotal =e= sum(ConjPeriodos, CustoGerador(ConjPeriodos) ) + sum((ConjPeriodos,ConjGerEolica), (CustoGerEolica * PotenciaGeradaEolica(ConjGerEolica,ConjPeriodos) * dt(ConjPeriodos))) + sum((ConjPeriodos,ConjGerSolar), (CustoGerSolar * PotenciaGeradaSolar(ConjGerSolar,ConjPeriodos) * dt(ConjPeriodos))) - sum((ConjPeriodos, ConjBaterias), BateriaDescarregamento(ConjBaterias, ConjPeriodos) * CustoBateria) + sum((ConjPeriodos, ConjBaterias), BateriaCarregamento(ConjBaterias, ConjPeriodos) * CustoBateria) + sum((DL, ConjPeriodos), PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL)) + sum(ConjPeriodos, PotenciaGeradaExcessoRenovaveis(ConjPeriodos) * 0.2);\r\n" + 
				"\r\n" +
				"CalculaCustoParcial(ConjPeriodos).. CustoPorPeriodos(ConjPeriodos) =e= CustoGerador(ConjPeriodos) + sum(ConjGerEolica, (CustoGerEolica * PotenciaGeradaEolica(ConjGerEolica,ConjPeriodos) * dt(ConjPeriodos))) + sum(ConjGerSolar, (CustoGerSolar * PotenciaGeradaSolar(ConjGerSolar,ConjPeriodos) * dt(ConjPeriodos))) - sum(ConjBaterias, BateriaDescarregamento(ConjBaterias, ConjPeriodos) * 0.01) + sum(ConjBaterias, BateriaCarregamento(ConjBaterias, ConjPeriodos) * 0.01) + sum(DL, PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL));" +
				"\r\n" +
				"BalancoPotencia(ConjPeriodos).. sum(G, PotenciaGeradaDiesel(G, ConjPeriodos)) + sum(W, PotenciaGeradaEolica(W, ConjPeriodos))+ sum(P, PotenciaGeradaSolar(P, ConjPeriodos)) + sum(B, BateriaDescarregamento(B, ConjPeriodos)) =e= sum(B, BateriaCarregamento(B, ConjPeriodos)) + PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"\r\n" +
				"IndicarOperacaoGeradorDiesel(G, ConjPeriodos).. GeradorLigou(G, ConjPeriodos) - GeradorDesligou(G, ConjPeriodos) =e= StatusGerDiesel(G, ConjPeriodos) - StatusGerDiesel(G, ConjPeriodos - 1) - EstadoInicialGerador;\r\n" + 
				"CalcularCustoGerador(ConjPeriodos).. CustoGerador(ConjPeriodos) =e= (ConsumoDiesel(ConjPeriodos) * CustoCombustivel * dt(ConjPeriodos)) + (sum(G, GeradorLigou(G, ConjPeriodos) * CustoOperacaoGeradorDiesel)) + (sum(G, GeradorDesligou(G, ConjPeriodos) * CustoOperacaoGeradorDiesel));\r\n" + 
				"LimiteSuperiorGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =l= ParGerDieselMaxima(G) * StatusGerDiesel(G, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =g= ParGerDieselMinima(G) * StatusGerDiesel(G, ConjPeriodos);\r\n" + 
				"CalcularGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =e= sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao) * ParGerDieselConsumoLinearizado(G, ConjILinearizacao));\r\n" + 
				"CalcularConsumoGeradorDiesel(ConjPeriodos).. ConsumoDiesel(ConjPeriodos) =e= sum((G, ConjILinearizacao), lam(ConjPeriodos, ConjILinearizacao) * consumoger(G, ConjILinearizacao));\r\n" + 
				"LinearizarGeradorDiesel(G, ConjPeriodos).. sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao)) =e= 1;\r\n" + 
				"LimiteRampaSubidaGeradorDiesel(G, ConjPeriodos)$(ord(ConjPeriodos) > 1).. PotenciaGeradaDiesel(G, ConjPeriodos) - PotenciaGeradaDiesel(G, ConjPeriodos - 1) =l= GerDieselLimiteRampaSubida * (dt(ConjPeriodos) / 0.08333);\r\n" + 
				"LimiteRampaDescidaGeradorDiesel(G, ConjPeriodos)$(ord(ConjPeriodos) > 1).. PotenciaGeradaDiesel(G, ConjPeriodos) - PotenciaGeradaDiesel(G, ConjPeriodos - 1) =g= GerDieselLimiteRampaDescida * (dt(ConjPeriodos) / 0.08333);\r\n" + 
				"\r\n" + 
				"*Load_consume(ConjPeriodos).. ConsumoCargasPrioritarias(ConjPeriodos) =e= sum(L, PrevisaoCargas(L, ConjPeriodos) * (1 - StatusCargas(L, ConjPeriodos)));\r\n" + 
				"\r\n" + 
				"CalcularTotalPotenciaProduzida(ConjPeriodos).. TotalPotenciaProduzida(ConjPeriodos) =e=  sum(W, PotenciaGeradaEolica(W, ConjPeriodos)) + sum(P, PotenciaGeradaSolar(P, ConjPeriodos)) + sum(B, BateriaDescarregamento(B, ConjPeriodos)) + sum(G, PotenciaGeradaDiesel(G, ConjPeriodos));\r\n" +
				"CalcularPrevisaoRenovaveis(ConjPeriodos).. PrevisaoRenovaveis(ConjPeriodos) =e= sum(P, PrevisaoGerSolar(P, ConjPeriodos)) + sum(W, PrevisaoGerEolica(W, ConjPeriodos));\r\n" +
				"CalcularPGER(ConjPeriodos).. PotenciaGeradaExcessoRenovaveis(ConjPeriodos) =e= PrevisaoRenovaveis(ConjPeriodos) - (sum(W, PotenciaGeradaEolica(W, ConjPeriodos)) + sum(P, PotenciaGeradaSolar(P, ConjPeriodos)));" +
				"CalcularPGE(ConjPeriodos).. PotenciaGeradaExcesso(ConjPeriodos) =e= TotalPotenciaProduzida(ConjPeriodos) - PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"CalcularPNS(DL, ConjPeriodos).. PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) =e= (1 - StatusCargasDesp(DL, ConjPeriodos)) * PrevisaoCargasDesp(DL, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"* Restri��es relacionadas ao balan�o de pot�ncia\r\n" + 
				"RespostaDemanda(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"ConsumoDemanda(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"\r\n" + 
				"LimiteGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =l= ParGerEolicaMaxima(W);\r\n" + 
				"LimiteSuperiorGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =l= PrevisaoGerEolica(W, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =g= 0;\r\n" + 
				"*WT_generation(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =e= PrevisaoGerEolica(W, ConjPeriodos) * StatusGerEolica(W, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"LimiteGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =l= ParGerFotovoltaicaMaxima(P);\r\n" + 
				"LimiteSuperiorGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =l= PrevisaoGerSolar(P, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =g= 0;\r\n" + 
				"*PT_generation(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =e= PrevisaoGerSolar(P, ConjPeriodos) * StatusGerSolar(P, ConjPeriodos);\r\n" + 
				"\r\n" +
				"BalancoPotenciaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos - 1) + ((BateriaCarregamento(B, ConjPeriodos - 1) - BateriaDescarregamento(B, ConjPeriodos - 1)) * dt(ConjPeriodos)) + ParBateriaSOCInicial(B, ConjPeriodos);\r\n" + 
				"CargaMinimaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =g= ParBateriaMinimaEnergia(B);\r\n" + 
				"CargaMaximaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =l= ParBateriaMaximaEnergia(B);\r\n" + 
				"LimiteSuperiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =l= ParBateriaLimiteCarregamento(B) * StatusBateria(B, ConjPeriodos);\r\n" + 
				"LimiteInferiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
				"LimiteCarregamentoBateria(B, ConjPeriodos).. ParBateriaMaximaEnergia(B) =g= BateriaEnergia(B, ConjPeriodos - 1) + BateriaCarregamento(B, ConjPeriodos);\r\n" + 
				"LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =l= ParBateriaLimiteDescarregamento(B) * (1 - StatusBateria(B, ConjPeriodos));\r\n" + 
				"LimiteInferiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
				"CalcularSOCBateria(B, ConjPeriodos).. BateriaSoC(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos) / ParBateriaMaximaEnergia(B) * 100;\r\n" +  
				"\r\n" + 
				"CalculaGeracaoDiesel(ConjPeriodos).. EnergiaGeradaGerDiesel(ConjPeriodos) =e= sum(G, PotenciaGeradaDiesel(G, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoEolica(ConjPeriodos).. EnergiaGeradaGerEolico(ConjPeriodos) =e= sum(W, PotenciaGeradaEolica(W, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoFotovoltaica(ConjPeriodos).. EnergiaGeradaGerFotovoltaico(ConjPeriodos) =e= sum(P, PotenciaGeradaSolar(P, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoBateria(ConjPeriodos).. EnergiaGeradaBateria(ConjPeriodos) =e= sum(B, BateriaDescarregamento(B, ConjPeriodos));\r\n" + 
				"CalculaTotalEnergiaProduzida(ConjPeriodos).. TotalEnergiaProduzida(ConjPeriodos) =e= EnergiaGeradaGerDiesel(ConjPeriodos) + EnergiaGeradaGerEolico(ConjPeriodos) + EnergiaGeradaGerFotovoltaico(ConjPeriodos) + EnergiaGeradaBateria(ConjPeriodos);\r\n" +
				"\r\n" +
				"CalculaPrevisaoCargas(DL, ConjPeriodos).. perfilCargas(DL, ConjPeriodos) =e= PrevisaoCargasDesp(DL, ConjPeriodos); \r\n" +
				"\r\n" + 
				"model microgrid / all /;\r\n" + 
				"\r\n" + 
				"solve microgrid using MIP minimizing CustoTotal;\r\n" + 
				"\r\n" + 
				"Display CustoTotal.l, CustoTotal.m";
		
		return this.model;
	}

// Este modelo utiliza a configura��o com dois geradores diesel
	public String buildModel3(BatteryData battery, MGConfiguration config) {
	
		if (battery.getCharging() > 0) {
			this.SoC = battery.getSoc() + (battery.getCharging() * 10 * 0.083333);
		} else if (battery.getDischarging() > 0) {
			this.SoC = battery.getSoc() - (battery.getDischarging() * 10 * 0.083333);
		} else {
			this.SoC = battery.getSoc();
		}
		
		//System.out.println("SOC na sa�da: " + this.SoC);
		
		if (this.SoC < 20.0) {
			this.SoC = 20.0;
		}
		
		this.model = "Sets\r\n" + 
				"         ConjGerDiesel   conjunto dos geradores a diesel                 / gen1 /\r\n" + 
				"         ConjPeriodos    conjunto dos per�odos da simula��o              / " + this.simulationData.getPeriods() + " /\r\n" + 
				"         ConjGerSolar    conjunto dos geradores fotovoltaicos            / photo1 /\r\n" + 
				"         ConjGerEolica   conjunto dos geradores eolicos                  / wind1 /\r\n" + 
				"         ConjCargasDespachaveis  conjunto das cargas despachaveis        / dloadA1, dloadB1, dloadC1, dloadD1 /\r\n" + 
				"         ConjBaterias    conjunto das baterias                           / bat1 /\r\n" + 
				"         ConjILinearizacao conjunto com valores para linearizacao do consumo do gerador a diese  / 1*6 /;\r\n" + 
				"\r\n" + 
				"* Defini��o dos alias (sinonimos) para os conjuntos criados, com o objetivo de simplificar\r\n" + 
				"* a chamada dos conjuntos no modelo matem�tico\r\n" + 
				"Alias(ConjGerDiesel, G);\r\n" + 
				"Alias(ConjGerEolica, W);\r\n" + 
				"Alias(ConjBaterias, B);\r\n" + 
				"Alias(ConjGerSolar, P);\r\n" + 
				"Alias(ConjCargasDespachaveis, DL);\r\n" + 
				"\r\n" + 
				"* Definicao dos custos de fornecimento dos Recursos Energicamente Distribuidos\r\n" + 
				"Scalar CustoGerEolica custo de gera��o por kW  para energia e�lica / 0.01 /;\r\n" + 
				"Scalar CustoGerSolar valor de gera��o por kW  para energia solar / 0.01 /;\r\n" + 
				"Scalar CustoBateria valor para utiliza��o da bateria / 0.001 /;\r\n" + 
				"Scalar CustoCombustivel custo de combustivel (R$ por litro) /4.1/;\r\n" +
				"Scalar CustoOperacaoGeradorDiesel custo de operacao do gerador / 10 /; \r\n" +
				"Scalar EstadoInicialGerador custo de operacao do gerador / " + Math.round(battery.getGeneratorStatus()) + " /; \r\n; " + 
				"\r\n" + 
				"* Definicao de constantes para a geracao a diesel\r\n" + 
				"Scalar GerDieselConstA parametro A do calculo de linearizacao /0.004446/;\r\n" + 
				"Scalar GerDieselConstB parametro B do calculo de linearizacao /0.121035/;\r\n" + 
				"Scalar GerDieselConstC parametro C do calculo de linearizacao /1.653882/;\r\n" + 
				"Scalar GerDieselLimiteRampaSubida limite de subida de geracao a diesel / 5 /;\r\n" + 
				"Scalar GerDieselLimiteRampaDescida limite de descida de geracao a diesel / -5 /;\r\n" + 
				"\r\n" + 
				"* Par�metros para configura��o de limites operacionais dos elementos da microrrede\r\n" + 
				"Parameters\r\n" + 
				"         ParGerDieselMaxima(ConjGerDiesel) limites maximo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 " + (config.getUseDieselGenerators() ? "20" : "0") + " / \r\n" + 
				"*           gen2 40 /\r\n" + 
				"*           gen3 500\r\n" + 
				"*           gen4 500 /\r\n" + 
				"\r\n" + 
				"         ParGerDieselMinima(ConjGerDiesel) limites minimo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 " + (config.getUseDieselGenerators() ? "4" : "0") + " / \r\n" + 
				"*           gen2 8 /\r\n" + 
				"*           gen3 130\r\n" + 
				"*           gen4 130 /\r\n" + 
				"\r\n" + 
				"         ParGerFotovoltaicaMaxima(ConjGerSolar) limites m�ximo de gera��o fotovoltaica em kW\r\n" + 
				"         / photo1 10 /\r\n" + 
				"\r\n" + 
				"         ParGerEolicaMaxima(ConjGerEolica) limites m�ximo da gera��o eolica em kW\r\n" + 
				"         / wind1 10 /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteCarregamento(ConjBaterias) limite de carregamento da bateria em kW\r\n" + 
				"         / bat1 " +  (config.getUseBattery() ? "10" : "0")  +  " /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteDescarregamento(ConjBaterias) limite d edescarregamento da bateria em kW\r\n" + 
				"         / bat1 " + (config.getUseBattery() ? "10" : "0") + " /\r\n" + 
				"*           bat2 -100 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMinimaEnergia(ConjBaterias) limite minimo de armazenamento da bateria SOC\r\n" + 
				"         / bat1 2 /\r\n" + 
				"*          bat2 60 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMaximaEnergia(ConjBaterias) limite maximo de armazenamento da bateria SOC\r\n" + 
				"         / bat1 10 /\r\n" + 
				"*           bat2 400 /\r\n" + 
				"\r\n" + 
				"         ParCargasDespachaveisPenalidade(ConjCargasDespachaveis) custo de penalizacao das cargas despach�veis\r\n" + 
				"         / dloadA1 100\r\n" + 
				"*           dloadA2 100\r\n" + 
				"           dloadB1 50\r\n" + 
				"*           dloadB2 50\r\n" + 
				"           dloadC1 25\r\n" + 
				"*           dloadC2 25\r\n" + 
				"           dloadD1 10/\r\n" + 
				"*           dloadD2 10 /\r\n" + 
				"\r\n" + 
				"Table ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) valores linearizados usados para a curva de consumo do gerador\r\n" + 
				"         1       2       3       4       5       6\r\n" + 
				"gen1     0.0     4.0     7.0     11.5    15.0    20.0;\r\n" +
				"*gen2     0.0     8.0     15.0    21.5    28.0    36.0;\r\n" +
				"\r\n" + 
				"Table consumoger(ConjGerDiesel, ConjILinearizacao) curva de consumo do gerador interpolada\r\n" + 
				"         1\r\n" + 
				"gen1     0;\r\n" +
				"*gen2     0;\r\n" + 
				"\r\n" + 
				"      consumoger(ConjGerDiesel, ConjILinearizacao)$(ord(ConjILinearizacao) > 1) = (GerDieselConstA * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) + GerDieselConstB * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) + GerDieselConstC);\r\n" + 
				"\r\n" + 
				"*DISCRETIZACAO EM 3 NIVEIS: na primeira meia hora otimiza de 5 em 5 min (dt=.083\r\n" + 
				"* horas), na segunda meia hora discretiza em .5 horas,\r\n" + 
				"* e no restante das 23 horas do dia, discretiza de\r\n" + 
				"* hora em hora (dt = 1 hora).\r\n" + 
				"Parameter dt(conjPeriodos)\r\n" + 
				this.simulationData.tDeltaToString() + "\n" +
//				"/t1*t19 0.08333\r\n" + 
				"\r\n" + 
				"Table    ParBateriaSOCInicial(B, ConjPeriodos) valor do SOC da bateria para o periodo inicial da simula��o\r\n" + 
				"         t1\r\n" + 
				"bat1     " + String.format("%3.5f ", (this.SoC / 10)).replace(",", ".") + ";\r\n" + 
				"\r\n" + 
				"Table    PrevisaoCargasDesp(ConjCargasDespachaveis, ConjPeriodos) previs�o de consumo das cargas despach�veis\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.comercialLoadToString().replace(",", ".") +
				this.simulationData.residentialHighLoadToString().replace(",", ".") +
				this.simulationData.residentialMediumLoadToString().replace(",", ".") +
				this.simulationData.residentialLowLoadToString().replace(",", ".") +
				"\r\n" + 
				"Table    PrevisaoGerSolar(ConjGerSolar, ConjPeriodos) previs�o de gera��o solar\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.powerSolarToString().replace(",", ".") + 
				"\r\n" + 
				"Table    PrevisaoGerEolica(ConjGerEolica, ConjPeriodos) previs�o de gera��o e�lica\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.powerWindToString().replace(",", ".") + 
				"\r\n" + 
				"Variable CustoTotal Custo total de opera��o;\r\n" + 
				"\r\n" + 
				"Positive Variable PotenciaGeradaEolica(ConjGerEolica, ConjPeriodos) Geracao eolica do gerador i no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaSolar(ConjGerSolar, ConjPeriodos) Geracao solar do gerador i no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaDiesel(ConjGerDiesel, ConjPeriodos) Geracao a diesel no periodo t;\r\n" + 
				"\r\n" + 
				"Variable perfilCargas(DL, ConjPeriodos) Previs�o de consumo das cargas para o per�odo t;\r\n" + 
				"\r\n" + 
				"Variable BateriaCarregamento(B, ConjPeriodos) Carregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaDescarregamento(B, ConjPeriodos) Descarregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaEnergia(B, ConjPeriodos) Energia armazenada na bateria no periodo t;\r\n" + 
				"Variable BateriaSoC(B, ConjPeriodos) Estado de carga (SoC) da bateria no periodo t;\r\n" + 
				"\r\n" + 
				"Variable ConsumoDiesel(ConjPeriodos) Consumo de combustivel do gerador a diesel no periodo t;\r\n" + 
				"*Variable ConsumoCargasPrioritarias(ConjPeriodos) Consumo das cargas prioritarias no periodo t;\r\n" + 
				"Variable ConsumoCargasDesp(ConjPeriodos) Consumo das cargas despachaveis no periodo t;\r\n" + 
				"\r\n" +
				"Variable CustoPorPeriodos(ConjPeriodos) Custo de opera��o da microrrede para cada per�odo t; \r\n" +
				"Variable CustoGerador(ConjPeriodos) Custo da gera��o � diesel por per�odo t; \r\n" +
				"\r\n" +
				"Variable PrevisaoRenovaveis(ConjPeriodos) previs�o da gera��o renov�vel para o per�odo t;\r\n" + 
				"Variable PotenciaGeradaExcessoRenovaveis(ConjPeriodos) previs�o de gera��o em excesso para renov�veis no per�odo t;\r\n" +
				"\r\n" +
				"Variable TotalPotenciaProduzida(ConjPeriodos) Total de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaExcesso(ConjPeriodos) Excesso de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) Potencia nao suprida das cargas despachaveis no periodo t;\r\n" + 
				"Variable PotenciaSupridaCargasDesp(ConjPeriodos) Potencia suprida para as cargas despachaveis no periodo t;\r\n" + 
				"\r\n" + 
				"*variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"sos2 variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"\r\n" + 
				"* VARI�VEIS BIN�RIAS\r\n" + 
				"Binary Variable StatusGerDiesel(G, ConjPeriodos) On-Off Status do gerador a diesel no per�odo t (0 desligado - 1 Ligado);\r\n" + 
				"Binary Variable StatusBateria(B, ConjPeriodos) Status of battery (0 descarregando - 1 carregando);\r\n" + 
				"Binary Variable StatusCargasDesp(DL, ConjPeriodos) On-Off Status das cargas despach�veis no per�odo t;\r\n" + 
				"*Binary Variable StatusCargas(L, ConjPeriodos) On-Off Status das cargas priorit�rias no per�odo t;\r\n" + 
				"Binary Variable StatusGerSolar(P, ConjPeriodos) On-Off Status do gerador fotovoltaico n no per�odo t;\r\n" + 
				"Binary Variable StatusGerEolica(W, ConjPeriodos) On-Off Status do gerador e�lico n no per�odo t;\r\n" +
				"Binary Variable GeradorLigou(G, ConjPeriodos) Indica se o gerador ligou no per�odo t; \r\n" +
				"Binary Variable GeradorDesligou(G, ConjPeriodos) Indica se o gerador desligou no per�odo t; \r\n" + 
				"\r\n" + 
				"* Vari�veis relacionadas ao c�lculo de energia consumida e gerada\r\n" + 
				"Variable EnergiaGeradaGerDiesel(ConjPeriodos) Total de energia gerada pelo gerador diesel no per�odo t;\r\n" + 
				"Variable EnergiaGeradaGerEolico(ConjPeriodos) Total de energia gerada pelo gerador eolico no per�odo t;\r\n" + 
				"Variable EnergiaGeradaGerFotovoltaico(ConjPeriodos) Total de energia gerada pelo gerador fotovoltaico no per�odo t;\r\n" + 
				"Variable EnergiaGeradaBateria(ConjPeriodos) Total de energia gerada pelo banco de baterias no per�odo t;\r\n" + 
				"Variable TotalEnergiaProduzida(ConjPeriodos) Total de energia produzida por todos os REDs no per�odo t;\r\n" + 
				"\r\n" + 
				"Equations\r\n" + 
				"*        Fun��o objetivo para o problema de minimiza��o do custo operacional no gerenciamento de uma microrrede\r\n" + 
				"         CustoFinal define the objetive function\r\n" + 
				"\r\n" +
				"         CalculaCustoParcial(ConjPeriodos) C�lculo do custo de opera��o para os per�odos\r\n" +
				"		  CalcularPrevisaoRenovaveis(ConjPeriodos) previsao de geracao e�lica para o per�odo t" +
				"\r\n" +
				"*        Balan�o de pot�ncia na microrrede\r\n" + 
				"         BalancoPotencia(ConjPeriodos) Balanco de potencia no periodo t\r\n" + 
				"\r\n" + 
				"*        Load_consume(ConjPeriodos) Consumo real das cargas priorit�rias para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas �s cargas despach�veis\r\n" + 
				"         RespostaDemanda(ConjPeriodos) Aplica��o da resposta da demanda para o per�odo t\r\n" + 
				"         ConsumoDemanda(ConjPeriodos) Consumo real das cargas despach�veis para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionada ao gerador diesel\r\n" +
				"		  IndicarOperacaoGeradorDiesel(G, ConjPeriodos) Indica��o se o gerador ligou ou desligou no periodo t \r\n" + 
				"         CalcularCustoGerador(ConjPeriodos) C�lculo do custo de gera��o � diesel no per�odo t \r\n" + 
				"         CalcularGeracaoDiesel(G, ConjPeriodos) Gera��o no per�odo t\r\n" + 
				"         CalcularConsumoGeradorDiesel(ConjPeriodos) Consumo de diesel para o gerador g no per�odo t \r\n" + 
				"         LimiteSuperiorGeracaoDiesel(G, ConjPeriodos) Limite m�ximo de gera��o para o gerador diesel \r\n" + 
				"         LimiteInferiorGeracaoDiesel(G, ConjPeriodos) Limite m�nimo de gera��o para o gerador diesel \r\n" + 
				"         LinearizarGeradorDiesel(G, ConjPeriodos) Restri��o da fun��o sos2 definindo um �nico intervalo linear para pi \r\n" + 
				"         LimiteRampaSubidaGeradorDiesel(G, ConjPeriodos) Restri��o de rampa de subida para gera��o a diesel \r\n" + 
				"         LimiteRampaDescidaGeradorDiesel(G, ConjPeriodos) Restri��o de rampa de descida para a gera��o a diesel \r\n" + 
				"\r\n" + 
				"         CalcularTotalPotenciaProduzida(ConjPeriodos) calculo do total gerado na microrrede no periodo t \r\n" +
				"         CalcularPGER(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t \r\n " +
				"         CalcularPGE(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t \r\n" + 
				"         CalcularPNS(DL, ConjPeriodos) C�lculo da energia n�o suprida para as cargas no per�odo t \r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador e�lico\r\n" + 
				"         LimiteGeracaoEolica(W, ConjPeriodos) Limite superior de gera��o da gera��o e�lica no periodo t\r\n" + 
				"         LimiteSuperiorGeracaoEolica(W, ConjPeriodos) gera��o e�lica m�xima para o per�odo t\r\n" + 
				"         LimiteInferiorGeracaoEolica(W, ConjPeriodos) gera��o e�lica m�nima para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador fotovoltaico\r\n" + 
				"         LimiteGeracaoSolar(P, ConjPeriodos) Limite superior de gera��o solar no per�odo t\r\n" + 
				"         LimiteSuperiorGeracaoSolar(P, ConjPeriodos) Gera��o solar m�xima para o per�odo t\r\n" + 
				"         LimiteInferiorGeracaoSolar(P, ConjPeriodos) Gera��o solar m�nima para o per�odo t\r\n" +
				"\r\n" + 
				"*        Restri��es relacionadas a bateria\r\n" + 
				"         BalancoPotenciaBateria(B, ConjPeriodos) balan�o de pot�ncia da bateria\r\n" + 
				"         CargaMinimaBateria(B, ConjPeriodos) valor m�nimo para armazenamento da bateria\r\n" + 
				"         CargaMaximaBateria(B, ConjPeriodos) valor m�ximo para armazenamento da bateria\r\n" + 
				"         LimiteSuperiorCarregamentoBateria(B, ConjPeriodos) carregamento da bateria i para o per�odo t\r\n" + 
				"         LimiteInferiorCarregamentoBateria(B, ConjPeriodos) limite inferior para carregamento da bateria\r\n" + 
				"         LimiteCarregamentoBateria(B, ConjPeriodos) limite de carregamento considerando energia armazenada na bateria\r\n" + 
				"         LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos) descarregamento m�ximo da bateria no per�odo t\r\n" + 
				"         LimiteInferiorDescarregamentoBateria(B, ConjPeriodos) descarregamento m�nimo da bateria no per�odo t\r\n" + 
//				"         LimiteDescarregamentoBateria(B, ConjPeriodos) limite de descarregamento considerando energia armazenada na bateria\r\n" + 
				"         CalcularSOCBateria(B, ConjPeriodos) SoC da bateria no per�odo t\r\n" + 
				"\r\n" + 
				"*        C�lculo da gera��o de energia pelas REDs\r\n" + 
				"         CalculaGeracaoDiesel(ConjPeriodos) calcula a geracao de energia a diesel no per�odo t\r\n" + 
				"         CalculaGeracaoEolica(ConjPeriodos) calcula a geracao de energia eolica no per�odo t\r\n" + 
				"         CalculaGeracaoFotovoltaica(ConjPeriodos) calcula a geracao de energia fotovoltaica no per�odo t\r\n" + 
				"         CalculaGeracaoBateria(ConjPeriodos) calcula a geracao de energia pela bateria no per�odo t\r\n" + 
				"         CalculaTotalEnergiaProduzida(ConjPeriodos) calcula o total de energia gerada na microrrede pelos DERs\r\n" +
				"         CalculaPrevisaoCargas(DL, ConjPeriodos) calcula a previs�o de consumo das cargas;\r\n" + 
				"\r\n" +  
				"CustoFinal..   CustoTotal =e= sum(ConjPeriodos, CustoGerador(ConjPeriodos) ) + sum((ConjPeriodos,ConjGerEolica), (CustoGerEolica * PotenciaGeradaEolica(ConjGerEolica,ConjPeriodos) * dt(ConjPeriodos))) + sum((ConjPeriodos,ConjGerSolar), (CustoGerSolar * PotenciaGeradaSolar(ConjGerSolar,ConjPeriodos) * dt(ConjPeriodos))) - sum((ConjPeriodos, ConjBaterias), BateriaDescarregamento(ConjBaterias, ConjPeriodos) * CustoBateria) + sum((ConjPeriodos, ConjBaterias), BateriaCarregamento(ConjBaterias, ConjPeriodos) * CustoBateria) + sum((DL, ConjPeriodos), PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL)) + sum(ConjPeriodos, PotenciaGeradaExcessoRenovaveis(ConjPeriodos) * 0.2);\r\n" + 
				"\r\n" +
				"CalculaCustoParcial(ConjPeriodos).. CustoPorPeriodos(ConjPeriodos) =e= CustoGerador(ConjPeriodos) + sum(ConjGerEolica, (CustoGerEolica * PotenciaGeradaEolica(ConjGerEolica,ConjPeriodos) * dt(ConjPeriodos))) + sum(ConjGerSolar, (CustoGerSolar * PotenciaGeradaSolar(ConjGerSolar,ConjPeriodos) * dt(ConjPeriodos))) - sum(ConjBaterias, BateriaDescarregamento(ConjBaterias, ConjPeriodos) * 0.01) + sum(ConjBaterias, BateriaCarregamento(ConjBaterias, ConjPeriodos) * 0.01) + sum(DL, PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL));" +
				"\r\n" +
				"BalancoPotencia(ConjPeriodos).. sum(G, PotenciaGeradaDiesel(G, ConjPeriodos)) + sum(W, PotenciaGeradaEolica(W, ConjPeriodos))+ sum(P, PotenciaGeradaSolar(P, ConjPeriodos)) + sum(B, BateriaDescarregamento(B, ConjPeriodos)) =e= sum(B, BateriaCarregamento(B, ConjPeriodos)) + PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"\r\n" +
//				"IndicarOperacaoGeradorDiesel(G, ConjPeriodos).. GeradorLigou(G, ConjPeriodos) - GeradorDesligou(G, ConjPeriodos) =e= StatusGerDiesel(G, ConjPeriodos) - StatusGerDiesel(G, ConjPeriodos - 1);\r\n" + 
				"IndicarOperacaoGeradorDiesel(G, ConjPeriodos).. GeradorLigou(G, ConjPeriodos) - GeradorDesligou(G, ConjPeriodos) =e= StatusGerDiesel(G, ConjPeriodos) - StatusGerDiesel(G, ConjPeriodos - 1) - EstadoInicialGerador;\r\n" +
				"CalcularCustoGerador(ConjPeriodos).. CustoGerador(ConjPeriodos) =e= (ConsumoDiesel(ConjPeriodos) * CustoCombustivel * dt(ConjPeriodos)) + (sum(G, GeradorLigou(G, ConjPeriodos) * CustoOperacaoGeradorDiesel)) + (sum(G, GeradorDesligou(G, ConjPeriodos) * CustoOperacaoGeradorDiesel));\r\n" + 
				"LimiteSuperiorGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =l= ParGerDieselMaxima(G) * StatusGerDiesel(G, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =g= ParGerDieselMinima(G) * StatusGerDiesel(G, ConjPeriodos);\r\n" + 
				"CalcularGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =e= sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao) * ParGerDieselConsumoLinearizado(G, ConjILinearizacao));\r\n" + 
				"CalcularConsumoGeradorDiesel(ConjPeriodos).. ConsumoDiesel(ConjPeriodos) =e= sum((G, ConjILinearizacao), lam(ConjPeriodos, ConjILinearizacao) * consumoger(G, ConjILinearizacao));\r\n" + 
				"LinearizarGeradorDiesel(G, ConjPeriodos).. sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao)) =e= 1;\r\n" + 
				"LimiteRampaSubidaGeradorDiesel(G, ConjPeriodos)$(ord(ConjPeriodos) > 1).. PotenciaGeradaDiesel(G, ConjPeriodos) - PotenciaGeradaDiesel(G, ConjPeriodos - 1) =l= GerDieselLimiteRampaSubida * (dt(ConjPeriodos) / 0.08333);\r\n" + 
				"LimiteRampaDescidaGeradorDiesel(G, ConjPeriodos)$(ord(ConjPeriodos) > 1).. PotenciaGeradaDiesel(G, ConjPeriodos) - PotenciaGeradaDiesel(G, ConjPeriodos - 1) =g= GerDieselLimiteRampaDescida * (dt(ConjPeriodos) / 0.08333);\r\n" + 
				"\r\n" + 
				"*Load_consume(ConjPeriodos).. ConsumoCargasPrioritarias(ConjPeriodos) =e= sum(L, PrevisaoCargas(L, ConjPeriodos) * (1 - StatusCargas(L, ConjPeriodos)));\r\n" + 
				"\r\n" + 
				"CalcularTotalPotenciaProduzida(ConjPeriodos).. TotalPotenciaProduzida(ConjPeriodos) =e=  sum(W, PotenciaGeradaEolica(W, ConjPeriodos)) + sum(P, PotenciaGeradaSolar(P, ConjPeriodos)) + sum(B, BateriaDescarregamento(B, ConjPeriodos)) + sum(G, PotenciaGeradaDiesel(G, ConjPeriodos));\r\n" +
				"CalcularPrevisaoRenovaveis(ConjPeriodos).. PrevisaoRenovaveis(ConjPeriodos) =e= sum(P, PrevisaoGerSolar(P, ConjPeriodos)) + sum(W, PrevisaoGerEolica(W, ConjPeriodos));\r\n" +
				"CalcularPGER(ConjPeriodos).. PotenciaGeradaExcessoRenovaveis(ConjPeriodos) =e= PrevisaoRenovaveis(ConjPeriodos) - (sum(W, PotenciaGeradaEolica(W, ConjPeriodos)) + sum(P, PotenciaGeradaSolar(P, ConjPeriodos)));" +
				"CalcularPGE(ConjPeriodos).. PotenciaGeradaExcesso(ConjPeriodos) =e= TotalPotenciaProduzida(ConjPeriodos) - PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"CalcularPNS(DL, ConjPeriodos).. PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) =e= (1 - StatusCargasDesp(DL, ConjPeriodos)) * PrevisaoCargasDesp(DL, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"* Restri��es relacionadas ao balan�o de pot�ncia\r\n" + 
				"RespostaDemanda(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"ConsumoDemanda(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"\r\n" + 
				"LimiteGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =l= ParGerEolicaMaxima(W);\r\n" + 
				"LimiteSuperiorGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =l= PrevisaoGerEolica(W, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =g= 0;\r\n" + 
				"*WT_generation(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =e= PrevisaoGerEolica(W, ConjPeriodos) * StatusGerEolica(W, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"LimiteGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =l= ParGerFotovoltaicaMaxima(P);\r\n" + 
				"LimiteSuperiorGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =l= PrevisaoGerSolar(P, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =g= 0;\r\n" + 
				"*PT_generation(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =e= PrevisaoGerSolar(P, ConjPeriodos) * StatusGerSolar(P, ConjPeriodos);\r\n" + 
				"\r\n" +
				"BalancoPotenciaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos - 1) + ((BateriaCarregamento(B, ConjPeriodos - 1) - BateriaDescarregamento(B, ConjPeriodos - 1)) * dt(ConjPeriodos)) + ParBateriaSOCInicial(B, ConjPeriodos);\r\n" + 
				"CargaMinimaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =g= ParBateriaMinimaEnergia(B);\r\n" + 
				"CargaMaximaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =l= ParBateriaMaximaEnergia(B);\r\n" + 
				"LimiteSuperiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =l= ParBateriaLimiteCarregamento(B) * StatusBateria(B, ConjPeriodos);\r\n" + 
				"LimiteInferiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
				"LimiteCarregamentoBateria(B, ConjPeriodos).. ParBateriaMaximaEnergia(B) =g= BateriaEnergia(B, ConjPeriodos - 1) + BateriaCarregamento(B, ConjPeriodos);\r\n" + 
				"LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =l= ParBateriaLimiteDescarregamento(B) * (1 - StatusBateria(B, ConjPeriodos));\r\n" + 
				"LimiteInferiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
				"CalcularSOCBateria(B, ConjPeriodos).. BateriaSoC(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos) / ParBateriaMaximaEnergia(B) * 100;\r\n" +  
				"\r\n" + 
				"CalculaGeracaoDiesel(ConjPeriodos).. EnergiaGeradaGerDiesel(ConjPeriodos) =e= sum(G, PotenciaGeradaDiesel(G, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoEolica(ConjPeriodos).. EnergiaGeradaGerEolico(ConjPeriodos) =e= sum(W, PotenciaGeradaEolica(W, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoFotovoltaica(ConjPeriodos).. EnergiaGeradaGerFotovoltaico(ConjPeriodos) =e= sum(P, PotenciaGeradaSolar(P, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoBateria(ConjPeriodos).. EnergiaGeradaBateria(ConjPeriodos) =e= sum(B, BateriaDescarregamento(B, ConjPeriodos));\r\n" + 
				"CalculaTotalEnergiaProduzida(ConjPeriodos).. TotalEnergiaProduzida(ConjPeriodos) =e= EnergiaGeradaGerDiesel(ConjPeriodos) + EnergiaGeradaGerEolico(ConjPeriodos) + EnergiaGeradaGerFotovoltaico(ConjPeriodos) + EnergiaGeradaBateria(ConjPeriodos);\r\n" +
				"\r\n" +
				"CalculaPrevisaoCargas(DL, ConjPeriodos).. perfilCargas(DL, ConjPeriodos) =e= PrevisaoCargasDesp(DL, ConjPeriodos); \r\n" +
				"\r\n" + 
				"model microgrid / all /;\r\n" + 
				"\r\n" + 
				"solve microgrid using MIP minimizing CustoTotal;\r\n" + 
				"\r\n" + 
				"Display CustoTotal.l, CustoTotal.m";
		
		return this.model;
}
	
	public String buildModel2Gen(BatteryData battery, MGConfiguration config) {
		
		if (battery.getCharging() > 0) {
			this.SoC = battery.getSoc() + (battery.getCharging() * 10 * 0.083333);
		} else if (battery.getDischarging() > 0) {
			this.SoC = battery.getSoc() - (battery.getDischarging() * 10 * 0.083333);
		} else {
			this.SoC = battery.getSoc();
		}
		
		if (this.SoC < 20.0) {
			this.SoC = 20.0;
		}
		
		this.model = "Sets\r\n" + 
				"         ConjGerDiesel   conjunto dos geradores a diesel                 / gen1, gen2 /\r\n" + 
				"         ConjPeriodos    conjunto dos per�odos da simula��o              / " + this.simulationData.getPeriods() + " /\r\n" + 
				"         ConjGerSolar    conjunto dos geradores fotovoltaicos            / photo1 /\r\n" + 
				"         ConjGerEolica   conjunto dos geradores eolicos                  / wind1 /\r\n" + 
				"         ConjCargasDespachaveis  conjunto das cargas despachaveis        / dloadA1, dloadB1, dloadC1, dloadD1 /\r\n" + 
				"         ConjBaterias    conjunto das baterias                           / bat1 /\r\n" + 
				"         ConjILinearizacao conjunto com valores para linearizacao do consumo do gerador a diese  / 1*6 /;\r\n" + 
				"\r\n" + 
				"* Defini��o dos alias (sinonimos) para os conjuntos criados, com o objetivo de simplificar\r\n" + 
				"* a chamada dos conjuntos no modelo matem�tico\r\n" + 
				"Alias(ConjGerDiesel, G);\r\n" + 
				"Alias(ConjGerEolica, W);\r\n" + 
				"Alias(ConjBaterias, B);\r\n" + 
				"Alias(ConjGerSolar, P);\r\n" + 
				"Alias(ConjCargasDespachaveis, DL);\r\n" + 
				"\r\n" + 
				"* Definicao dos custos de fornecimento dos Recursos Energicamente Distribuidos\r\n" + 
				"Scalar CustoGerEolica custo de gera��o por kW  para energia e�lica / 0.01 /;\r\n" + 
				"Scalar CustoGerSolar valor de gera��o por kW  para energia solar / 0.01 /;\r\n" + 
				"Scalar CustoBateria valor para utiliza��o da bateria / 0.001 /;\r\n" + 
				"Scalar CustoCombustivel custo de combustivel (R$ por litro) /4.1/;\r\n" +
				"Scalar CustoOperacaoGeradorDiesel custo de operacao do gerador / 10 /; \r\n" +
				"Scalar EstadoInicialGerador custo de operacao do gerador / " + Math.round(battery.getGeneratorStatus()) + " /; \r\n; " + 
				"\r\n" + 
				"* Definicao de constantes para a geracao a diesel\r\n" + 
				"Scalar GerDieselConstA parametro A do calculo de linearizacao /0.004446/;\r\n" + 
				"Scalar GerDieselConstB parametro B do calculo de linearizacao /0.121035/;\r\n" + 
				"Scalar GerDieselConstC parametro C do calculo de linearizacao /1.653882/;\r\n" + 
				"Scalar GerDieselLimiteRampaSubida limite de subida de geracao a diesel / 5 /;\r\n" + 
				"Scalar GerDieselLimiteRampaDescida limite de descida de geracao a diesel / -5 /;\r\n" + 
				"\r\n" + 
				"* Par�metros para configura��o de limites operacionais dos elementos da microrrede\r\n" + 
				"Parameters\r\n" + 
				"         ParGerDieselMaxima(ConjGerDiesel) limites maximo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 " + (config.getUseDieselGenerators() ? "20" : "0") + " \r\n" + 
				"           gen2 40 /\r\n" + 
				"*           gen3 500\r\n" + 
				"*           gen4 500 /\r\n" + 
				"\r\n" + 
				"         ParGerDieselMinima(ConjGerDiesel) limites minimo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 " + (config.getUseDieselGenerators() ? "4" : "0") + " \r\n" + 
				"           gen2 8 /\r\n" + 
				"*           gen3 130\r\n" + 
				"*           gen4 130 /\r\n" + 
				"\r\n" + 
				"         ParGerFotovoltaicaMaxima(ConjGerSolar) limites m�ximo de gera��o fotovoltaica em kW\r\n" + 
				"         / photo1 10 /\r\n" + 
				"\r\n" + 
				"         ParGerEolicaMaxima(ConjGerEolica) limites m�ximo da gera��o eolica em kW\r\n" + 
				"         / wind1 10 /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteCarregamento(ConjBaterias) limite de carregamento da bateria em kW\r\n" + 
				"         / bat1 " +  (config.getUseBattery() ? "10" : "0")  +  " /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteDescarregamento(ConjBaterias) limite d edescarregamento da bateria em kW\r\n" + 
				"         / bat1 " + (config.getUseBattery() ? "10" : "0") + " /\r\n" + 
				"*           bat2 -100 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMinimaEnergia(ConjBaterias) limite minimo de armazenamento da bateria SOC\r\n" + 
				"         / bat1 2 /\r\n" + 
				"*          bat2 60 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMaximaEnergia(ConjBaterias) limite maximo de armazenamento da bateria SOC\r\n" + 
				"         / bat1 10 /\r\n" + 
				"*           bat2 400 /\r\n" + 
				"\r\n" + 
				"         ParCargasDespachaveisPenalidade(ConjCargasDespachaveis) custo de penalizacao das cargas despach�veis\r\n" + 
				"         / dloadA1 30\r\n" + 
				"*           dloadA2 30\r\n" + 
				"           dloadB1 20\r\n" + 
				"*           dloadB2 20\r\n" + 
				"           dloadC1 15\r\n" + 
				"*           dloadC2 10\r\n" + 
				"           dloadD1 10 /\r\n" + 
				"*           dloadD2 10 /\r\n" + 
				"\r\n" + 
				"Table ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) valores linearizados usados para a curva de consumo do gerador\r\n" + 
				"         1       2       3       4       5       6\r\n" + 
				"gen1     0.0     4.0     7.0     11.5    15.0    20.0\r\n" +
				"gen2     0.0     8.0     15.0    21.5    28.0    36.0;\r\n" +
				"\r\n" + 
				"Table consumoger(ConjGerDiesel, ConjILinearizacao) curva de consumo do gerador interpolada\r\n" + 
				"         1\r\n" + 
				"gen1     0\r\n" +
				"gen2     0;\r\n" + 
				"\r\n" + 
				"      consumoger(ConjGerDiesel, ConjILinearizacao)$(ord(ConjILinearizacao) > 1) = (GerDieselConstA * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) + GerDieselConstB * ParGerDieselConsumoLinearizado(ConjGerDiesel, ConjILinearizacao) + GerDieselConstC);\r\n" + 
				"\r\n" + 
				"*DISCRETIZACAO EM 3 NIVEIS: na primeira meia hora otimiza de 5 em 5 min (dt=.083\r\n" + 
				"* horas), na segunda meia hora discretiza em .5 horas,\r\n" + 
				"* e no restante das 23 horas do dia, discretiza de\r\n" + 
				"* hora em hora (dt = 1 hora).\r\n" + 
				"Parameter dt(conjPeriodos)\r\n" + 
				this.simulationData.tDeltaToString() + "\n" +
//				"/t1*t19 0.08333\r\n" + 
				"\r\n" + 
				"Table    ParBateriaSOCInicial(B, ConjPeriodos) valor do SOC da bateria para o periodo inicial da simula��o\r\n" + 
				"         t1\r\n" + 
				"bat1     " + String.format("%3.5f ", (this.SoC / 10)).replace(",", ".") + ";\r\n" + 
				"\r\n" + 
				"Table    PrevisaoCargasDesp(ConjCargasDespachaveis, ConjPeriodos) previs�o de consumo das cargas despach�veis\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.comercialLoadToString().replace(",", ".") +
				this.simulationData.residentialHighLoadToString().replace(",", ".") +
				this.simulationData.residentialMediumLoadToString().replace(",", ".") +
				this.simulationData.residentialLowLoadToString().replace(",", ".") +
				"\r\n" + 
				"Table    PrevisaoGerSolar(ConjGerSolar, ConjPeriodos) previs�o de gera��o solar\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.powerSolarToString().replace(",", ".") + 
				"\r\n" + 
				"Table    PrevisaoGerEolica(ConjGerEolica, ConjPeriodos) previs�o de gera��o e�lica\r\n" + 
				this.simulationData.getTHeader() + 
				this.simulationData.powerWindToString().replace(",", ".") + 
				"\r\n" + 
				"Variable CustoTotal Custo total de opera��o;\r\n" + 
				"\r\n" + 
				"Positive Variable PotenciaGeradaEolica(ConjGerEolica, ConjPeriodos) Geracao eolica do gerador i no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaSolar(ConjGerSolar, ConjPeriodos) Geracao solar do gerador i no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaDiesel(ConjGerDiesel, ConjPeriodos) Geracao a diesel no periodo t;\r\n" + 
				"\r\n" + 
				"Variable perfilCargas(DL, ConjPeriodos) Previs�o de consumo das cargas para o per�odo t;\r\n" + 
				"\r\n" + 
				"Variable BateriaCarregamento(B, ConjPeriodos) Carregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaDescarregamento(B, ConjPeriodos) Descarregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaEnergia(B, ConjPeriodos) Energia armazenada na bateria no periodo t;\r\n" + 
				"Variable BateriaSoC(B, ConjPeriodos) Estado de carga (SoC) da bateria no periodo t;\r\n" + 
				"\r\n" + 
				"Variable ConsumoDiesel(ConjPeriodos) Consumo de combustivel do gerador a diesel no periodo t;\r\n" + 
				"*Variable ConsumoCargasPrioritarias(ConjPeriodos) Consumo das cargas prioritarias no periodo t;\r\n" + 
				"Variable ConsumoCargasDesp(ConjPeriodos) Consumo das cargas despachaveis no periodo t;\r\n" + 
				"\r\n" +
				"Variable CustoPorPeriodos(ConjPeriodos) Custo de opera��o da microrrede para cada per�odo t; \r\n" +
				"Variable CustoGerador(ConjPeriodos) Custo da gera��o � diesel por per�odo t; \r\n" +
				"\r\n" +
				"Variable PrevisaoRenovaveis(ConjPeriodos) previs�o da gera��o renov�vel para o per�odo t;\r\n" + 
				"Variable PotenciaGeradaExcessoRenovaveis(ConjPeriodos) previs�o de gera��o em excesso para renov�veis no per�odo t;\r\n" +
				"\r\n" +
				"Variable TotalPotenciaProduzida(ConjPeriodos) Total de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaExcesso(ConjPeriodos) Excesso de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) Potencia nao suprida das cargas despachaveis no periodo t;\r\n" + 
				"Variable PotenciaSupridaCargasDesp(ConjPeriodos) Potencia suprida para as cargas despachaveis no periodo t;\r\n" + 
				"\r\n" + 
				"*variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"sos2 variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"\r\n" + 
				"* VARI�VEIS BIN�RIAS\r\n" + 
				"Binary Variable StatusGerDiesel(G, ConjPeriodos) On-Off Status do gerador a diesel no per�odo t (0 desligado - 1 Ligado);\r\n" + 
				"Binary Variable StatusBateria(B, ConjPeriodos) Status of battery (0 descarregando - 1 carregando);\r\n" + 
				"Binary Variable StatusCargasDesp(DL, ConjPeriodos) On-Off Status das cargas despach�veis no per�odo t;\r\n" + 
				"*Binary Variable StatusCargas(L, ConjPeriodos) On-Off Status das cargas priorit�rias no per�odo t;\r\n" + 
				"Binary Variable StatusGerSolar(P, ConjPeriodos) On-Off Status do gerador fotovoltaico n no per�odo t;\r\n" + 
				"Binary Variable StatusGerEolica(W, ConjPeriodos) On-Off Status do gerador e�lico n no per�odo t;\r\n" +
				"Binary Variable GeradorLigou(G, ConjPeriodos) Indica se o gerador ligou no per�odo t; \r\n" +
				"Binary Variable GeradorDesligou(G, ConjPeriodos) Indica se o gerador desligou no per�odo t; \r\n" + 
				"\r\n" + 
				"* Vari�veis relacionadas ao c�lculo de energia consumida e gerada\r\n" + 
				"Variable EnergiaGeradaGerDiesel(ConjPeriodos) Total de energia gerada pelo gerador diesel no per�odo t;\r\n" + 
				"Variable EnergiaGeradaGerEolico(ConjPeriodos) Total de energia gerada pelo gerador eolico no per�odo t;\r\n" + 
				"Variable EnergiaGeradaGerFotovoltaico(ConjPeriodos) Total de energia gerada pelo gerador fotovoltaico no per�odo t;\r\n" + 
				"Variable EnergiaGeradaBateria(ConjPeriodos) Total de energia gerada pelo banco de baterias no per�odo t;\r\n" + 
				"Variable TotalEnergiaProduzida(ConjPeriodos) Total de energia produzida por todos os REDs no per�odo t;\r\n" + 
				"\r\n" + 
				"Equations\r\n" + 
				"*        Fun��o objetivo para o problema de minimiza��o do custo operacional no gerenciamento de uma microrrede\r\n" + 
				"         CustoFinal define the objetive function\r\n" + 
				"\r\n" +
				"         CalculaCustoParcial(ConjPeriodos) C�lculo do custo de opera��o para os per�odos\r\n" +
				"		  CalcularPrevisaoRenovaveis(ConjPeriodos) previsao de geracao e�lica para o per�odo t" +
				"\r\n" +
				"*        Balan�o de pot�ncia na microrrede\r\n" + 
				"         BalancoPotencia(ConjPeriodos) Balanco de potencia no periodo t\r\n" + 
				"\r\n" + 
				"*        Load_consume(ConjPeriodos) Consumo real das cargas priorit�rias para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas �s cargas despach�veis\r\n" + 
				"         RespostaDemanda(ConjPeriodos) Aplica��o da resposta da demanda para o per�odo t\r\n" + 
				"         ConsumoDemanda(ConjPeriodos) Consumo real das cargas despach�veis para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionada ao gerador diesel\r\n" +
				"		  IndicarOperacaoGeradorDiesel(G, ConjPeriodos) Indica��o se o gerador ligou ou desligou no periodo t \r\n" + 
				"         CalcularCustoGerador(ConjPeriodos) C�lculo do custo de gera��o � diesel no per�odo t \r\n" + 
				"         CalcularGeracaoDiesel(G, ConjPeriodos) Gera��o no per�odo t\r\n" + 
				"         CalcularConsumoGeradorDiesel(ConjPeriodos) Consumo de diesel para o gerador g no per�odo t \r\n" + 
				"         LimiteSuperiorGeracaoDiesel(G, ConjPeriodos) Limite m�ximo de gera��o para o gerador diesel \r\n" + 
				"         LimiteInferiorGeracaoDiesel(G, ConjPeriodos) Limite m�nimo de gera��o para o gerador diesel \r\n" + 
				"         LinearizarGeradorDiesel(G, ConjPeriodos) Restri��o da fun��o sos2 definindo um �nico intervalo linear para pi \r\n" + 
				"         LimiteRampaSubidaGeradorDiesel(G, ConjPeriodos) Restri��o de rampa de subida para gera��o a diesel \r\n" + 
				"         LimiteRampaDescidaGeradorDiesel(G, ConjPeriodos) Restri��o de rampa de descida para a gera��o a diesel \r\n" + 
				"\r\n" + 
				"         CalcularTotalPotenciaProduzida(ConjPeriodos) calculo do total gerado na microrrede no periodo t \r\n" +
				"         CalcularPGER(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t \r\n " +
				"         CalcularPGE(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t \r\n" + 
				"         CalcularPNS(DL, ConjPeriodos) C�lculo da energia n�o suprida para as cargas no per�odo t \r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador e�lico\r\n" + 
				"         LimiteGeracaoEolica(W, ConjPeriodos) Limite superior de gera��o da gera��o e�lica no periodo t\r\n" + 
				"         LimiteSuperiorGeracaoEolica(W, ConjPeriodos) gera��o e�lica m�xima para o per�odo t\r\n" + 
				"         LimiteInferiorGeracaoEolica(W, ConjPeriodos) gera��o e�lica m�nima para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador fotovoltaico\r\n" + 
				"         LimiteGeracaoSolar(P, ConjPeriodos) Limite superior de gera��o solar no per�odo t\r\n" + 
				"         LimiteSuperiorGeracaoSolar(P, ConjPeriodos) Gera��o solar m�xima para o per�odo t\r\n" + 
				"         LimiteInferiorGeracaoSolar(P, ConjPeriodos) Gera��o solar m�nima para o per�odo t\r\n" +
				"\r\n" + 
				"*        Restri��es relacionadas a bateria\r\n" + 
				"         BalancoPotenciaBateria(B, ConjPeriodos) balan�o de pot�ncia da bateria\r\n" + 
				"         CargaMinimaBateria(B, ConjPeriodos) valor m�nimo para armazenamento da bateria\r\n" + 
				"         CargaMaximaBateria(B, ConjPeriodos) valor m�ximo para armazenamento da bateria\r\n" + 
				"         LimiteSuperiorCarregamentoBateria(B, ConjPeriodos) carregamento da bateria i para o per�odo t\r\n" + 
				"         LimiteInferiorCarregamentoBateria(B, ConjPeriodos) limite inferior para carregamento da bateria\r\n" + 
				"         LimiteCarregamentoBateria(B, ConjPeriodos) limite de carregamento considerando energia armazenada na bateria\r\n" + 
				"         LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos) descarregamento m�ximo da bateria no per�odo t\r\n" + 
				"         LimiteInferiorDescarregamentoBateria(B, ConjPeriodos) descarregamento m�nimo da bateria no per�odo t\r\n" + 
//				"         LimiteDescarregamentoBateria(B, ConjPeriodos) limite de descarregamento considerando energia armazenada na bateria\r\n" + 
				"         CalcularSOCBateria(B, ConjPeriodos) SoC da bateria no per�odo t\r\n" + 
				"\r\n" + 
				"*        C�lculo da gera��o de energia pelas REDs\r\n" + 
				"         CalculaGeracaoDiesel(ConjPeriodos) calcula a geracao de energia a diesel no per�odo t\r\n" + 
				"         CalculaGeracaoEolica(ConjPeriodos) calcula a geracao de energia eolica no per�odo t\r\n" + 
				"         CalculaGeracaoFotovoltaica(ConjPeriodos) calcula a geracao de energia fotovoltaica no per�odo t\r\n" + 
				"         CalculaGeracaoBateria(ConjPeriodos) calcula a geracao de energia pela bateria no per�odo t\r\n" + 
				"         CalculaTotalEnergiaProduzida(ConjPeriodos) calcula o total de energia gerada na microrrede pelos DERs\r\n" +
				"         CalculaPrevisaoCargas(DL, ConjPeriodos) calcula a previs�o de consumo das cargas;\r\n" + 
				"\r\n" +  
				"CustoFinal..   CustoTotal =e= sum(ConjPeriodos, CustoGerador(ConjPeriodos) ) + sum((ConjPeriodos,ConjGerEolica), (CustoGerEolica * PotenciaGeradaEolica(ConjGerEolica,ConjPeriodos) * dt(ConjPeriodos))) + sum((ConjPeriodos,ConjGerSolar), (CustoGerSolar * PotenciaGeradaSolar(ConjGerSolar,ConjPeriodos) * dt(ConjPeriodos))) - sum((ConjPeriodos, ConjBaterias), BateriaDescarregamento(ConjBaterias, ConjPeriodos) * CustoBateria) + sum((ConjPeriodos, ConjBaterias), BateriaCarregamento(ConjBaterias, ConjPeriodos) * CustoBateria) + sum((DL, ConjPeriodos), PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL)) + sum(ConjPeriodos, PotenciaGeradaExcessoRenovaveis(ConjPeriodos) * 0.2);\r\n" + 
				"\r\n" +
				"CalculaCustoParcial(ConjPeriodos).. CustoPorPeriodos(ConjPeriodos) =e= CustoGerador(ConjPeriodos) + sum(ConjGerEolica, (CustoGerEolica * PotenciaGeradaEolica(ConjGerEolica,ConjPeriodos) * dt(ConjPeriodos))) + sum(ConjGerSolar, (CustoGerSolar * PotenciaGeradaSolar(ConjGerSolar,ConjPeriodos) * dt(ConjPeriodos))) - sum(ConjBaterias, BateriaDescarregamento(ConjBaterias, ConjPeriodos) * 0.01) + sum(ConjBaterias, BateriaCarregamento(ConjBaterias, ConjPeriodos) * 0.01) + sum(DL, PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL));" +
				"\r\n" +
				"BalancoPotencia(ConjPeriodos).. sum(G, PotenciaGeradaDiesel(G, ConjPeriodos)) + sum(W, PotenciaGeradaEolica(W, ConjPeriodos))+ sum(P, PotenciaGeradaSolar(P, ConjPeriodos)) + sum(B, BateriaDescarregamento(B, ConjPeriodos)) =e= sum(B, BateriaCarregamento(B, ConjPeriodos)) + PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"\r\n" +
				"IndicarOperacaoGeradorDiesel(G, ConjPeriodos).. GeradorLigou(G, ConjPeriodos) - GeradorDesligou(G, ConjPeriodos) =e= StatusGerDiesel(G, ConjPeriodos) - StatusGerDiesel(G, ConjPeriodos - 1) - EstadoInicialGerador;\r\n" + 
				"CalcularCustoGerador(ConjPeriodos).. CustoGerador(ConjPeriodos) =e= (ConsumoDiesel(ConjPeriodos) * CustoCombustivel * dt(ConjPeriodos)) + (sum(G, GeradorLigou(G, ConjPeriodos) * CustoOperacaoGeradorDiesel)) + (sum(G, GeradorDesligou(G, ConjPeriodos) * CustoOperacaoGeradorDiesel));\r\n" + 
				"LimiteSuperiorGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =l= ParGerDieselMaxima(G) * StatusGerDiesel(G, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =g= ParGerDieselMinima(G) * StatusGerDiesel(G, ConjPeriodos);\r\n" + 
				"CalcularGeracaoDiesel(G, ConjPeriodos).. PotenciaGeradaDiesel(G, ConjPeriodos) =e= sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao) * ParGerDieselConsumoLinearizado(G, ConjILinearizacao));\r\n" + 
				"CalcularConsumoGeradorDiesel(ConjPeriodos).. ConsumoDiesel(ConjPeriodos) =e= sum((G, ConjILinearizacao), lam(ConjPeriodos, ConjILinearizacao) * consumoger(G, ConjILinearizacao));\r\n" + 
				"LinearizarGeradorDiesel(G, ConjPeriodos).. sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao)) =e= 1;\r\n" + 
				"LimiteRampaSubidaGeradorDiesel(G, ConjPeriodos)$(ord(ConjPeriodos) > 1).. PotenciaGeradaDiesel(G, ConjPeriodos) - PotenciaGeradaDiesel(G, ConjPeriodos - 1) =l= GerDieselLimiteRampaSubida;\r\n" + 
				"LimiteRampaDescidaGeradorDiesel(G, ConjPeriodos)$(ord(ConjPeriodos) > 1).. PotenciaGeradaDiesel(G, ConjPeriodos) - PotenciaGeradaDiesel(G, ConjPeriodos - 1) =g= GerDieselLimiteRampaDescida;\r\n" + 
				"\r\n" + 
				"*Load_consume(ConjPeriodos).. ConsumoCargasPrioritarias(ConjPeriodos) =e= sum(L, PrevisaoCargas(L, ConjPeriodos) * (1 - StatusCargas(L, ConjPeriodos)));\r\n" + 
				"\r\n" + 
				"CalcularTotalPotenciaProduzida(ConjPeriodos).. TotalPotenciaProduzida(ConjPeriodos) =e=  sum(W, PotenciaGeradaEolica(W, ConjPeriodos)) + sum(P, PotenciaGeradaSolar(P, ConjPeriodos)) + sum(B, BateriaDescarregamento(B, ConjPeriodos)) + sum(G, PotenciaGeradaDiesel(G, ConjPeriodos));\r\n" +
				"CalcularPrevisaoRenovaveis(ConjPeriodos).. PrevisaoRenovaveis(ConjPeriodos) =e= sum(P, PrevisaoGerSolar(P, ConjPeriodos)) + sum(W, PrevisaoGerEolica(W, ConjPeriodos));\r\n" +
				"CalcularPGER(ConjPeriodos).. PotenciaGeradaExcessoRenovaveis(ConjPeriodos) =e= PrevisaoRenovaveis(ConjPeriodos) - (sum(W, PotenciaGeradaEolica(W, ConjPeriodos)) + sum(P, PotenciaGeradaSolar(P, ConjPeriodos)));" +
				"CalcularPGE(ConjPeriodos).. PotenciaGeradaExcesso(ConjPeriodos) =e= TotalPotenciaProduzida(ConjPeriodos) - PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"CalcularPNS(DL, ConjPeriodos).. PotenciaNaoSupridaCargasDesp(DL, ConjPeriodos) =e= (1 - StatusCargasDesp(DL, ConjPeriodos)) * PrevisaoCargasDesp(DL, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"* Restri��es relacionadas ao balan�o de pot�ncia\r\n" + 
				"RespostaDemanda(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"ConsumoDemanda(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"\r\n" + 
				"LimiteGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =l= ParGerEolicaMaxima(W);\r\n" + 
				"LimiteSuperiorGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =l= PrevisaoGerEolica(W, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoEolica(W, ConjPeriodos).. PotenciaGeradaEolica(W, ConjPeriodos) =g= 0;\r\n" + 
				"*WT_generation(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =e= PrevisaoGerEolica(W, ConjPeriodos) * StatusGerEolica(W, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"LimiteGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =l= ParGerFotovoltaicaMaxima(P);\r\n" + 
				"LimiteSuperiorGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =l= PrevisaoGerSolar(P, ConjPeriodos);\r\n" + 
				"LimiteInferiorGeracaoSolar(P, ConjPeriodos).. PotenciaGeradaSolar(P, ConjPeriodos) =g= 0;\r\n" + 
				"*PT_generation(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =e= PrevisaoGerSolar(P, ConjPeriodos) * StatusGerSolar(P, ConjPeriodos);\r\n" + 
				"\r\n" +
				"BalancoPotenciaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos - 1) + ((BateriaCarregamento(B, ConjPeriodos - 1) - BateriaDescarregamento(B, ConjPeriodos - 1)) * dt(ConjPeriodos)) + ParBateriaSOCInicial(B, ConjPeriodos);\r\n" + 
				"CargaMinimaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =g= ParBateriaMinimaEnergia(B);\r\n" + 
				"CargaMaximaBateria(B, ConjPeriodos).. BateriaEnergia(B, ConjPeriodos) =l= ParBateriaMaximaEnergia(B);\r\n" + 
				"LimiteSuperiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =l= ParBateriaLimiteCarregamento(B) * StatusBateria(B, ConjPeriodos);\r\n" + 
				"LimiteInferiorCarregamentoBateria(B, ConjPeriodos).. BateriaCarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
				"LimiteCarregamentoBateria(B, ConjPeriodos).. ParBateriaMaximaEnergia(B) =g= BateriaEnergia(B, ConjPeriodos - 1) + BateriaCarregamento(B, ConjPeriodos);\r\n" + 
				"LimiteSuperiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =l= ParBateriaLimiteDescarregamento(B) * (1 - StatusBateria(B, ConjPeriodos));\r\n" + 
				"LimiteInferiorDescarregamentoBateria(B, ConjPeriodos).. BateriaDescarregamento(B, ConjPeriodos) =g= 0;\r\n" + 
				"CalcularSOCBateria(B, ConjPeriodos).. BateriaSoC(B, ConjPeriodos) =e= BateriaEnergia(B, ConjPeriodos) / ParBateriaMaximaEnergia(B) * 100;\r\n" +  
				"\r\n" + 
				"CalculaGeracaoDiesel(ConjPeriodos).. EnergiaGeradaGerDiesel(ConjPeriodos) =e= sum(G, PotenciaGeradaDiesel(G, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoEolica(ConjPeriodos).. EnergiaGeradaGerEolico(ConjPeriodos) =e= sum(W, PotenciaGeradaEolica(W, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoFotovoltaica(ConjPeriodos).. EnergiaGeradaGerFotovoltaico(ConjPeriodos) =e= sum(P, PotenciaGeradaSolar(P, ConjPeriodos) * dt(ConjPeriodos));\r\n" + 
				"CalculaGeracaoBateria(ConjPeriodos).. EnergiaGeradaBateria(ConjPeriodos) =e= sum(B, BateriaDescarregamento(B, ConjPeriodos));\r\n" + 
				"CalculaTotalEnergiaProduzida(ConjPeriodos).. TotalEnergiaProduzida(ConjPeriodos) =e= EnergiaGeradaGerDiesel(ConjPeriodos) + EnergiaGeradaGerEolico(ConjPeriodos) + EnergiaGeradaGerFotovoltaico(ConjPeriodos) + EnergiaGeradaBateria(ConjPeriodos);\r\n" +
				"\r\n" +
				"CalculaPrevisaoCargas(DL, ConjPeriodos).. perfilCargas(DL, ConjPeriodos) =e= PrevisaoCargasDesp(DL, ConjPeriodos); \r\n" +
				"\r\n" + 
				"model microgrid / all /;\r\n" + 
				"\r\n" + 
				"solve microgrid using MIP minimizing CustoTotal;\r\n" + 
				"\r\n" + 
				"Display CustoTotal.l, CustoTotal.m";
		
		return this.model;
	}
	
	public String getData() {
		return this.data;
	}
	
	public String getModel() {
		return this.model;
	}
}
