package Model;

import java.util.ArrayList;
import java.util.Arrays;

public class WindTurbine {
	private Double ratedPower;
	private ArrayList<Double> generationCurve;
	
	public WindTurbine(Double ratedPower) {
		this.ratedPower = ratedPower;
		this.generationCurve = new ArrayList<Double>();
		this.generationCurve.add(0.0);
		this.generationCurve.add(0.0);
		this.generationCurve.add(0.015);
		this.generationCurve.add(0.042);
		this.generationCurve.add(0.091);
		this.generationCurve.add(0.167);
		this.generationCurve.add(0.279);
		this.generationCurve.add(0.418);
		this.generationCurve.add(0.594);
		this.generationCurve.add(0.758);
		this.generationCurve.add(0.758);
		this.generationCurve.add(0.887);
		this.generationCurve.add(0.97);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
		this.generationCurve.add(1.015);
	}

	public Double getRatedPower() {
		return ratedPower;
	}

	public void setRatedPower(Double ratedPower) {
		this.ratedPower = ratedPower;
	}
	
	public Double getGeneration(Integer windSpeed) {
		if (windSpeed > 0)
			return (this.generationCurve.get(windSpeed - 1) * this.ratedPower);
		else
			return 0.0;
	}

	public Double getEqualGeneration(Integer windSpeed) {
		return (this.generationCurve.get(windSpeed - 1) * this.ratedPower);
	}
	
}
