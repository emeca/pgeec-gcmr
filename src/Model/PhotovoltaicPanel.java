package Model;

public class PhotovoltaicPanel {
	private Double Smax;
	private Double ratedPower;
	
	public PhotovoltaicPanel(Double ratedPower, Double maxIrradiation) {
		this.ratedPower = ratedPower;
		// 941 was the maximum irradiation found on datasets
		this.Smax = 941.0;
	}
	
	public Double getGeneration(Double irradiation) {
		return (irradiation / this.Smax) * this.ratedPower;
	}

	public Double getEqualGeneration(Double irradiation) {
		return (irradiation / this.Smax) * this.ratedPower;
	}

}
