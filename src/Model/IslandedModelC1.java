package Model;

public class IslandedModelC1 {
	private String data;
	private String model;
	
	public IslandedModelC1() {
		this.data = 
				"Sets																										\n" + 
				"         ConjGerDiesel   conjunto dos geradores a diesel                 / gen1 /							\n" + 
				"         ConjPeriodos    conjunto dos per�odos da simula��o              / t1*t8 /							\n" + 
				"         ConjGerSolar    conjunto dos geradores fotovoltaicos            / photo1 /						\n" + 
				"         ConjGerEolica   conjunto dos geradores eolicos                  / wind1 /							\n" + 
				"         ConjCargasPrioritarias  conjunto das cargas prioritarias        / load1 /							\n" + 
				"         ConjCargasDespachaveis  conjunto das cargas despachaveis        / dloadA1, dloadA2, dloadB1, dloadB2, dloadC1, dloadC2, dloadD1, dloadD2 /\r\n" + 
				"         ConjBaterias    conjunto das baterias                           / bat1 /							\n" + 
				"         ConjILinearizacao conjunto com valores para linearizacao do consumo do gerador a diese  / 1*6 /;	\n" + 
//				"\r\n" + 
//				"* Definicao dos custos de fornecimento dos Recursos Energicamente Distribuidos\r\n" + 
//				"Scalar CustoGerEolica custo de gera��o por kW  para energia e�lica / 0.01 /;\r\n" + 
//				"Scalar CustoGerSolar valor de gera��o por kW  para energia solar / 0.01 /;\r\n" + 
//				"Scalar CustoBateria valor para utiliza��o da bateria / 0.001 /;\r\n" + 
//				"Scalar CustoCombustivel custo de combustivel (R$ por litro) /4.1/;\r\n" + 
//				"\r\n" + 
//				"Scalar BatteryInicitalSOC valor inicialpara estado de carga da bateria em kW  / 6 /;\r\n" + 
//				"\r\n" + 
//				"* Definicao de constantes para a geracao a diesel\r\n" + 
//				"Scalar GerDieselConstA parametro A do calculo de linearizacao /0.004446/;\r\n" + 
//				"Scalar GerDieselConstB parametro B do calculo de linearizacao /0.121035/;\r\n" + 
//				"Scalar GerDieselConstC parametro C do calculo de linearizacao /1.653882/;\r\n" + 
//				"Scalar GerDieselLimiteRampaSubida limite de subida de geracao a diesel / 5 /;\r\n" + 
//				"Scalar GerDieselLimiteRampaDescida limite de descida de geracao a diesel / -5 /;\r\n" + 
//				"\r\n" + 
//				"* Par�metros para configura��o de limites operacionais dos elementos da microrrede\r\n" + 
//				"Parameters\r\n" + 
//				"         ParGerDieselMaxima(ConjGerDiesel) limites maximo de operacao dos geradores a dieselem kW\r\n" + 
//				"         / gen1 20 /\r\n" + 
//				"*           gen2 500\r\n" + 
//				"*           gen3 500\r\n" + 
//				"*           gen4 500 /\r\n" + 
//				"\r\n" + 
//				"         ParGerDieselMinima(ConjGerDiesel) limites minimo de operacao dos geradores a dieselem kW\r\n" + 
//				"         / gen1 4 /\r\n" + 
//				"*           gen2 130\r\n" + 
//				"*           gen3 130\r\n" + 
//				"*           gen4 130 /\r\n" + 
//				"\r\n" + 
//				"         ParGerFotovoltaicaMaxima(ConjGerSolar) limites m�ximo de gera��o fotovoltaica em kW\r\n" + 
//				"         / photo1 10 /\r\n" + 
//				"\r\n" + 
//				"         ParGerEolicaMaxima(ConjGerEolica) limites m�ximo da gera��o eolica em kW\r\n" + 
//				"         / wind1 10 /\r\n" + 
//				"\r\n" + 
//				"         ParBateriaLimiteCarregamento limite de carregamento da bateria em kW\r\n" + 
//				"         / 10 /\r\n" + 
//				"\r\n" + 
//				"         ParBateriaLimiteDescarregamento limite d edescarregamento da bateria em kW\r\n" + 
//				"         / 10 /\r\n" + 
//				"*           bat2 -100 /\r\n" + 
//				"\r\n" + 
//				"         ParBateriaMinimaEnergia limite minimo de armazenamento da bateria SOC\r\n" + 
//				"         / 2 /\r\n" + 
//				"*          bat2 60 /\r\n" + 
//				"\r\n" + 
//				"         ParBateriaMaximaEnergia limite maximo de armazenamento da bateria SOC\r\n" + 
//				"         / 10 /\r\n" + 
//				"*           bat2 400 /\r\n" + 
//				"\r\n" + 
//				"         ParBateriaSOCInicial(ConjPeriodos) valor do SOC da bateria para o periodo inicial da simula��o\r\n" + 
//				"         / t1 6 /\r\n" + 
//				"*           bat2.t1 160 /\r\n" + 
//				"\r\n" + 
//				"         ParCargasDespachaveisPenalidade(ConjCargasDespachaveis) custo de penalizacao das cargas despach�veis\r\n" + 
//				"         / dloadA1 30\r\n" + 
//				"           dloadA2 30\r\n" + 
//				"           dloadB1 20\r\n" + 
//				"           dloadB2 20\r\n" + 
//				"           dloadC1 10\r\n" + 
//				"           dloadC2 10\r\n" + 
//				"           dloadD1 10\r\n" + 
//				"           dloadD2 10 /\r\n" + 
//				"\r\n" + 
//				"\r\n" + 
//				"Parameter ParGerDieselConsumoLinearizado(ConjILinearizacao) valores linearizados usados para a curva de consumo do gerador\r\n" + 
//				"         /\r\n" + 
//				"           1        0.0\r\n" + 
//				"           2        4.0\r\n" + 
//				"           3        7.0\r\n" + 
//				"           4        11.5\r\n" + 
//				"           5        15.0\r\n" + 
//				"           6        20.0 /;\r\n" + 
//				"\r\n" + 
//				"Parameter consumoger(ConjILinearizacao) curva de consumo do gerador interpolada /1 0./;\r\n" + 
//				"         consumoger(ConjILinearizacao)$(ord(ConjILinearizacao) > 1) = (GerDieselConstA * ParGerDieselConsumoLinearizado(ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjILinearizacao) + GerDieselConstB * ParGerDieselConsumoLinearizado(ConjILinearizacao) + GerDieselConstC);\r\n" + 
//				"\r\n" + 
//				"*DISCRETIZACAO EM 3 NIVEIS: na primeira meia hora otimiza de 5 em 5 min (dt=.083\r\n" + 
//				"* horas), na segunda meia hora discretiza em .5 horas,\r\n" + 
//				"* e no restante das 23 horas do dia, discretiza de\r\n" + 
//				"* hora em hora (dt = 1 hora).\r\n" + 
//				"set t enumeracao com os periodos de tempo dt\r\n" + 
//				"         / 1*8 /;\r\n" + 
//				"\r\n" + 
//				"Parameter dt(conjPeriodos)\r\n" + 
//				"         / t1*t6 .083333\r\n" + 
//				"           t7 .5\r\n" + 
//				"           t8 .25 /;\r\n" + 
//				"\r\n" + 
//				"*** Todas as cargas s�o consideradas despach�veis neste cen�rio, a prioriza��o se dar� pela valor de\r\n" + 
//				"*** penalidade para as cargas\r\n" + 
//				"*Table    PrevisaoCargas(ConjCargasPrioritarias, ConjPeriodos) previs�o de consumo das cargas priorit�rias\r\n" + 
//				"*                  t1      t2      t3      t4      t5      t6      t7      t8      t9      t10     t11     t12     t13     t14     t15     t16     t17     t18     t19     t20     t21     t22     t23     t24\r\n" + 
//				"*          load1  51      54      41      55      54      57      53      48      51      52      55      59      57      61      60      88      113     164     199      51      50      48      46      45;\r\n" + 
//				"*          load1   51      54      41      55      54      57      53      48      51      52      55      59      57      61      60      58      53      54      49      51      50      48      46      45;\r\n" + 
//				"*           load1   1       4      1       5      54      57      53      48      51      52      55      59      57      61      60      58      53      54      49      51      50      48      46      45;\r\n" + 
//				"\r\n" + 
//				"\r\n" + 
//				"Table    PrevisaoCargasDesp(ConjCargasDespachaveis, ConjPeriodos) previs�o de consumo das cargas despach�veis\r\n" + 
//				"*                 t1       t2       t3       t4       t5       t6       t7       t8       t9       t10      t11      t12      t13      t14      t15        t16        t17        t18        t19        t20        t21        t22        t23        t24\r\n" + 
//				"*         dloadA1 3        3        3        3        3        5        9        12       13       13       10       9        10       11       10         10         10         7          5          4          4          4          3          3\r\n" + 
//				"*         dloadA2 3        3        3        3        3        4        8        10       11       11       9        8        9        9        8          8          8          6          5          4          4          3          3          3\r\n" + 
//				"*         dloadB1 2        3        3        3        3        3        4        5        5        5        6        5        5        5        5          5          5          5          5          4          4          4          3          3\r\n" + 
//				"*         dloadB2 2        3        3        3        3        3        3        4        4        4        4        4        4        4        4          4          4          4          4          4          4          3          3          3\r\n" + 
//				"*         dloadC1 1        1        1        1        2        2        2        2        2        2        2        2        2        2        2          4          4          4          4          3          3          2          2          2\r\n" + 
//				"*         dloadC2 1        1        1        1        2        2        2        2        2        2        2        2        2        2        2          4          4          4          4          3          3          2          2          2\r\n" + 
//				"*         dloadD1 1        1        1        1        1        1        1        1        1        1        1        1        1        1        1          2          2          2          2          2          1          1          1          1\r\n" + 
//				"*         dloadD2 1        1        1        1        1        1        1        1        1        1        1        1        1        1        1          2          2          2          2          2          1          1          1          1;\r\n" + 
//				"\r\n" + 
//				"* Considerando o caso de interrup��o �s 10:00 AM\r\n" + 
//				"                 t1       t2       t3       t4       t5       t6       t7       t8\r\n" + 
//				"         dloadA1 13       13       13       13       13       13       13       10\r\n" + 
//				"         dloadA2 11       11       11       11       11       11       11       9\r\n" + 
//				"         dloadB1 5        5        5        5        5        5        5        6\r\n" + 
//				"         dloadB2 4        4        4        4        4        4        4        4\r\n" + 
//				"         dloadC1 2        2        2        2        2        2        2        2\r\n" + 
//				"         dloadC2 1        2        2        2        2        2        2        2\r\n" + 
//				"         dloadD1 1        1        1        1        1        1        1        1\r\n" + 
//				"         dloadD2 1        1        1        1        1        1        1        1\r\n" + 
//				"\r\n" + 
//				"Table    PrevisaoGerSolar(ConjGerSolar, ConjPeriodos) previs�o de gera��o solar\r\n" + 
//				"                 t1      t2      t3      t4      t5      t6      t7      t8\r\n" + 
//				"*         photo1  0       0       0       0       0       0       3       4       7      8     8      9      10     8      7      6      4      4       2       0       0       0       0       0;\r\n" + 
//				"         photo1  8       8       8       8       8       8       8       9\r\n" + 
//				"\r\n" + 
//				"Table    PrevisaoGerEolica(ConjGerEolica, ConjPeriodos) previs�o de gera��o e�lica\r\n" + 
//				"                 t1     t2     t3     t4     t5     t6     t7     t8\r\n" + 
//				"*         wind1   2      2      2      3      3      5      8      9      9      10      9      8      9      9       10     10     10     10     10      6      4      3       3      3;\r\n" + 
//				"         wind1   10      10     10    10     10     10     10     9";
				"";
		this.model = 
				"* Modelo matem�tico para otimiza��o do gerenciamento de uma microrrede de energia\r\n" + 
				"* el�trica em modo ilhado, com foco na minimizacao dos custos operacionais. Suporta\r\n" + 
				"* corte de cargas n�o prioritarias, chamadas neste caso de cargas despachaveis.\r\n" + 
				"\r\n" + 
				"* Autor: Andre da Silva Barbosa\r\n" + 
				"* Universidade Estadual do Oeste do Paran� - Campus Foz do Igua�u\r\n" + 
				"* Programa de P�s Gradua��o em Engenharia El�trica e Computa��o\r\n" + 
				"* Orientador: Adriano Batista de Almeida\r\n" + 
				"\r\n" + 
				"* Definicao dos conjuntos utilizados no modelo\r\n" + 
				"Sets\r\n" + 
				"         ConjGerDiesel   conjunto dos geradores a diesel                 / gen1 /\r\n" + 
				"         ConjPeriodos    conjunto dos per�odos da simula��o              / t1*t24 /\r\n" + 
				"         ConjGerSolar    conjunto dos geradores fotovoltaicos            / photo1 /\r\n" + 
				"         ConjGerEolica   conjunto dos geradores eolicos                  / wind1 /\r\n" + 
				"         ConjCargasPrioritarias  conjunto das cargas prioritarias        / load1 /\r\n" + 
				"         ConjCargasDespachaveis  conjunto das cargas despachaveis        / dloadA1, dloadA2, dloadB1, dloadB2, dloadC1, dloadC2, dloadD1, dloadD2 /\r\n" + 
				"         ConjBaterias    conjunto das baterias                           / bat1 /\r\n" + 
				"         ConjILinearizacao conjunto com valores para linearizacao do consumo do gerador a diese  / 1*6 /;\r\n" + 
				"\r\n" + 
				"* Defini��o dos alias (sinonimos) para os conjuntos criados, com o objetivo de simplificar\r\n" + 
				"* a chamada dos conjuntos no modelo matem�tico\r\n" + 
				"Alias(ConjGerDiesel, G);\r\n" + 
				"Alias(ConjGerEolica, W);\r\n" + 
				"Alias(ConjCargasPrioritarias, L);\r\n" + 
				"Alias(ConjBaterias, B);\r\n" + 
				"Alias(ConjGerSolar, P);\r\n" + 
				"Alias(ConjCargasDespachaveis, DL);\r\n" + 
				"\r\n" + 
				"* Definicao dos custos de fornecimento dos Recursos Energicamente Distribuidos\r\n" + 
				"Scalar CustoGerEolica custo de gera��o por kW  para energia e�lica / 0.01 /;\r\n" + 
				"Scalar CustoGerSolar valor de gera��o por kW  para energia solar / 0.01 /;\r\n" + 
				"Scalar CustoBateria valor para utiliza��o da bateria / 0.001 /;\r\n" + 
				"Scalar CustoCombustivel custo de combustivel (R$ por litro) /4.1/;\r\n" + 
				"\r\n" + 
				"Scalar BatteryInicitalSOC valor inicialpara estado de carga da bateria em kW  / 6 /;\r\n" + 
				"\r\n" + 
				"* Definicao de constantes para a geracao a diesel\r\n" + 
				"Scalar GerDieselConstA parametro A do calculo de linearizacao /0.004446/;\r\n" + 
				"Scalar GerDieselConstB parametro B do calculo de linearizacao /0.121035/;\r\n" + 
				"Scalar GerDieselConstC parametro C do calculo de linearizacao /1.653882/;\r\n" + 
				"Scalar GerDieselLimiteRampaSubida limite de subida de geracao a diesel / 5 /;\r\n" + 
				"Scalar GerDieselLimiteRampaDescida limite de descida de geracao a diesel / -5 /;\r\n" + 
				"\r\n" + 
				"* Par�metros para configura��o de limites operacionais dos elementos da microrrede\r\n" + 
				"Parameters\r\n" + 
				"         ParGerDieselMaxima(ConjGerDiesel) limites maximo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 20 /\r\n" + 
				"*           gen2 500\r\n" + 
				"*           gen3 500\r\n" + 
				"*           gen4 500 /\r\n" + 
				"\r\n" + 
				"         ParGerDieselMinima(ConjGerDiesel) limites minimo de operacao dos geradores a dieselem kW\r\n" + 
				"         / gen1 4 /\r\n" + 
				"*           gen2 130\r\n" + 
				"*           gen3 130\r\n" + 
				"*           gen4 130 /\r\n" + 
				"\r\n" + 
				"         ParGerFotovoltaicaMaxima(ConjGerSolar) limites m�ximo de gera��o fotovoltaica em kW\r\n" + 
				"         / photo1 10 /\r\n" + 
				"\r\n" + 
				"         ParGerEolicaMaxima(ConjGerEolica) limites m�ximo da gera��o eolica em kW\r\n" + 
				"         / wind1 10 /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteCarregamento limite de carregamento da bateria em kW\r\n" + 
				"         / 10 /\r\n" + 
				"\r\n" + 
				"         ParBateriaLimiteDescarregamento limite d edescarregamento da bateria em kW\r\n" + 
				"         / 10 /\r\n" + 
				"*           bat2 -100 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMinimaEnergia limite minimo de armazenamento da bateria SOC\r\n" + 
				"         / 2 /\r\n" + 
				"*          bat2 60 /\r\n" + 
				"\r\n" + 
				"         ParBateriaMaximaEnergia limite maximo de armazenamento da bateria SOC\r\n" + 
				"         / 10 /\r\n" + 
				"*           bat2 400 /\r\n" + 
				"\r\n" + 
				"         ParBateriaSOCInicial(ConjPeriodos) valor do SOC da bateria para o periodo inicial da simula��o\r\n" + 
				"         / t1 6 /\r\n" + 
				"*           bat2.t1 160 /\r\n" + 
				"\r\n" + 
				"         ParCargasDespachaveisPenalidade(ConjCargasDespachaveis) custo de penalizacao das cargas despach�veis\r\n" + 
				"         / dloadA1 30\r\n" + 
				"           dloadA2 30\r\n" + 
				"           dloadB1 20\r\n" + 
				"           dloadB2 20\r\n" + 
				"           dloadC1 10\r\n" + 
				"           dloadC2 10\r\n" + 
				"           dloadD1 10\r\n" + 
				"           dloadD2 10 /\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Parameter ParGerDieselConsumoLinearizado(ConjILinearizacao) valores linearizados usados para a curva de consumo do gerador\r\n" + 
				"         /\r\n" + 
				"           1        0.0\r\n" + 
				"           2        4.0\r\n" + 
				"           3        7.0\r\n" + 
				"           4        11.5\r\n" + 
				"           5        15.0\r\n" + 
				"           6        20.0 /;\r\n" + 
				"\r\n" + 
				"Parameter consumoger(ConjILinearizacao) curva de consumo do gerador interpolada /1 0./;\r\n" + 
				"         consumoger(ConjILinearizacao)$(ord(ConjILinearizacao) > 1) = (GerDieselConstA * ParGerDieselConsumoLinearizado(ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjILinearizacao) + GerDieselConstB * ParGerDieselConsumoLinearizado(ConjILinearizacao) + GerDieselConstC);\r\n" + 
				"\r\n" + 
				"*** Todas as cargas s�o consideradas despach�veis neste cen�rio, a prioriza��o se dar� pela valor de\r\n" + 
				"*** penalidade para as cargas\r\n" + 
				"*Table    PrevisaoCargas(ConjCargasPrioritarias, ConjPeriodos) previs�o de consumo das cargas priorit�rias\r\n" + 
				"*                  t1      t2      t3      t4      t5      t6      t7      t8      t9      t10     t11     t12     t13     t14     t15     t16     t17     t18     t19     t20     t21     t22     t23     t24\r\n" + 
				"*          load1  51      54      41      55      54      57      53      48      51      52      55      59      57      61      60      88      113     164     199      51      50      48      46      45;\r\n" + 
				"*          load1   51      54      41      55      54      57      53      48      51      52      55      59      57      61      60      58      53      54      49      51      50      48      46      45;\r\n" + 
				"*           load1   1       4      1       5      54      57      53      48      51      52      55      59      57      61      60      58      53      54      49      51      50      48      46      45;\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"Table    PrevisaoCargasDesp(ConjCargasDespachaveis, ConjPeriodos) previs�o de consumo das cargas despach�veis\r\n" + 
				"*                   t1    t2      t3      t4      t5      t6      t7      t8      t9      t10     t11     t12     t13     t14     t15     t16     t17     t18     t19     t20     t21     t22     t23     t24\r\n" + 
				"*         dloadA1   1     1       2       2       2       3       3       3       4       4       6       6       6       6       6       8       8       8       6       6       4       4       2       2\r\n" + 
				"*         dloadA2   1     1       2       2       2       3       3       3       4       4       4       5       5       6       6       7       7       7       7       6       5       4       2       2\r\n" + 
				"*         dloadB1   0     0       1       2       2       2       2       4       4       4       4       4       4       4       5       5       5       5       5       4       4       3       3       2\r\n" + 
				"*         dloadB2   0     0       1       1       1       2       2       2       3       4       4       4       4       5       5       5       5       5       2       2       1       1       1       1\r\n" + 
				"*         dloadC1   1     1       1       1       1       1       1       3       3       3       3       3       3       2       2       2       2       3       4       5       5       6       5       3\r\n" + 
				"*         dloadC2   1     1       2       2       2       2       3       3       3       3       4       4       4       5       5       5       5       5       6       7       7       4       3       2\r\n" + 
				"*         dloadD1   0     0       0       1       1       1       1       2       2       2       3       3       3       3       3       4       4       4       4       3       3       3       2       2\r\n" + 
				"*         dloadD2   0     0       0       1       1       2       2       2       3       3       4       4       4       5       5       5       4       4       3       3       3       2       2       2\r\n" + 
				"                 t1       t2       t3       t4       t5       t6       t7       t8       t9       t10      t11      t12      t13      t14      t15        t16        t17        t18        t19        t20        t21        t22        t23        t24\r\n" + 
				"         dloadA1 3        3        3        3        3        5        9        12       13       13       10       9        10       11       10         10         10         7          5          4          4          4          3          3\r\n" + 
				"         dloadA2 3        3        3        3        3        4        8        10       11       11       9        8        9        9        8          8          8          6          5          4          4          3          3          3\r\n" + 
				"         dloadB1 2        3        3        3        3        3        4        5        5        5        6        5        5        5        5          5          5          5          5          4          4          4          3          3\r\n" + 
				"         dloadB2 2        3        3        3        3        3        3        4        4        4        4        4        4        4        4          4          4          4          4          4          4          3          3          3\r\n" + 
				"         dloadC1 1        1        1        1        2        2        2        2        2        2        2        2        2        2        2          4          4          4          4          3          3          2          2          2\r\n" + 
				"         dloadC2 1        1        1        1        2        2        2        2        2        2        2        2        2        2        2          4          4          4          4          3          3          2          2          2\r\n" + 
				"         dloadD1 1        1        1        1        1        1        1        1        1        1        1        1        1        1        1          2          2          2          2          2          1          1          1          1\r\n" + 
				"         dloadD2 1        1        1        1        1        1        1        1        1        1        1        1        1        1        1          2          2          2          2          2          1          1          1          1;\r\n" + 
				"\r\n" + 
				"Table    PrevisaoGerSolar(ConjGerSolar, ConjPeriodos) previs�o de gera��o solar\r\n" + 
				"                 t1      t2      t3      t4      t5      t6      t7      t8      t9     t10   t11    t12    t13    t14    t15    t16    t17    t18     t19     t20     t21     t22     t23     t24\r\n" + 
				"*         photo1  0       0       0       0       0       0       40      220     420     590     680     700     600     690     570     320     100     0       0       0       0       0       0       0;\r\n" + 
				"         photo1  0       0       0       0       0       0       3       4       7      8     8      9      10     8      7      6      4      4       2       0       0       0       0       0;\r\n" + 
				"\r\n" + 
				"Table    PrevisaoGerEolica(ConjGerEolica, ConjPeriodos) previs�o de gera��o e�lica\r\n" + 
				"                 t1     t2     t3     t4     t5     t6     t7     t8     t9     t10    t11    t12    t13    t14     t15    t16    t17    t18    t19     t20    t21    t22     t23    t24\r\n" + 
				"*         wind1   130     170     130     160     200     210     130     110     90      80      90      120     100     130     120     110     100     90      80      120     100     90      100     110;\r\n" + 
				"         wind1   2      2      2      3      3      5      8      9      9      10      9      8      9      9       10     10     10     10     10      6      4      3       3      3;\r\n" + 
				"\r\n" + 
				"Variable CustoTotal Custo total de opera��o;\r\n" + 
				"\r\n" + 
				"Positive Variable EnergiaGeradaEolica(ConjGerEolica, ConjPeriodos) Geracao eolica do gerador i no periodo t;\r\n" + 
				"Positive Variable EnergiaGeradaSolar(ConjGerSolar, ConjPeriodos) Geracao solar do gerador i no periodo t;\r\n" + 
				"Positive Variable EnergiaGeradaDiesel(ConjPeriodos) Geracao a diesel no periodo t;\r\n" + 
				"\r\n" + 
				"Variable BateriaCarregamento(ConjPeriodos) Carregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaDescarregamento(ConjPeriodos) Descarregamento da bateria no periodo t;\r\n" + 
				"Variable BateriaEnergia(ConjPeriodos) Energia armazenada na bateria no periodo t;\r\n" + 
				"Variable BateriaSoC(ConjPeriodos) Estado de carga (SoC) da bateria no periodo t;\r\n" + 
				"\r\n" + 
				"Variable ConsumoDiesel(ConjPeriodos) Consumo de combustivel do gerador a diesel no periodo t;\r\n" + 
				"*Variable ConsumoCargasPrioritarias(ConjPeriodos) Consumo das cargas prioritarias no periodo t;\r\n" + 
				"Variable ConsumoCargasDesp(ConjPeriodos) Consumo das cargas despachaveis no periodo t;\r\n" + 
				"\r\n" + 
				"Variable TotalPotenciaProduzida(ConjPeriodos) Total de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaGeradaExcesso(ConjPeriodos) Excesso de potencia produzida no periodo t;\r\n" + 
				"Positive Variable PotenciaNaoSupridaCargas(ConjPeriodos) Potencia nao suprida das cargas prioritarias no periodo t;\r\n" + 
				"Positive Variable PotenciaNaoSupridaCargasDesp(ConjPeriodos) Potencia nao suprida das cargas despachaveis no periodo t;\r\n" + 
				"Variable PotenciaSupridaCargasDesp(ConjPeriodos) Potencia suprida para as cargas despachaveis no periodo t;\r\n" + 
				"\r\n" + 
				"variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"sos2 variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
				"\r\n" + 
				"* VARI�VEIS BIN�RIAS\r\n" + 
				"Binary Variable StatusGerDiesel(ConjPeriodos) On-Off Status do gerador a diesel no per�odo t (0 desligado - 1 Ligado);\r\n" + 
				"Binary Variable StatusBateria(ConjPeriodos) Status of battery (0 descarregando - 1 carregando);\r\n" + 
				"Binary Variable StatusCargasDesp(DL, ConjPeriodos) On-Off Status das cargas despach�veis no per�odo t;\r\n" + 
				"*Binary Variable StatusCargas(L, ConjPeriodos) On-Off Status das cargas priorit�rias no per�odo t;\r\n" + 
				"Binary Variable StatusGerSolar(P, ConjPeriodos) On-Off Status do gerador fotovoltaico n no per�odo t;\r\n" + 
				"Binary Variable StatusGerEolica(W, ConjPeriodos) On-Off Status do gerador e�lico n no per�odo t;\r\n" +
				"Positive Variable EnergiaGeradaDiesel;\n" +
				"																									\n" + 
				"Equations\r\n" + 
				"*        Fun��o objetivo para o problema de minimiza��o do custo operacional no gerenciamento de uma microrrede\r\n" + 
				"         Ocost define the objetive function\r\n" + 
				"\r\n" + 
				"*        Balan�o de pot�ncia na microrrede\r\n" + 
				"         power_balance(ConjPeriodos) Balanco de potencia no periodo t\r\n" + 
				"\r\n" + 
				"*        Load_consume(ConjPeriodos) Consumo real das cargas priorit�rias para o per�odo t\r\n" + 
				"\r\n" + 
				"         EGP_Calc(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t\r\n" + 
				"*        PUp_Calc(ConjPeriodos) C�lculo da energia n�o suprida para o per�odo t\r\n" + 
				"         PLUp_Calc(ConjPeriodos) C�lculo da energia n�o suprida para as cargas priorit�rias no per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas �s cargas despach�veis\r\n" + 
				"         DemandResponse(ConjPeriodos) Aplica��o da resposta da demanda para o per�odo t\r\n" + 
				"         DR_Consume(ConjPeriodos) Consumo real das cargas despach�veis para o per�odo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionada ao gerador diesel\r\n" + 
				"         DG_generation(ConjPeriodos) Gera��o no per�odo t\r\n" + 
				"         GerDieselConstConsume(ConjPeriodos) Consumo de diesel para o gerador g no per�odo t\r\n" + 
				"         DG_generation_limit_up(ConjPeriodos) Limite m�ximo de gera��o para o gerador diesel\r\n" + 
				"         DG_generation_limit_down(ConjPeriodos) Limite m�nimo de gera��o para o gerador diesel\r\n" + 
				"         DG_Linear(ConjPeriodos) Restri��o da fun��o sos2 definindo um �nico intervalo linear para pi\r\n" + 
				"         DG_Ramp_Up(ConjPeriodos) Restri��o de rampa de subida para gera��o a diesel\r\n" + 
				"         DG_Ramp_Down(ConjPeriodos) Restri��o de rampa de descida para a gera��o a diesel\r\n" + 
				"\r\n" + 
				"         Calcule_TotalPotenciaProduzida(ConjPeriodos) calculo do total gerado na microrrede no periodo t\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador e�lico\r\n" + 
				"         WT_upper_limit(W, ConjPeriodos) Limite superior de gera��o da gera��o e�lica no periodo t\r\n" + 
				"         WT_generation_limit(W, ConjPeriodos) gera��o e�lica prevista para o per�odo t\r\n" + 
				"         WT_generation(W, ConjPeriodos)\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas ao gerador fotovoltaico\r\n" + 
				"         PT_upper_limit(P, ConjPeriodos) Limite superior de gera��o solar no per�odo t\r\n" + 
				"         PT_generation_limit(P, ConjPeriodos) Gera��o solar prevista para o per�odo t\r\n" + 
				"         PT_generation(P, ConjPeriodos)\r\n" + 
				"\r\n" + 
				"*        Restri��es relacionadas a bateria\r\n" + 
				"         Battery_power_balance(ConjPeriodos) balan�o de pot�ncia da bateria\r\n" + 
				"         Battery_minimum_energy(ConjPeriodos) valor m�nimo para armazenamento da bateria\r\n" + 
				"         Battery_maximum_energy(ConjPeriodos) valor m�ximo para armazenamento da bateria\r\n" + 
				"         Battery_charge_limit_up(ConjPeriodos) carregamento da bateria i para o per�odo t\r\n" + 
				"         Battery_charge_limit_down(ConjPeriodos) limite inferior para carregamento da bateria\r\n" + 
				"         Battery_charge_limit_energy(ConjPeriodos) limite de carregamento considerando energia armazenada na bateria\r\n" + 
				"         Battery_discharge_limit_up(ConjPeriodos) descarregamento m�ximo da bateria no per�odo t\r\n" + 
				"         Battery_discharge_limit_down(ConjPeriodos) descarregamento m�nimo da bateria no per�odo t\r\n" + 
				"         Battery_discharge_limit_energy(ConjPeriodos) limite de descarregamento considerando energia armazenada na bateria\r\n" + 
				"         Battery_soc(ConjPeriodos) SoC da bateria no per�odo t;\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"*Ocost..   CustoTotal =e= sum(ConjPeriodos, ConsumoDiesel(ConjPeriodos) * CustoCombustivel ) + sum((ConjPeriodos,ConjGerEolica), (CustoGerEolica * EnergiaGeradaEolica(ConjGerEolica,ConjPeriodos))) + sum((ConjPeriodos,ConjGerSolar), (CustoGerSolar * EnergiaGeradaSolar(ConjGerSolar,ConjPeriodos))) + sum(ConjPeriodos, BateriaDescarregamento(ConjPeriodos) * 0.01) - sum(ConjPeriodos, BateriaCarregamento(ConjPeriodos) * 0.01) - sum((DL, ConjPeriodos), StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL)) + sum(ConjPeriodos, PotenciaNaoSupridaCargasDesp(ConjPeriodos) * 100);\r\n" + 
				"Ocost..   CustoTotal =e= sum(ConjPeriodos, ConsumoDiesel(ConjPeriodos) * CustoCombustivel ) + sum((ConjPeriodos,ConjGerEolica), (CustoGerEolica * EnergiaGeradaEolica(ConjGerEolica,ConjPeriodos))) + sum((ConjPeriodos,ConjGerSolar), (CustoGerSolar * EnergiaGeradaSolar(ConjGerSolar,ConjPeriodos))) + sum(ConjPeriodos, BateriaDescarregamento(ConjPeriodos) * 0.01) - sum(ConjPeriodos, BateriaCarregamento(ConjPeriodos) * 0.01) - sum((DL, ConjPeriodos), StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL));\r\n" + 
				"\r\n" + 
				"power_balance(ConjPeriodos).. EnergiaGeradaDiesel(ConjPeriodos) + sum(W, EnergiaGeradaEolica(W, ConjPeriodos))+ sum(P, EnergiaGeradaSolar(P, ConjPeriodos)) + BateriaDescarregamento(ConjPeriodos) =e= BateriaCarregamento(ConjPeriodos) + PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"\r\n" + 
				"DG_generation_limit_up(ConjPeriodos).. EnergiaGeradaDiesel(ConjPeriodos) =l= 20 * StatusGerDiesel(ConjPeriodos);\r\n" + 
				"DG_generation_limit_down(ConjPeriodos).. EnergiaGeradaDiesel(ConjPeriodos) =g= 4 * StatusGerDiesel(ConjPeriodos);\r\n" + 
				"DG_generation(ConjPeriodos).. EnergiaGeradaDiesel(ConjPeriodos) =e= sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjILinearizacao));\r\n" + 
				"GerDieselConstConsume(ConjPeriodos).. ConsumoDiesel(ConjPeriodos) =e= sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao) * consumoger(ConjILinearizacao));\r\n" + 
				"DG_Linear(ConjPeriodos).. sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao)) =e= 1;\r\n" + 
				"DG_Ramp_Up(ConjPeriodos)$(ord(ConjPeriodos) > 1).. EnergiaGeradaDiesel(ConjPeriodos) - EnergiaGeradaDiesel(ConjPeriodos - 1) =l= GerDieselLimiteRampaSubida;\r\n" + 
				"DG_Ramp_Down(ConjPeriodos)$(ord(ConjPeriodos) > 1).. EnergiaGeradaDiesel(ConjPeriodos) - EnergiaGeradaDiesel(ConjPeriodos - 1) =g= GerDieselLimiteRampaDescida;\r\n" + 
				"\r\n" + 
				"*Load_consume(ConjPeriodos).. ConsumoCargasPrioritarias(ConjPeriodos) =e= sum(L, PrevisaoCargas(L, ConjPeriodos) * (1 - StatusCargas(L, ConjPeriodos)));\r\n" + 
				"\r\n" + 
				"Calcule_TotalPotenciaProduzida(ConjPeriodos).. TotalPotenciaProduzida(ConjPeriodos) =e=  sum(W, EnergiaGeradaEolica(W, ConjPeriodos)) + sum(P, EnergiaGeradaSolar(P, ConjPeriodos)) + BateriaDescarregamento(ConjPeriodos) + EnergiaGeradaDiesel(ConjPeriodos);\r\n" + 
				"*Abaixo para c�lculo sem corte da carga priorit�ria\r\n" + 
				"EGP_Calc(ConjPeriodos).. PotenciaGeradaExcesso(ConjPeriodos) =e= TotalPotenciaProduzida(ConjPeriodos);\r\n" + 
				"*PUp_Calc(ConjPeriodos).. PotenciaNaoSupridaCargas(ConjPeriodos) =e= sum(DL, PrevisaoCargasDesp(DL, ConjPeriodos) * (1 - StatusCargasDesp(DL, ConjPeriodos)));\r\n" + 
				"PLUp_Calc(ConjPeriodos).. PotenciaNaoSupridaCargasDesp(ConjPeriodos) =e= sum(DL, PrevisaoCargasDesp(DL, ConjPeriodos)) - PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
				"\r\n" + 
				"* Restri��es relacionadas ao balan�o de pot�ncia\r\n" + 
				"DemandResponse(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
				"DR_Consume(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =l= PotenciaGeradaExcesso(ConjPeriodos);\r\n" + 
				"\r\n" + 
				"WT_upper_limit(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =l= ParGerEolicaMaxima(W);\r\n" + 
				"WT_generation_limit(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =l= PrevisaoGerEolica(W, ConjPeriodos);\r\n" + 
				"WT_generation(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =e= PrevisaoGerEolica(W, ConjPeriodos) * StatusGerEolica(W, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"PT_upper_limit(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =l= ParGerFotovoltaicaMaxima(P);\r\n" + 
				"PT_generation_limit(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =l= PrevisaoGerSolar(P, ConjPeriodos);\r\n" + 
				"PT_generation(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =e= PrevisaoGerSolar(P, ConjPeriodos) * StatusGerSolar(P, ConjPeriodos);\r\n" + 
				"\r\n" + 
				"Battery_power_balance(ConjPeriodos).. BateriaEnergia(ConjPeriodos) =e= BateriaEnergia(ConjPeriodos - 1) + (BateriaCarregamento(ConjPeriodos - 1) - BateriaDescarregamento(ConjPeriodos - 1)) + ParBateriaSOCInicial(ConjPeriodos);\r\n" + 
				"Battery_minimum_energy(ConjPeriodos).. BateriaEnergia(ConjPeriodos) =g= ParBateriaMinimaEnergia;\r\n" + 
				"Battery_maximum_energy(ConjPeriodos).. BateriaEnergia(ConjPeriodos) =l= ParBateriaMaximaEnergia;\r\n" + 
				"Battery_soc(ConjPeriodos).. BateriaSoC(ConjPeriodos) =e= BateriaEnergia(ConjPeriodos) / ParBateriaMaximaEnergia * 100;\r\n" + 
				"Battery_charge_limit_up(ConjPeriodos).. BateriaCarregamento(ConjPeriodos) =l= ParBateriaLimiteCarregamento * StatusBateria(ConjPeriodos);\r\n" + 
				"Battery_charge_limit_down(ConjPeriodos).. BateriaCarregamento(ConjPeriodos) =g= 0;\r\n" + 
				"Battery_charge_limit_energy(ConjPeriodos).. ParBateriaMaximaEnergia =g= BateriaEnergia(ConjPeriodos - 1) + BateriaCarregamento(ConjPeriodos);\r\n" + 
				"Battery_discharge_limit_up(ConjPeriodos).. BateriaDescarregamento(ConjPeriodos) =l= ParBateriaLimiteDescarregamento * (1 - StatusBateria(ConjPeriodos));\r\n" + 
				"Battery_discharge_limit_down(ConjPeriodos).. BateriaDescarregamento(ConjPeriodos) =g= 0;\r\n" + 
				"Battery_discharge_limit_energy(ConjPeriodos).. BateriaEnergia(ConjPeriodos - 1) =g= BateriaDescarregamento(ConjPeriodos);\r\n" + 
				"\r\n" + 
				"model microgrid / all /;\r\n" + 
				"\r\n" + 
				"solve microgrid using MIP minimizing CustoTotal;\r\n" + 
				"\r\n" + 
				"Display CustoTotal.l, CustoTotal.m\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"\r\n";
//				"Sets" + 
//				"	ConjPeriodos 	Conjunto de Periodos          				 \n" +
//				"	ConjGerDiesel   conjunto dos geradores a diesel          \n" + 
//				"   ConjPeriodos    conjunto dos per�odos da simula��o       \n" + 
//				"   ConjGerSolar    conjunto dos geradores fotovoltaicos     \n" + 
//				"   ConjGerEolica   conjunto dos geradores eolicos           \n" + 
//				"   ConjCargasPrioritarias  conjunto das cargas prioritarias \n" + 
//				"   ConjCargasDespachaveis  conjunto das cargas despachaveis \n" + 
//				"   ConjBaterias    conjunto das baterias                    \n" +
//				"   ConjILinearizacao conjunto com valores para linearizacao do consumo do gerador a diesel		\n" +
//				"															 \n" +
//				"	Alias(ConjGerDiesel, G);\r\n" + 
//				"	Alias(ConjGerEolica, W);\r\n" + 
//				"	Alias(ConjCargasPrioritarias, L);\r\n" + 
//				"	Alias(ConjBaterias, B);\r\n" + 
//				"	Alias(ConjGerSolar, P);\r\n" + 
//				"	Alias(ConjCargasDespachaveis, DL);\r\n" +
//				"																						\n" +
//				"* Definicao dos custos de fornecimento dos Recursos Energicamente Distribuidos			\n" + 
//				"Scalar CustoGerEolica custo de gera��o por kW  para energia e�lica						\n" + 
//				"Scalar CustoGerSolar valor de gera��o por kW  para energia solar						\n" + 
//				"Scalar CustoBateria valor para utiliza��o da bateria									\n" + 
//				"Scalar CustoCombustivel custo de combustivel (R$ por litro)							\n" + 
//				"															 							\n" +
//				"Scalar BatteryInicitalSOC valor inicialpara estado de carga da bateria em kW			\n" + 
//				"															 							\n" +
//				"* Definicao de constantes para a geracao a diesel  									\n" + 
//				"Scalar GerDieselConstA parametro A do calculo de linearizacao 							\n" + 
//				"Scalar GerDieselConstB parametro B do calculo de linearizacao							\n" + 
//				"Scalar GerDieselConstC parametro C do calculo de linearizacao							\n" + 
//				"Scalar GerDieselLimiteRampaSubida limite de subida de geracao a diesel					\n" + 
//				"Scalar GerDieselLimiteRampaDescida limite de descida de geracao a diesel				\n" + 
//				"																						\n" +
//				"* Par�metros para configura��o de limites operacionais dos elementos da microrrede		\n" + 
//				"Parameters																				\n" + 
//				"	ParGerDieselMaxima(ConjGerDiesel) limites maximo de operacao dos geradores a dieselem kW			\n" + 
//				"	Parameter ParGerDieselMinima(ConjGerDiesel) limites minimo de operacao dos geradores a dieselem kW	\n" + 
//				"	Parameter ParGerFotovoltaicaMaxima(ConjGerSolar) limites m�ximo de gera��o fotovoltaica em kW		\n" + 
//				"   ParGerEolicaMaxima(ConjGerEolica) limites m�ximo da gera��o eolica em kW							\n" + 
//				"   ParBateriaLimiteCarregamento limite de carregamento da bateria em kW								\n" + 
//				"   ParBateriaLimiteDescarregamento limite d edescarregamento da bateria em kW							\n" + 
//				"   ParBateriaMinimaEnergia limite minimo de armazenamento da bateria SOC\r\n" + 
//				"   ParBateriaMaximaEnergia limite maximo de armazenamento da bateria SOC\r\n" + 
//				"   ParBateriaSOCInicial(ConjPeriodos) valor do SOC da bateria para o periodo inicial da simula��o\r\n" + 
//				"   ParCargasDespachaveisPenalidade(ConjCargasDespachaveis) custo de penalizacao das cargas despach�veis\r\n" + 
//				"   ParGerDieselConsumoLinearizado(ConjILinearizacao) valores linearizados usados para a curva de consumo do gerador\r\n" + 
//				"   consumoger(ConjILinearizacao) curva de consumo do gerador interpolada		 									\n" + 
////				"   		 consumoger(ConjILinearizacao)$(ord(ConjILinearizacao) > 1) = (GerDieselConstA * ParGerDieselConsumoLinearizado(ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjILinearizacao) + GerDieselConstB * ParGerDieselConsumoLinearizado(ConjILinearizacao) + GerDieselConstC);	\n" + 
//				"	dt(conjPeriodos) conjunto de periodos e intervalo de cada per�odo									\n" +
//				"																						\n" +  
//				"   PrevisaoCargasDesp(ConjCargasDespachaveis, ConjPeriodos)							\n" +
//				"   PrevisaoGerSolar(ConjGerSolar, ConjPeriodos)										\n" +
//				"   PrevisaoGerEolica(ConjGerEolica, ConjPeriodos);										\n" +
//				"																						\n" +
//				"Variable CustoTotal Custo total de opera��o;\r\n" + 
//				"\r\n" + 
//				"Positive Variable EnergiaGeradaEolica(ConjGerEolica, ConjPeriodos) Geracao eolica do gerador i no periodo t;\r\n" + 
//				"Positive Variable EnergiaGeradaSolar(ConjGerSolar, ConjPeriodos) Geracao solar do gerador i no periodo t;\r\n" + 
//				"Positive Variable EnergiaGeradaDiesel(ConjPeriodos) Geracao a diesel no periodo t;\r\n" + 
//				"\r\n" + 
//				"Variable BateriaCarregamento(ConjPeriodos) Carregamento da bateria no periodo t;\r\n" + 
//				"Variable BateriaDescarregamento(ConjPeriodos) Descarregamento da bateria no periodo t;\r\n" + 
//				"Variable BateriaEnergia(ConjPeriodos) Energia armazenada na bateria no periodo t;\r\n" + 
//				"Variable BateriaSoC(ConjPeriodos) Estado de carga (SoC) da bateria no periodo t;\r\n" + 
//				"\r\n" + 
//				"Variable ConsumoDiesel(ConjPeriodos) Consumo de combustivel do gerador a diesel no periodo t;\r\n" + 
//				"*Variable ConsumoCargasPrioritarias(ConjPeriodos) Consumo das cargas prioritarias no periodo t;\r\n" + 
//				"Variable ConsumoCargasDesp(ConjPeriodos) Consumo das cargas despachaveis no periodo t;\r\n" + 
//				"\r\n" + 
//				"Variable TotalPotenciaProduzida(ConjPeriodos) Total de potencia produzida no periodo t;\r\n" + 
//				"Positive Variable PotenciaGeradaExcesso(ConjPeriodos) Excesso de potencia produzida no periodo t;\r\n" + 
//				"Positive Variable PotenciaNaoSupridaCargas(ConjPeriodos) Potencia nao suprida das cargas prioritarias no periodo t;\r\n" + 
//				"Positive Variable PotenciaNaoSupridaCargasDesp(ConjPeriodos) Potencia nao suprida das cargas despachaveis no periodo t;\r\n" + 
//				"Variable PotenciaSupridaCargasDesp(ConjPeriodos) Potencia suprida para as cargas despachaveis no periodo t;\r\n" + 
//				"\r\n" + 
//				"variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
//				"sos2 variables lam(ConjPeriodos, ConjILinearizacao) Utilizada no c�lculo linear de consumo do gerador � diesel;\r\n" + 
//				"\r\n" + 
//				"* VARI�VEIS BIN�RIAS\r\n" + 
//				"Binary Variable StatusGerDiesel(ConjPeriodos) On-Off Status do gerador a diesel no per�odo t (0 desligado - 1 Ligado);\r\n" + 
//				"Binary Variable StatusBateria(ConjPeriodos) Status of battery (0 descarregando - 1 carregando);\r\n" + 
//				"Binary Variable StatusCargasDesp(DL, ConjPeriodos) On-Off Status das cargas despach�veis no per�odo t;\r\n" + 
//				"*Binary Variable StatusCargas(L, ConjPeriodos) On-Off Status das cargas priorit�rias no per�odo t;\r\n" + 
//				"Binary Variable StatusGerSolar(P, ConjPeriodos) On-Off Status do gerador fotovoltaico n no per�odo t;\r\n" + 
//				"Binary Variable StatusGerEolica(W, ConjPeriodos) On-Off Status do gerador e�lico n no per�odo t;\r\n" + 
//				"\r\n" + 
//				"Equations\r\n" + 
//				"*        Fun��o objetivo para o problema de minimiza��o do custo operacional no gerenciamento de uma microrrede\r\n" + 
//				"         Ocost define the objetive function\r\n" + 
//				"\r\n" + 
//				"*        Balan�o de pot�ncia na microrrede\r\n" + 
//				"         power_balance(ConjPeriodos) Balanco de potencia no periodo t\r\n" + 
//				"\r\n" + 
//				"*        Load_consume(ConjPeriodos) Consumo real das cargas priorit�rias para o per�odo t\r\n" + 
//				"\r\n" + 
//				"         EGP_Calc(ConjPeriodos) C�lculo do excesso de pot�ncia para o per�odo t\r\n" + 
//				"*        PUp_Calc(ConjPeriodos) C�lculo da energia n�o suprida para o per�odo t\r\n" + 
//				"         PLUp_Calc(ConjPeriodos) C�lculo da energia n�o suprida para as cargas priorit�rias no per�odo t\r\n" + 
//				"\r\n" + 
//				"*        Restri��es relacionadas �s cargas despach�veis\r\n" + 
//				"         DemandResponse(ConjPeriodos) Aplica��o da resposta da demanda para o per�odo t\r\n" + 
//				"         DR_Consume(ConjPeriodos) Consumo real das cargas despach�veis para o per�odo t\r\n" + 
//				"\r\n" + 
//				"*        Restri��es relacionada ao gerador diesel\r\n" + 
//				"         DG_generation(ConjPeriodos) Gera��o no per�odo t\r\n" + 
//				"         GerDieselConstConsume(ConjPeriodos) Consumo de diesel para o gerador g no per�odo t\r\n" + 
//				"         DG_generation_limit_up(ConjPeriodos) Limite m�ximo de gera��o para o gerador diesel\r\n" + 
//				"         DG_generation_limit_down(ConjPeriodos) Limite m�nimo de gera��o para o gerador diesel\r\n" + 
//				"         DG_Linear(ConjPeriodos) Restri��o da fun��o sos2 definindo um �nico intervalo linear para pi\r\n" + 
//				"         DG_Ramp_Up(ConjPeriodos) Restri��o de rampa de subida para gera��o a diesel\r\n" + 
//				"         DG_Ramp_Down(ConjPeriodos) Restri��o de rampa de descida para a gera��o a diesel\r\n" + 
//				"\r\n" + 
//				"         Calcule_TotalPotenciaProduzida(ConjPeriodos) calculo do total gerado na microrrede no periodo t\r\n" + 
//				"\r\n" + 
//				"*        Restri��es relacionadas ao gerador e�lico\r\n" + 
//				"         WT_upper_limit(W, ConjPeriodos) Limite superior de gera��o da gera��o e�lica no periodo t\r\n" + 
//				"         WT_generation_limit(W, ConjPeriodos) gera��o e�lica prevista para o per�odo t\r\n" + 
//				"         WT_generation(W, ConjPeriodos)\r\n" + 
//				"\r\n" + 
//				"*        Restri��es relacionadas ao gerador fotovoltaico\r\n" + 
//				"         PT_upper_limit(P, ConjPeriodos) Limite superior de gera��o solar no per�odo t\r\n" + 
//				"         PT_generation_limit(P, ConjPeriodos) Gera��o solar prevista para o per�odo t\r\n" + 
//				"         PT_generation(P, ConjPeriodos)\r\n" + 
//				"\r\n" + 
//				"*        Restri��es relacionadas a bateria\r\n" + 
//				"         Battery_power_balance(ConjPeriodos) balan�o de pot�ncia da bateria\r\n" + 
//				"         Battery_minimum_energy(ConjPeriodos) valor m�nimo para armazenamento da bateria\r\n" + 
//				"         Battery_maximum_energy(ConjPeriodos) valor m�ximo para armazenamento da bateria\r\n" + 
//				"         Battery_charge_limit_up(ConjPeriodos) carregamento da bateria i para o per�odo t\r\n" + 
//				"         Battery_charge_limit_down(ConjPeriodos) limite inferior para carregamento da bateria\r\n" + 
//				"         Battery_charge_limit_energy(ConjPeriodos) limite de carregamento considerando energia armazenada na bateria\r\n" + 
//				"         Battery_discharge_limit_up(ConjPeriodos) descarregamento m�ximo da bateria no per�odo t\r\n" + 
//				"         Battery_discharge_limit_down(ConjPeriodos) descarregamento m�nimo da bateria no per�odo t\r\n" + 
//				"         Battery_discharge_limit_energy(ConjPeriodos) limite de descarregamento considerando energia armazenada na bateria\r\n" + 
//				"         Battery_soc(ConjPeriodos) SoC da bateria no per�odo t;\r\n" + 
//				"\r\n" + 
//				"\r\n" + 
//				"*Ocost..   CustoTotal =e= sum(ConjPeriodos, ConsumoDiesel(ConjPeriodos) * CustoCombustivel ) + sum((ConjPeriodos,ConjGerEolica), (CustoGerEolica * EnergiaGeradaEolica(ConjGerEolica,ConjPeriodos))) + sum((ConjPeriodos,ConjGerSolar), (CustoGerSolar * EnergiaGeradaSolar(ConjGerSolar,ConjPeriodos))) + sum(ConjPeriodos, BateriaDescarregamento(ConjPeriodos) * 0.01) - sum(ConjPeriodos, BateriaCarregamento(ConjPeriodos) * 0.01) - sum((DL, ConjPeriodos), StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL)) + sum(ConjPeriodos, PotenciaNaoSupridaCargasDesp(ConjPeriodos) * 100);\r\n" + 
//				"Ocost..   CustoTotal =e= sum(ConjPeriodos, ConsumoDiesel(ConjPeriodos) * CustoCombustivel ) + sum((ConjPeriodos,ConjGerEolica), (CustoGerEolica * EnergiaGeradaEolica(ConjGerEolica,ConjPeriodos))) + sum((ConjPeriodos,ConjGerSolar), (CustoGerSolar * EnergiaGeradaSolar(ConjGerSolar,ConjPeriodos))) + sum(ConjPeriodos, BateriaDescarregamento(ConjPeriodos) * 0.01) - sum(ConjPeriodos, BateriaCarregamento(ConjPeriodos) * 0.01) - sum((DL, ConjPeriodos), StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos) * ParCargasDespachaveisPenalidade(DL));\r\n" + 
//				"\r\n" + 
//				"power_balance(ConjPeriodos).. EnergiaGeradaDiesel(ConjPeriodos) + sum(W, EnergiaGeradaEolica(W, ConjPeriodos))+ sum(P, EnergiaGeradaSolar(P, ConjPeriodos)) + BateriaDescarregamento(ConjPeriodos) =e= BateriaCarregamento(ConjPeriodos) + PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
//				"\r\n" + 
//				"DG_generation_limit_up(ConjPeriodos).. EnergiaGeradaDiesel(ConjPeriodos) =l= 20 * StatusGerDiesel(ConjPeriodos);\r\n" + 
//				"DG_generation_limit_down(ConjPeriodos).. EnergiaGeradaDiesel(ConjPeriodos) =g= 4 * StatusGerDiesel(ConjPeriodos);\r\n" + 
//				"DG_generation(ConjPeriodos).. EnergiaGeradaDiesel(ConjPeriodos) =e= sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao) * ParGerDieselConsumoLinearizado(ConjILinearizacao));\r\n" + 
//				"GerDieselConstConsume(ConjPeriodos).. ConsumoDiesel(ConjPeriodos) =e= sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao) * consumoger(ConjILinearizacao));\r\n" + 
//				"DG_Linear(ConjPeriodos).. sum(ConjILinearizacao, lam(ConjPeriodos, ConjILinearizacao)) =e= 1;\r\n" + 
//				"DG_Ramp_Up(ConjPeriodos)$(ord(ConjPeriodos) > 1).. EnergiaGeradaDiesel(ConjPeriodos) - EnergiaGeradaDiesel(ConjPeriodos - 1) =l= GerDieselLimiteRampaSubida;\r\n" + 
//				"DG_Ramp_Down(ConjPeriodos)$(ord(ConjPeriodos) > 1).. EnergiaGeradaDiesel(ConjPeriodos) - EnergiaGeradaDiesel(ConjPeriodos - 1) =g= GerDieselLimiteRampaDescida;\r\n" + 
//				"\r\n" + 
//				"*Load_consume(ConjPeriodos).. ConsumoCargasPrioritarias(ConjPeriodos) =e= sum(L, PrevisaoCargas(L, ConjPeriodos) * (1 - StatusCargas(L, ConjPeriodos)));\r\n" + 
//				"\r\n" + 
//				"Calcule_TotalPotenciaProduzida(ConjPeriodos).. TotalPotenciaProduzida(ConjPeriodos) =e=  sum(W, EnergiaGeradaEolica(W, ConjPeriodos)) + sum(P, EnergiaGeradaSolar(P, ConjPeriodos)) + BateriaDescarregamento(ConjPeriodos) + EnergiaGeradaDiesel(ConjPeriodos);\r\n" + 
//				"*Abaixo para c�lculo sem corte da carga priorit�ria\r\n" + 
//				"EGP_Calc(ConjPeriodos).. PotenciaGeradaExcesso(ConjPeriodos) =e= TotalPotenciaProduzida(ConjPeriodos);\r\n" + 
//				"*PUp_Calc(ConjPeriodos).. PotenciaNaoSupridaCargas(ConjPeriodos) =e= sum(DL, PrevisaoCargasDesp(DL, ConjPeriodos) * (1 - StatusCargasDesp(DL, ConjPeriodos)));\r\n" + 
//				"PLUp_Calc(ConjPeriodos).. PotenciaNaoSupridaCargasDesp(ConjPeriodos) =e= sum(DL, PrevisaoCargasDesp(DL, ConjPeriodos)) - PotenciaSupridaCargasDesp(ConjPeriodos);\r\n" + 
//				"\r\n" + 
//				"* Restri��es relacionadas ao balan�o de pot�ncia\r\n" + 
//				"DemandResponse(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =e= sum(DL, StatusCargasDesp(DL, ConjPeriodos) * PrevisaoCargasDesp(DL, ConjPeriodos));\r\n" + 
//				"DR_Consume(ConjPeriodos).. PotenciaSupridaCargasDesp(ConjPeriodos) =l= PotenciaGeradaExcesso(ConjPeriodos);\r\n" + 
//				"\r\n" + 
//				"WT_upper_limit(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =l= ParGerEolicaMaxima(W);\r\n" + 
//				"WT_generation_limit(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =l= PrevisaoGerEolica(W, ConjPeriodos);\r\n" + 
//				"WT_generation(W, ConjPeriodos).. EnergiaGeradaEolica(W, ConjPeriodos) =e= PrevisaoGerEolica(W, ConjPeriodos) * StatusGerEolica(W, ConjPeriodos);\r\n" + 
//				"\r\n" + 
//				"PT_upper_limit(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =l= ParGerFotovoltaicaMaxima(P);\r\n" + 
//				"PT_generation_limit(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =l= PrevisaoGerSolar(P, ConjPeriodos);\r\n" + 
//				"PT_generation(P, ConjPeriodos).. EnergiaGeradaSolar(P, ConjPeriodos) =e= PrevisaoGerSolar(P, ConjPeriodos) * StatusGerSolar(P, ConjPeriodos);\r\n" + 
//				"\r\n" + 
//				"Battery_power_balance(ConjPeriodos).. BateriaEnergia(ConjPeriodos) =e= BateriaEnergia(ConjPeriodos - 1) + (BateriaCarregamento(ConjPeriodos - 1) - BateriaDescarregamento(ConjPeriodos - 1)) + ParBateriaSOCInicial(ConjPeriodos);\r\n" + 
//				"Battery_minimum_energy(ConjPeriodos).. BateriaEnergia(ConjPeriodos) =g= ParBateriaMinimaEnergia;\r\n" + 
//				"Battery_maximum_energy(ConjPeriodos).. BateriaEnergia(ConjPeriodos) =l= ParBateriaMaximaEnergia;\r\n" + 
//				"Battery_soc(ConjPeriodos).. BateriaSoC(ConjPeriodos) =e= BateriaEnergia(ConjPeriodos) / ParBateriaMaximaEnergia * 100;\r\n" + 
//				"Battery_charge_limit_up(ConjPeriodos).. BateriaCarregamento(ConjPeriodos) =l= ParBateriaLimiteCarregamento * StatusBateria(ConjPeriodos) * dt(ConjPeriodos);\r\n" + 
//				"Battery_charge_limit_down(ConjPeriodos).. BateriaCarregamento(ConjPeriodos) =g= 0;\r\n" + 
//				"Battery_charge_limit_energy(ConjPeriodos).. ParBateriaMaximaEnergia =g= BateriaEnergia(ConjPeriodos - 1) + BateriaCarregamento(ConjPeriodos);\r\n" + 
//				"Battery_discharge_limit_up(ConjPeriodos).. BateriaDescarregamento(ConjPeriodos) =l= ParBateriaLimiteDescarregamento * (1 - StatusBateria(ConjPeriodos)) * dt(ConjPeriodos);\r\n" + 
//				"Battery_discharge_limit_down(ConjPeriodos).. BateriaDescarregamento(ConjPeriodos) =g= 0;\r\n" + 
//				"Battery_discharge_limit_energy(ConjPeriodos).. BateriaEnergia(ConjPeriodos - 1) =g= BateriaDescarregamento(ConjPeriodos);\r\n" + 
//				"\r\n" + 
//				"model microgrid / all /;\r\n" + 
//				"\r\n" + 
//				"solve microgrid using MIP minimizing CustoTotal;\r\n" + 
//				"\r\n" + 
//				"Display CustoTotal.l, CustoTotal.m";
	}
	
	public String getData() {
		return this.data;
	}
	
	public String getModel() {
		return this.model;
	}
}
