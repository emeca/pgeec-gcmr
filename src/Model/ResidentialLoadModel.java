package Model;

public class ResidentialLoadModel {
	private Double ratedConsume;
	
	public ResidentialLoadModel(Double ratedConsume) {
		this.ratedConsume = ratedConsume;
	}
	
	public Double getLoad(Double value) {
		return value * this.ratedConsume;
	}
	
	public Double getEqualDemand(Double ratio) {
		return ratedConsume / ratio;
	}
}
