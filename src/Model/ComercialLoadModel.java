package Model;

public class ComercialLoadModel {
	private Double ratedConsume;
	
	public ComercialLoadModel(Double ratedConsume) {
		this.ratedConsume = ratedConsume;
	}
	
	public Double getGeneration(Double value) {
		return value * this.ratedConsume;
	}
	
	public Double getEqualDemand(Double ratio) {
		return ratedConsume / ratio;
	}
}
