package Parser;

public class ATData {
	private String atNumber;
	private String document;
	private String documentNumber;
	private String sector;
	private String entryDateOPU;
	private String entryDateOPS;
	
	public String getAtNumber() {
		return atNumber;
	}

	public void setAtNumber(String atNumber) {
		this.atNumber = atNumber;
	}

	public String getDocument() {
		return document;
	}
	
	public void setDocument(String document) {
		this.document = document;
	}
	
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public String getSector() {
		return sector;
	}
	
	public void setSector(String sector) {
		this.sector = sector;
	}
	
	public String getEntryDateOPU() {
		return entryDateOPU;
	}
	
	public void setEntryDateOPU(String entryDate) {
		this.entryDateOPU = entryDate;
	}
	
	public String getEntryDateOPS() {
		return entryDateOPS;
	}
	
	public void setEntryDateOPS(String entryDate) {
		this.entryDateOPS = entryDate;
	}
}
