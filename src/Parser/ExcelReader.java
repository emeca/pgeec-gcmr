package Parser;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import Data.LoadsData;
import Data.WeatherData;

public class ExcelReader {
	private String fileName;
	private String LoadsFileName;
	
	public ExcelReader(String file, String loadsFile) {
		this.fileName = file;
		this.LoadsFileName = loadsFile;
	}
	
	public WeatherData parseWeatherData() throws IOException {
		WeatherData weatherData = new WeatherData();
		
		// Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(this.fileName));

        // Retrieving the number of sheets in the Workbook
        //System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

        /*
           =============================================================
           Iterating over all the sheets in the workbook (Multiple ways)
           =============================================================
        */

        // 2. Or you can use a for-each loop
        //System.out.println("Retrieving Sheets using for-each loop");
        for(Sheet sheet: workbook) {
            System.out.println("=> " + sheet.getSheetName());
        }

        /*
           ==================================================================
           Iterating over all the rows and columns in a Sheet (Multiple ways)
           ==================================================================
        */

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // 2. Or you can use a for-each loop to iterate over the rows and columns
        //System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
        for (Row row: sheet) {
        	if (row.getRowNum() > 0) {
	            for(Cell cell: row) {
	            	if (row.getRowNum() > 0) {
		                String cellValue = dataFormatter.formatCellValue(cell);
	//	                System.out.print(cellValue + "\t");
		                switch (cell.getAddress().getColumn()) {
		                case 1:
		                	weatherData.addWindValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	weatherData.addWindValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	break;
		                case 2:
		                	weatherData.addTemperatureValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	weatherData.addTemperatureValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	break;
		                case 3:
		                	weatherData.addIrradiationValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	weatherData.addIrradiationValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	break;
		                }
	            	}
	            }
        	}
//            System.out.println();
        }

        // Closing the workbook
        workbook.close();

		
		return weatherData;
	}

	public LoadsData parseLoadsData() throws IOException {
		LoadsData loadsData = new LoadsData();
		
		// Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(this.LoadsFileName));

        // Retrieving the number of sheets in the Workbook
        //System.out.println("Workbook Loads has " + workbook.getNumberOfSheets() + " Sheets : ");

        /*
           =============================================================
           Iterating over all the sheets in the workbook (Multiple ways)
           =============================================================
        */

        // 2. Or you can use a for-each loop
        //System.out.println("Retrieving Sheets using for-each loop");
        for(Sheet sheet: workbook) {
           // System.out.println("=> " + sheet.getSheetName());
        }

        /*
           ==================================================================
           Iterating over all the rows and columns in a Sheet (Multiple ways)
           ==================================================================
        */

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(1);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // 2. Or you can use a for-each loop to iterate over the rows and columns
        //System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
        for (Row row: sheet) {
        	if (row.getRowNum() > 0) {
	            for(Cell cell: row) {
	            	if (row.getRowNum() > 0) {
		                String cellValue = dataFormatter.formatCellValue(cell);
	//	                System.out.print(cellValue + "\t");
		                switch (cell.getAddress().getColumn()) {
		                case 1:
		                	loadsData.addComercialValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	loadsData.addComercialValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	break;
		                case 2:
		                	loadsData.addResidentialHighValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	loadsData.addResidentialHighValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	break;
		                case 3:
		                	loadsData.addResidentialMediumValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	loadsData.addResidentialMediumValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	break;
		                case 4:
		                	loadsData.addResidentialLowValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	loadsData.addResidentialLowValue(new Date(), Double.parseDouble(cellValue.replace(",", ".")));
		                	break;
		                }
		                //System.out.println("C�lula: [" + cell.getAddress().getRow() + "," + cell.getAddress().getColumn() + "] valor: " + cellValue);
	            	}
	            }
        	}
//            System.out.println();
        }

        // Closing the workbook
        workbook.close();

		
		return loadsData;
	}

	
}
