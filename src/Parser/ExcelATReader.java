package Parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelATReader {

	public static String formatDate(String date) {
		String nDate = new String();
		
		if (date.length() > 18)
			nDate = date.substring(0, 11) + date.substring(14, 19);
		
		return nDate;
	}
	
	public static void main(String[] args) throws Exception, IOException {
		// Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File("ListaATs.xlsx"));

        // Retrieving the number of sheets in the Workbook
        System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

        /*
           =============================================================
           Iterating over all the sheets in the workbook (Multiple ways)
           =============================================================
        */

        // 2. Or you can use a for-each loop
        System.out.println("Retrieving Sheets using for-each loop");
        for(Sheet sheet: workbook) {
            System.out.println("=> " + sheet.getSheetName());
        }

        /*
           ==================================================================
           Iterating over all the rows and columns in a Sheet (Multiple ways)
           ==================================================================
        */

        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // 2. Or you can use a for-each loop to iterate over the rows and columns
        System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
        Boolean readingData = false;
        ArrayList<ATData> ats = new ArrayList<ATData>();
        ATData atAux = new ATData();
        Integer docCount = 0;
        Integer opuCount = 0;
        Integer opsCount = 0;
        Integer sectorCount = 0;
        Integer atNumberCount = 0;
        for (Row row: sheet) {
        	if (row.getRowNum() == 137) {
        		System.out.println("In�cio dos problemas de parser.");
        	}
        	if (row.getRowNum() > 0) {
//	            for(Cell cell: row) {
//	            	if (row.getRowNum() > 0) {
//		                String cellValue = dataFormatter.formatCellValue(cell);
	//	                System.out.print(cellValue + "\t");
        		String cellValue = dataFormatter.formatCellValue(row.getCell(3));
            	if (readingData) {
                	if (!cellValue.isEmpty() && cellValue.contains("SS")) {
                		docCount++;
                		atAux.setDocument(cellValue.substring(0, 3));
                		atAux.setDocumentNumber(dataFormatter.formatCellValue(row.getCell(8)));
                		ats.add(atAux);
                		//System.out.println(cellValue);
                		if (atAux.getSector() != null && atAux.getEntryDateOPS() != null)
                			readingData = false;
                	}
            	}
            	cellValue = dataFormatter.formatCellValue(row.getCell(6));
            	if (!readingData) {
                	if (!cellValue.isEmpty() && cellValue.contains("/")) {
                		atNumberCount++;
                		atAux = new ATData();
                		atAux.setAtNumber(cellValue.toString());
                		readingData = true;
                	}
            	}
            	cellValue = dataFormatter.formatCellValue(row.getCell(14));
            	if (readingData) {
                	if (!cellValue.isEmpty() && cellValue.contains("M")) {
                		sectorCount++;
                		atAux.setSector(dataFormatter.formatCellValue(row.getCell(14)));
                		if (atAux.getDocument() != null && atAux.getEntryDateOPS() != null)
                			readingData = false;
                	}
            	}
            	cellValue = dataFormatter.formatCellValue(row.getCell(34));
            	if (readingData) {
                	if (!cellValue.isEmpty() && cellValue.contains("Ent. OPU") ) {
                		if ( (docCount != sectorCount) || (docCount != opsCount) || (docCount != opuCount)) {
                			System.out.println("Linha " + row.getRowNum());
                		}
                		opuCount++;
                		//atAux = new ATData();
                		atAux.setEntryDateOPU(dataFormatter.formatCellValue(row.getCell(39)) != null ? formatDate(dataFormatter.formatCellValue(row.getCell(39))) : "");
                		//readingData = true;
                	} else if (!cellValue.isEmpty() && cellValue.contains("Ent. OPS")) {
                		opsCount++;
                		atAux.setEntryDateOPS(dataFormatter.formatCellValue(row.getCell(39)) != null ? formatDate(dataFormatter.formatCellValue(row.getCell(39))) : "");
                		if (atAux.getSector() != null && atAux.getDocument() != null)
                			readingData = false;
                	}
                	//System.out.println(cellValue);
            	}
//            	} else if (!cellValue.isEmpty() && cellValue.contains("Ent. OPS")) {
//            		opsCount++;
//            		atAux.setEntryDateOPS(dataFormatter.formatCellValue(row.getCell(39)) != null ? formatDate(dataFormatter.formatCellValue(row.getCell(39))) : "");
//            		if (atAux.getSector() != null && atAux.getDocument() != null)
//            			readingData = false;
//            	} else if (!cellValue.isEmpty() && cellValue.contains("Ent. OPU")) {
//            		//System.out.println("Resolvendo problema de documento n�o associado.");
//            		//atAux = new ATData();
//            		//ats.add(atAux);
//            		atAux.setEntryDateOPU(dataFormatter.formatCellValue(row.getCell(39)) != null ? formatDate(dataFormatter.formatCellValue(row.getCell(39))) : "");
//            	}
//		                }
//	            	}
//	            }
        	}
//            System.out.println();
        }
        System.out.println("* " + docCount + " documentos encontrados.");
        System.out.println("* " + sectorCount + " setores encontrados.");
        System.out.println("* " + opuCount + " opu encontrados.");
        System.out.println("* " + opsCount + " ops encontrados.");
        BufferedWriter br = new BufferedWriter(new FileWriter("ats.csv"));
        StringBuilder sb = new StringBuilder();
        for (ATData at: ats) {
        	sb.append(at.getAtNumber() + "; ");
        	sb.append(at.getDocument() + "; ");
        	sb.append(at.getDocumentNumber() + "; ");
        	sb.append(at.getSector() + "; ");
        	sb.append(at.getEntryDateOPU() + "; ");
        	sb.append(at.getEntryDateOPS() + "\n");
//        	System.out.print(at.getDocument() + "; " + 
//        			at.getDocumentNumber() + "; " +
//        			at.getSector().trim() + "; " + 
//        			(at.getEntryDateOPU() == null ? "" : at.getEntryDateOPU()) + "; " + 
//        			(at.getEntryDateOPS() == null ? "" : at.getEntryDateOPS()) + "\n");
        }
        br.write(sb.toString());
        br.close();
        // Closing the workbook
        workbook.close();

	}

}
