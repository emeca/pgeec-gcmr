# Gerenciador Centralizado de Microrrede #

Esta página contém o código-fonte e as informações necessárias para alterar/executar o GCMR proposto no trabalho de mestrado de André da Silva Barbosa no Programa de Pós Graduação em Engenharia Elétrica e Computação da UNIOESTE Foz.

O GCMR foi desenvolvido em JAVA e utiliza algumas bibliotecas para interpretação de arquivos (Excel, CSV) e comunicação com o GAMS via API. Essas bibliotecas são todas livres e tem download disponível em suas páginas na internet.

As IDEs recomendadas para executar o projeto são Eclipse ou IntelliJ.

